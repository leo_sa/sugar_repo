using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputHandler : MonoBehaviour
{
    public Vector2 RawMovementInput { get; private set; }
    public int NormalizedInputX { get; private set; }
    public int NormalizedInputY { get; private set; }

    public bool JumpInput { get; private set; }
    public bool JumpInputStop { get; private set; }
    public bool GrabInput { get; private set; }
    public bool GlideInput { get; private set; }
    public bool SlowWalkInput { get; private set; }
    public bool GrapplingHookInput { get; private set; }

    [SerializeField]
    private float inputHoldTime = 0.2f;
    private float jumpInputStartTime;

    [SerializeField]
    private float glideInputHoldTime = 0.15f;
    private float glideInputStartTimer;
    private bool startGlideTimer;

    /// <summary>
    /// Timer in start setzten
    /// Methode die, sobald timer glide hold zeit erreicht hat, GlideInput auf true setzt
    /// on released in canceled auf false und timer resetten
    /// aus unity hold und times entfernen
    /// nur �ber code timer berechnen
    /// </summary>
    /// 

    private void Start()
    {
        glideInputStartTimer = 0;
    }

    private void Update()
    {
        CheckJumpInputHoldTime();
        CheckGlideInputHoldTime();
    }

    //context gets value, true or false, phase
    public void OnMoveInput(InputAction.CallbackContext context)
    {
        RawMovementInput = context.ReadValue<Vector2>();

        //To give back either 1 or 0
        if (Mathf.Abs(RawMovementInput.x) > 0.5f)
        {
            NormalizedInputX = (int)(RawMovementInput * Vector2.right).normalized.x;
        }
        else
        {
            NormalizedInputX = 0;
        }

        if(Mathf.Abs(RawMovementInput.y) > 0.5f)
        {
            NormalizedInputY = (int)(RawMovementInput * Vector2.up).normalized.y;
        }
        else
        {
            NormalizedInputY = 0;
        }
    }

    public void OnJumpInput(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            JumpInput = true;
            JumpInputStop = false;
            jumpInputStartTime = Time.time;
        }
        
        if(context.canceled)
        {
            JumpInputStop = true;
        }
    }

    public void OnGlideInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            startGlideTimer = true;
            //GlideInput = true;
        }

        if(context.canceled)
        {
            startGlideTimer = false;
            glideInputStartTimer = 0;
            //GlideInput = false;
        }
    }

    public void OnGrabInput(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            GrabInput = true;
        }

        if(context.canceled)
        {
            GrabInput = false;
        }
    }

    public void OnSlowWalkInput(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            SlowWalkInput = true;
        }

        if(context.canceled)
        {
            SlowWalkInput = false;
        }
    }

    public void OnGrapplingHookInput(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            GrapplingHookInput = true;
        }

        if(context.canceled)
        {
            GrapplingHookInput = false;
        }
    }

    //Shorted version of a method with just one line of code in it
    public void UseJumpInput() => JumpInput = false;

    private void CheckJumpInputHoldTime()
    {
        if(Time.time >= jumpInputStartTime + inputHoldTime)
        {
            JumpInput = false;
        }
    }

    private void CheckGlideInputHoldTime()
    {
        if(startGlideTimer)
        {
            glideInputStartTimer += Time.deltaTime;
            if(glideInputStartTimer >= glideInputHoldTime)
            {
                GlideInput = true;
            }
            else
            {
                GlideInput = false;
            }
        }
        else
        {
            GlideInput = false;
        }
    }
}
