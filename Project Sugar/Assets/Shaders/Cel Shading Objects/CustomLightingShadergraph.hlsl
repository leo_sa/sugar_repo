void MainLight_float(float smoothness, float3 view, float3 normal, float3 WorldPos, out float3 Direction, out float3 Color, out float DistanceAtten, out float ShadowAtten, out float3 Specular)
{
    #ifdef SHADERGRAPH_PREVIEW
    Direction  = float3(0.5, 0.5, 0);
    Color = 1;
    DistanceAtten = 1;
    ShadowAtten = 1;
    Specular = float3(1, 1, 1);
#else
    float4 shadowCoord = TransformWorldToShadowCoord(WorldPos);
    
    Light mainLight = GetMainLight(shadowCoord);
    Direction = mainLight.direction;
    Color = mainLight.color;
    DistanceAtten = mainLight.distanceAttenuation;
    Specular = LightingSpecular(Color, Direction, normal, view, float4(float3(1, 1, 1), 0), smoothness);
    
#if !defined(_MAIN_LIGHT_SHADOWS) || defined(_RECEIVE_SHADOWS_OFF)
    ShadowAtten = 1.0h;
    #else
    ShadowSamplingData shadowSamplingData = GetMainLightShadowSamplingData();
    float shadowStrength = GetMainLightShadowStrength();
    ShadowAtten = SampleShadowmap(shadowCoord, TEXTURE2D_ARGS(_MainLightShadowmapTexture, sampler_MainLightShadowmapTexture), shadowSamplingData, shadowStrength, false);
#endif
    #endif
    ShadowAtten = step(0.2f, ShadowAtten);

}


void AdditionalLights_float(float3 specColor, float Smoothness, float3 WorldPosition, float3 WorldNormal, float3 WorldView, out float3 Diffuse, out float3 Specular, out float3 Dir, out float3 Col)
{
    float3 diffuseColor = 0;
    float3 specularColor = 0;
    float3 directionVector = 0;
    float3 colorC = 0;
    
    #ifndef SHADERGRAPH_PREVIEW
    Smoothness = exp2(10 * Smoothness + 1);
    WorldNormal = normalize(WorldNormal);
    WorldView = SafeNormalize(WorldView);
    int pixelLightCount = GetAdditionalLightsCount();
    
    for (int i = 0; i < pixelLightCount; i++)   //work here! No Color variations
    {
        Light light = GetAdditionalLight(i, WorldPosition);
        half3 attenuatedColor = light.color * (light.distanceAttenuation * light.shadowAttenuation);
        half3 direction = light.distanceAttenuation * light.shadowAttenuation;
        //LightColor = light.color;
        //directionVector += LightingLambert(direction, light.direction, WorldNormal);
        directionVector += direction * light.direction;
        colorC += attenuatedColor;
        diffuseColor += LightingLambert(attenuatedColor, light.direction, WorldNormal);
        specularColor += LightingSpecular(attenuatedColor, light.direction, WorldNormal, WorldView, float4(specColor, 0), Smoothness);
        Specular = step(specularColor, 0.5f);
    }
    #endif
    
    Col = colorC;
    Dir = directionVector;
    Diffuse = diffuseColor;
    Specular = specularColor;
}
//edited version of unity provided script