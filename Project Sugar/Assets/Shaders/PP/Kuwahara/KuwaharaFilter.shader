Shader "Unlit/KuwaharaFilter"
{
    Properties
    {
        [NoScaleOffset]_MainTex ("Texture", 2D) = "white" {}
        _Kernel("Size", float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            //Kuwahara Related

            struct region{
                float4 allPixels[10];
                float4 mean;
                float4 deviation;
            };


            sampler2D _MainTex;
            float4 _MainTex_ST;
            uniform float2 _MainTex_TexelSize; 
            float _Kernel;
            
            //Index: Is it Left right or up?
            region ApplyRegion(float2 uv, int index, sampler2D input){

                region r;
                float4 allPixelsProduct = 0;
                float4 squareSum;
                float2 offset = float2(_MainTex_TexelSize.x, _MainTex_TexelSize.y);
                float4 pixelArray[10];

                [unroll]
                for(int i = 0; i < 10; i++){
                    switch(index){

                        //RIGHT
                        case 0:                       
                        pixelArray[i] = tex2D(input, uv + (float2(i, 0) * _Kernel * offset));                            
                        //pixelArray[i] = float4(0, 0, 0, 1);                                                            
                        break;

                        //LEFT
                        case 1:
                        pixelArray[i] = tex2D(input, uv - (float2(i, 0) * _Kernel * offset));  
                        //pixelArray[i] = float4(1, 0, 0, 1);                                                            
                        break;


                        //UP
                        case 2:
                        pixelArray[i] = tex2D(input, uv + (float2(0, i) * _Kernel * offset));  
                        //pixelArray[i] = float4(0, 1, 0, 1);                                                            
                        break;


                        //DOWN
                        case 3:
                        pixelArray[i] = tex2D(input, uv - (float2(0, i) * _Kernel * offset));    
                        //pixelArray[i] = float4(1, 1, 1, 1);                                                            
                        break;
                    }   
                    allPixelsProduct += pixelArray[i];   
                    squareSum = pixelArray[i] * pixelArray[i];
                    r.allPixels[i] = pixelArray[i];            
                }
                    

                //Set Mean
                r.mean = allPixelsProduct * 0.1f;      //Mean is X/N or X*1/n to avoid division
                r.mean.a = 1;

                //Calculate Variance
                float4 squaredProduct = 0;

                for(int x = 0; x < 10; x++){
                    squaredProduct += pow((r.allPixels[x] - r.mean), 2);     
                }

                //float avgSquare = (squaredProduct.r + squaredProduct.g + squaredProduct.b + squaredProduct.a) / 4;
 
                r.deviation = squaredProduct * 0.1f; //Mean is X/N or X*1/n to avoid division
                r.deviation.a = 1;

               return r;
            }


            float4 ApplyKuwahara(region r, region l, region u, region d){

                region regionArray[4];

                regionArray[0] = r;
                regionArray[1] = l;
                regionArray[2] = u;
                regionArray[3] = d;



                float smallestValue = 500; //Just say large number that is always larger than deviation
                int index;
                

                for(int i = 0; i < 4; i ++){
                    
                    if(regionArray[i].deviation.Length < smallestValue){
                    index = i;
                    smallestValue = regionArray[i].deviation.Length;
                    }
                }

                return float4(1, 1, 1, 1);

            }


            //VF Shader
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };



            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

                region r = ApplyRegion(i.uv, 0, _MainTex);
                region l = ApplyRegion(i.uv, 1, _MainTex);

                region u = ApplyRegion(i.uv, 2, _MainTex);
                region d = ApplyRegion(i.uv, 3, _MainTex);

                float4 cachedValue = 0;
                float4 decider = r.deviation;
                float4 color = r.mean;


                cachedValue = step(l.deviation, decider);
                color = lerp(color, l.mean, cachedValue);
                decider = lerp(decider, l.deviation, cachedValue);

                cachedValue = step(u.deviation, decider);
                color = lerp(color, u.mean, cachedValue);
                decider = lerp(decider, u.deviation, cachedValue);

                cachedValue = step(d.deviation, decider);
                color = lerp(color, d.mean, cachedValue);

                color.a = 1;

                return color;
            }
            ENDHLSL
        }
    }
}
