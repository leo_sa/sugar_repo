﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

//This was made with the help of the following tutorial: https://www.youtube.com/watch?v=6Yg2EedqDhc
public class BlitRendererFeature : ScriptableRendererFeature
{
    class CustomRenderPass : ScriptableRenderPass
    {
        private string profilingName;
        private int materialPassIndex;
        private string exposeString;
        private Material material;
        private RenderTargetIdentifier source;
        private RenderTargetHandle tempTexture;
        private FilteringSettings filterSettings;
       

        public CustomRenderPass(string profilingName, Material mat, int passIndex, LayerMask mask, string exposeString) : base()
        {
            this.profilingName = profilingName;
            materialPassIndex = passIndex;
            material = mat;
            filterSettings = new FilteringSettings(RenderQueueRange.opaque, mask);
            this.exposeString = exposeString;
            tempTexture.Init("_TempBlitMaterialTexture");
        }

        public void SetSource(RenderTargetIdentifier source)
        {
            this.source = source;
        }

       

      
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {           
            CommandBuffer cmd = CommandBufferPool.Get(profilingName);

            RenderTextureDescriptor cameraTextureDesc = renderingData.cameraData.cameraTargetDescriptor;
            cameraTextureDesc.colorFormat = RenderTextureFormat.ARGB32;     //ensuring transparency
            cameraTextureDesc.depthBufferBits = 0;

            cmd.GetTemporaryRT(tempTexture.id, cameraTextureDesc, FilterMode.Point);
            //cmd.GetTemporaryRT(tempTexture.id, cameraTextureDesc, FilterMode.Bilinear);


            Blit(cmd, source, tempTexture.Identifier(), material, materialPassIndex);
            Blit(cmd, tempTexture.Identifier(), source);

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);

        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            cmd.ReleaseTemporaryRT(tempTexture.id);
        }
    }

    [System.Serializable]
    public class Settings
    {
        public LayerMask mask;
        public Material material;
        public int materialPassIndex = -1;
        public RenderPassEvent renderEvent = RenderPassEvent.AfterRenderingOpaques;
        public string exposeString; //leave empty if unwanted
    }

    [SerializeField]
    private Settings settings = new Settings();

    public Material Material
    {
        get => settings.material;
    }

    private CustomRenderPass m_ScriptablePass;

    public override void Create()
    {       
        this.m_ScriptablePass = new CustomRenderPass(name, settings.material, settings.materialPassIndex, settings.mask, settings.exposeString);

        m_ScriptablePass.renderPassEvent = settings.renderEvent;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        m_ScriptablePass.SetSource(renderer.cameraColorTarget);
        renderer.EnqueuePass(m_ScriptablePass);
    }
}


