using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplinghookSoundController : MonoBehaviour
{
    [SerializeField] private AudioSource grappleSource;
    [SerializeField] private AudioClip onHookEnter, onHookGo;
    private PlayerGrapplingHookState grapplingHookState;
    private void Start(){
        ServiceLocator.grappleState.onHookHit += OnHookHit;
        ServiceLocator.grappleState.onHookLeave += OnHookExit;
    }

    private void OnHookHit(){
        grappleSource.PlayFast(onHookEnter);
        //StartCoroutine(ServiceLocator.smoothCam.Shake(0.05f, 0.025f));
    }

    private void OnHookExit(){
        grappleSource.PlayFast(onHookGo);
    }
}
