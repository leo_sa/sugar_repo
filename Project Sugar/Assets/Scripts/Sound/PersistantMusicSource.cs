using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistantMusicSource : MonoBehaviour
{
    private static PersistantMusicSource instance;

    public static PersistantMusicSource Instance { get => instance; set => instance = value; }

    void Awake(){
        if(instance == null){
            instance = this;
            DontDestroyOnLoad(gameObject);
        } 
        else Destroy(gameObject);
   }

   void OnDisable(){
       if(instance == this) instance = null;
   }

   //TODO:
   //Dont destroy from credits and **ensure** destruction :D
}
