using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSoundController : MonoBehaviour
{
    [SerializeField] private StepSound stepSound;
    [SerializeField] private bool isPlayer = true;
    [SerializeField] private AudioClip fall, jump;
    [SerializeField] private AudioSource source;
    //When multiple characters, change variables to be fed from a scriptable OBJ
    private void FeedData(){
        //Change variables based on ScrObj here if necessary
    }

    private void Start(){
        if(isPlayer){
            RigAnimationController.Instance.onGroundedEnter += PlayFall;
            RigAnimationController.Instance.onGroundedExit += PlayJump;
            ServiceLocator.jumpState.onJump += PlayJump;
        }
    }

    private void PlayFall(){
        Utility.PlayFast(source, fall);
        stepSound.ExecuteStep();
    }

    private void PlayJump(){
        Utility.PlayFast(source, jump);
    }
}
