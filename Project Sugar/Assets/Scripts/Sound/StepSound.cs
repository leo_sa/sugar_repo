using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    [SerializeField] private AudioClip[] mossStep, gravelStep, wetStep, grassStep, woodStep, stoneStep;
    [SerializeField] private AudioSource source;
    [SerializeField] private float castLength = 1;
    [SerializeField] private LayerMask castMask;
    //Currently: Stoney-Steps, maybe Wet-Steps, Wood-Steps?
    [SerializeField]private string currentTag = "XXX";
    private uint[] trackers;
    void Start(){
        trackers = new uint[6]; //Parralel to all audiclip[]
    }

    private void PlayStep(){
        switch (currentTag)
        {
            case "STEP_MOSS":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(mossStep, ref trackers[0]));
            break;

            case "STEP_GRAVEL":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(gravelStep, ref trackers[1]));
            break;

            case "STEP_WET":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(wetStep, ref trackers[2]));
            break;

            case "STEP_GRASS":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(grassStep, ref trackers[3]));
            break;

            case "STEP_WOOD":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(woodStep, ref trackers[4]));
            break;

            case "STEP_STONE":
            source.PlayFastUnscaled(GenericUtility<AudioClip>.RandomFromArrayNonRep(stoneStep, ref trackers[5]));
            break;
        }
        //Debug.Log(currentTag + " KOP");
    }


    public void ExecuteStep(){ 
        RaycastHit2D hit2D;
        hit2D = Physics2D.Raycast(transform.position, Vector2.down, castLength, castMask);  //!N At some point this has to change due wall-grabbing etc.
        if(hit2D.collider != null){
            currentTag = hit2D.collider.tag;
        }

        PlayStep();
        //Debug.Log("AWOUBGJ " + hit2D.collider + "  h");
    }

    public void ExecuteClimbStep(){
        RaycastHit2D hit2D;
        hit2D = Physics2D.Raycast(transform.position, transform.right * Mathf.Sign(transform.eulerAngles.y + 1), castLength, castMask);  
        if(hit2D.collider != null){
            currentTag = hit2D.collider.tag;
        }

        PlayStep();
    }

    private void OnDrawGizmos(){
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(transform.position, Vector2.down * castLength);
    }
}
