using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sugar_FlyingSound : MonoBehaviour
{
    [SerializeField] private AudioSource source;

    private void Start(){
        RigAnimationController.Instance.onFlyEnter += PlaySound;
        RigAnimationController.Instance.onFlyExit += StopSound;
    }
    public void PlaySound(){
        source.Play();
    }

    public void StopSound(){
        source.Stop();
    }
}
