using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMixing : MonoBehaviour
{
    [SerializeField] private AudioMixingSettings settings;
    private static AudioMixing instance;
    public static AudioMixing Instance { get => instance; private set => instance = value; }
    [SerializeField] private AudioMixer targetMixer;
    void Awake(){
        instance = this;    //It may happen that it appers multiple times through scenes
        ApplyMixerSettings(settings);
    }

    public void SetMixerFloat(string target, float value){
        targetMixer.SetFloat(target, value);
    }

    public float GetMixerFloat(string key){
        float output = 0;
        targetMixer.GetFloat(key, out output);
        return output;
    }


    private void ApplyMixerSettings(AudioMixingSettings settings_input){
        for (int i = 0; i < settings_input.mixingKeys.Length; i++)
        {
            targetMixer.SetFloat(settings_input.mixingKeys[i], settings_input.parallelVolumes[i]);
        }
    }
}
