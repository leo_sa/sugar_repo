using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MixerOptions", menuName = "Options/AudioMixerOptions", order = 4)]
public class AudioMixingSettings : ScriptableObject
{
    public string[] mixingKeys;
    public float[] parallelVolumes;
}
