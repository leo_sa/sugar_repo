using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnTrigger : MonoBehaviour
{
    [SerializeField] private GameObject enableTarget;
    [SerializeField] private string targetString = "Player";
    private void OnTriggerEnter2D(Collider2D col){
        if(col.CompareTag(targetString))enableTarget.SetActive(true);
    }
}
