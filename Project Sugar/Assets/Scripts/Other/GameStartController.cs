using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartController : MonoBehaviour
{
    [SerializeField] private float cameraStartSpeed, cameraStartSize = 10, cameraTargetize = 30;
    [SerializeField] private Material targetMaterial;
    [SerializeField] private Animator animator;
    [SerializeField] private string animationString;
    [SerializeField] private FadeOut fadeInController;
    [SerializeField] private DialogueObject[] startDisplayDialogue;

    private void Start(){
        DialogueManager.TriggerNewDialogue(startDisplayDialogue, null);
        FadeScreenIn();
        SetScreenSize();
        if(animator == null){
            animator = ServiceLocator.player.GetComponentInChildren<Animator>();
        }
        animator.Play(animationString, 3);
    }


    private void FadeScreenIn(){
        targetMaterial.SetFloat("fadeOutString", 3);
        fadeInController.FadeGameIn();
    }

    private void SetScreenSize(){
        CustomCameraBrain.Instance.SetSizeTargetRaw(cameraStartSize);
        CustomCameraBrain.Instance.SetSizeTarget(cameraTargetize, cameraStartSpeed);
    }

}
