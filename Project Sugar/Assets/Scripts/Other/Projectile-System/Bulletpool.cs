using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulletpool : MonoBehaviour
{
    [SerializeField] private GameObject[] targetBullets;
    [SerializeField] private Bullet[] bullets;   //Both arrays need to be parralel
    private uint currentIndex;
    public void ShootBullet(Vector2 direction){
        EnableBullet(targetBullets[currentIndex], bullets[currentIndex], direction);
    }

    private void EnableBullet(GameObject target, Bullet bullet, Vector2 dir){
        target.SetActive(true);
        bullet.Initialize(dir);

        currentIndex += 1;
        if(currentIndex >= targetBullets.Length - 1){
            currentIndex = 0;
        }
    }
}
