using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private ResetTrigger optionalReset;
    [SerializeField] private GameObject onHitEnter;
    [SerializeField]private float speed = .2f;
    private Vector2 direction = Vector2.right;
    public Vector2 Direction { get => direction; set => direction = value; }
    private float lifeTime = 30;
    private Vector3 originalPosition;
    private void OnEnable(){
        Invoke("Reset", lifeTime);
    }
    private void Start(){
        originalPosition = transform.localPosition;
        if(optionalReset != null){
            optionalReset.onReset = EffectReset;
        }
    }
    public void Initialize(Vector2 targetDirection){
        direction = targetDirection;
        Reset();
    }

    private void LoadData(){
        //Some day this data should be filled with a scriptable-object
    }

    private void Update(){
        transform.position += new Vector3(direction.x, direction.y, 0) * speed * Time.deltaTime * 100;
    }

    private void Reset(){
        transform.localPosition = originalPosition;
    }

    private void HardReset(){
        Reset();
        gameObject.SetActive(false);
    }

    private void EffectReset(){
        GameObject cached = Instantiate(onHitEnter, transform.position, Quaternion.identity);
        cached.transform.rotation = Quaternion.FromToRotation(cached.transform.eulerAngles, -transform.forward);
        //This means player will die
        var particleSystem = cached.GetComponentInChildren<ParticleSystem>().main;
        particleSystem.useUnscaledTime = true;
        HardReset();
    }

    //Yes, instantiate is pretty un-neat, but *time* n stuff
    private void OnCollisionEnter2D(Collision2D collision2D){
        if(onHitEnter != null){
            GameObject cached = Instantiate(onHitEnter, collision2D.contacts[0].point, Quaternion.identity);
            cached.transform.rotation = Quaternion.FromToRotation(cached.transform.eulerAngles, -collision2D.contacts[0].normal);
            if(collision2D.gameObject.CompareTag("Player")){
                //This means player will die
                var particleSystem = cached.GetComponentInChildren<ParticleSystem>().main;
                particleSystem.useUnscaledTime = true;
            }
        }
        HardReset();
    }
}
