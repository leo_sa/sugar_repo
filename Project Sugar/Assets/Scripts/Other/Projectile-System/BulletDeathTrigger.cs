using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDeathTrigger : ResetTrigger
{
    //Imma go to the toilert
    //You prolly rember but disable all bullets/stop them or stop time or something when dying n stuff
    protected override void Start(){
        base.Start();
        onTriggered += StopOtherBullets;
    }

    private void StopOtherBullets(){
        TimeManager.SetTime(0.01f); //Just not zero for now
    }
}
