using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    [SerializeField, Range(0, 1)] private float lerpSpeed = 0.1f;
    [SerializeField] private Transform player;
    [SerializeField] private Vector2 intensityMultiplier = new Vector2(1, 1);

    private Vector3 AddInputPosition(){
        return new Vector3(Input.GetAxisRaw("Horizontal") * intensityMultiplier.x, Input.GetAxisRaw("Vertical") * intensityMultiplier.y, 0);
    }

    void Update(){
        transform.position = Vector3.Lerp(transform.position, player.position + AddInputPosition(), lerpSpeed * Time.deltaTime * 100);
    }
}
