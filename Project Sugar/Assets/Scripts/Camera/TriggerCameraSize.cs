using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCameraSize : SmoothTriggerStay
{
    [SerializeField, Tooltip("around 35 or something is the original. Leave 0 for reset")] private float target;
    [SerializeField] private float speed = 0.5f;
    [SerializeField] private bool justResetSize;
    private void Start(){
        onTriggered += SetCamSize;
    }

    private void SetCamSize(){
        if(!justResetSize) CustomCameraBrain.Instance.SetSizeTarget(target, speed);
        else CustomCameraBrain.Instance.ResetSizeTarget();
    }

}
