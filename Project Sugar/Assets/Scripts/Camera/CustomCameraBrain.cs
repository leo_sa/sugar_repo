using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCameraBrain : MonoBehaviour
{
    private static CustomCameraBrain instance;

    public static CustomCameraBrain Instance { get => instance; set => instance = value; }

    [SerializeField] private Camera[] cameras;
    public Camera camRef { get => cameras[1]; } //1 is the ref cam
    private float originalSize; //Both Cameras should have the same size
    private float currentTargetSize;
    private float refFloatVel = 1;    //For smoothdamo
    private float speed = 0.1f;
    private float currentZ, zLerpSpeed = 0.1f;

    void Awake()
    {
        instance = this;
        originalSize = cameras[0].orthographicSize;
        currentTargetSize = originalSize;
    }

    private void Update(){
        if(cameras[0].orthographicSize != currentTargetSize){
            foreach(Camera cam in cameras){
                cam.orthographicSize = Mathf.SmoothStep(cam.orthographicSize, currentTargetSize, speed * Time.unscaledDeltaTime * 100);
                Debug.Log(speed + "SPED");
            }
        }
        if(transform.eulerAngles.z != currentZ){
            Vector3 newTarget = new Vector3(0, 0, currentZ); 
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(newTarget), zLerpSpeed * Time.unscaledDeltaTime * 100); 
        }
    }
    public void SetSizeTarget(float target, float targetSpeed){
        SetCamScaleTargets(target);
        speed = targetSpeed;
    }
    public void SetSizeTargetRaw(float target){
        foreach(Camera cam in cameras){
                cam.orthographicSize = target;
            }
    }
    public void ResetSizeTarget(){
        SetCamScaleTargets(originalSize);
        speed = 0.1f;
    }
    private void SetCamScaleTargets(float target){
        currentTargetSize = target;
    }

    public void SetRot(float Input, float speed){
        currentZ = Input;
        zLerpSpeed = speed;
    }

    public void SetRotRaw(float Input){
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, currentZ));
        currentZ = Input;
        zLerpSpeed = 0.1f;  //!MAGIC NUMBER (default i guess)
    }

}
