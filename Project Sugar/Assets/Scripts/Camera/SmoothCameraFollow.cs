using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCameraFollow : MonoBehaviour
{
#region Variables

    [SerializeField] private Vector2 movementOffsetMultiplier = new Vector2(1, 1);
    [SerializeField] private Vector3 offset;
    [SerializeField, Range(0, 0.5f)] private float smoothness;
    [SerializeField] private Transform target;

    [SerializeField] private float clampDistance = 15;

    //Properties
    public Transform Target { get => target; set => target = value; }
    public Vector3 Offset { get => offset; set => offset = value; }
    private Vector3 smoothDampVelocity = Vector3.zero;
    private Vector3 screenMid;
    
    #endregion


    #region UnityMethods

    void Awake()
    {
        ServiceLocator.smoothCam = this;
        ServiceLocator.refCam = GetComponentInChildren<Camera>();   //!Maybe change this if using custom buffers
    }

    float targetX, targetY;
    void FixedUpdate()
    {

        if(Input.GetAxisRaw("Horizontal") != 0){
            targetX = Input.GetAxis("Horizontal");
        }
        else{
            targetX = Mathf.Lerp(targetX, 0, 0.4f * Time.deltaTime * 100);
        }

        Vector3 moveOffset = new Vector3(targetX * movementOffsetMultiplier.x, Input.GetAxis("Vertical") * movementOffsetMultiplier.y, 0);
        //transform.position = Vector3.Lerp(transform.position, TargetCamPosition(), smoothness * Time.deltaTime * 50) + moveOffset;
        //transform.position = Vector3.Lerp(transform.position, TargetCamPosition(), smoothness * 10) + moveOffset;
        transform.position = Vector3.SmoothDamp(transform.position, TargetCamPosition(), ref smoothDampVelocity, smoothness);
        Vector3 distance = transform.position - TargetCamPosition();
        distance = Vector3.ClampMagnitude(distance, clampDistance);
        transform.position = TargetCamPosition() + distance;
    }

    private void ClampPos(){
    }


    void OnDrawGizmosSelected(){    //draws current target pos

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(TargetCamPosition() + new Vector3(0, 0, -transform.position.z), 0.5f);
    }


#endregion


#region CustomMethods

    public void ChangePosition(Vector3 target){
        transform.position = target;
    }
    private Vector3 TargetCamPosition() //Basically target but with locked Z axis
    {        
        return new Vector3(target.position.x, target.position.y, transform.position.z) + offset;
    }

    
    public IEnumerator Shake(float duration, float magnitude)
    {
        //magnitude *= 5; //Because of the updated code, it seems (literally) like there is less screenshake, so this is a lazy compensationo
        magnitude *= 1.5f;

        Vector3 originalPos = transform.localPosition;
        Vector3 addedPosition = Vector3.zero;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-3f, 3f) * magnitude;

            float y = Random.Range(-3f, 3) * magnitude;

            transform.localPosition += new Vector3(x, y, 0);

            elapsed += Time.unscaledDeltaTime;      

            yield return null;

            //transform.localPosition = originalPos;
            transform.localPosition -= new Vector3(x, y, 0);
            addedPosition += new Vector3(x, y, 0);
            //A problem that arises here ist, that the frame basically stops "following
        }

        transform.localPosition += addedPosition;

    }

 #endregion
}
