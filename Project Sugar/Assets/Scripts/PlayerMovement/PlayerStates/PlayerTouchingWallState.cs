using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouchingWallState : PlayerState
{
    protected bool isGrounded;
    protected bool isTouchingWall;
    protected bool grabInput;
    protected bool jumpInput;
    protected bool isTouchingLedge;
    protected bool isTouchingMoss;
    protected bool isTouchingTreesap;
    protected bool shouldDie;
    protected bool isJumpingShroomed;

    protected int xInput;
    protected int yInput;

    protected string tagString;

    //------------------------------SLOPES-------------------------------------------

    protected RaycastHit2D verticalSlopeHit;
        
    protected Vector2 wallCheckPosition;
    protected Vector2 wallCheckHitPoint;

    public Vector2 slopeNormalPerp; //perpendicular    

    protected bool isOnSlope;
    protected bool canClimbOnSlope;

    private float slopeDownAngle;
    private float slopeSideAngle;
    private float lastSlopeAngle;

    public PlayerTouchingWallState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void AnimationFinishTrigger()
    {
        base.AnimationFinishTrigger();
    }

    public override void AnimationTrigger()
    {
        base.AnimationTrigger();
    }

    public override void DoChecks()
    {
        base.DoChecks();

        isGrounded = player.CheckIfGrounded();
        isTouchingWall = player.CheckIfTouchingWall();
        wallCheckHitPoint = player.wallCheckHit; // ------------------------------------
        isTouchingLedge = player.CheckIfTouchingLedge();
        isTouchingMoss = player.CheckIfTouchingMoss();
        isTouchingTreesap = player.CheckIfTouchingTreesap();
        shouldDie = player.CheckIfShouldDie();
        isJumpingShroomed = player.CheckIfTouchingJumpingShroom();

        //if (!isTouchingWall && isTouchingLedge)
        //{
        //    player.LedgeClimbState.SetDetectedPosition(player.transform.position);
        //}
        
        wallCheckPosition = player.CheckIfOnCurvedWall();
        SlopeCheckHorizontal(wallCheckPosition);
        SlopeCheckVertical(wallCheckPosition);
    }

    public override void Enter()
    {
        base.Enter();

        player.InAirState.glideTimer = 0f;
        player.InAirState.startGlideTimer = false;
        player.InAirState.highPt = false;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;
        grabInput = player.InputHandler.GrabInput;
        jumpInput = player.InputHandler.JumpInput;

        
        if (isTouchingMoss)
        {
            if (yInput > 0 && grabInput) //Player is climbing
            {
                if (wallCheckHitPoint != null)
                {
                    if(player.FacingDirection > 0)
                    {
                        wallCheckHitPoint += new Vector2(-playerData.distanceToWall, playerData.distanceToWall);
                    }
                    else if (player.FacingDirection < 0)
                    {
                        wallCheckHitPoint += new Vector2(playerData.distanceToWall, playerData.distanceToWall);
                    }
                    player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
                }
                stateMachine.ChangeState(player.WallClimbState);
            }
            else if (player.CurrentVelocity.y <= 0 && !grabInput) //wall sliding
            {
                if (wallCheckHitPoint != null)
                {
                    if (player.FacingDirection > 0)
                    {
                        wallCheckHitPoint += new Vector2(-playerData.distanceToWall, playerData.distanceToWall);
                    }
                    else if (player.FacingDirection < 0)
                    {
                        wallCheckHitPoint += new Vector2(playerData.distanceToWall, playerData.distanceToWall);
                    }
                    player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f * Time.deltaTime);
                }

                stateMachine.ChangeState(player.WallSlideState);
            }
            else if (jumpInput) //wall jump
            {
                player.WallJumpState.DetermineWallJumpDirection(isTouchingWall);
                stateMachine.ChangeState(player.WallJumpState);
            }
            else if (isGrounded && !grabInput) //letting go on ground
            {
                stateMachine.ChangeState(player.IdleState);
            }
            else if (!isTouchingMoss || !grabInput)  //letting go in air
            {
                stateMachine.ChangeState(player.InAirState);
            }
            

            if (shouldDie)
            {
                stateMachine.ChangeState(player.DeathState);
            }

        }
        else if (isTouchingTreesap)
        {
            //if (wallCheckHitPoint != null)
            //{
            //    player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
            //}

            if (jumpInput)
            {
                player.WallJumpState.DetermineWallJumpDirection(isTouchingTreesap);
                stateMachine.ChangeState(player.WallJumpState);
            }
        }

        if (shouldDie)
        {
            stateMachine.ChangeState(player.DeathState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        if (!isTouchingWall && !isGrounded && isTouchingLedge) //Stuck on the Wall
        {
            //stateMachine.ChangeState(player.WallSlideState);
            player.SetVelocityX(0);
            player.SetForce(Vector2.down * 1);
        }
    }

    private void SlopeCheckHorizontal(Vector2 position)
    {
        //check for slope before or behind player
        RaycastHit2D slopeHitOver = Physics2D.Raycast(position, player.transform.up, playerData.slopeCheckDistance, playerData.whatIsGround);
        RaycastHit2D slopeHitUnder = Physics2D.Raycast(position, -player.transform.up, playerData.slopeCheckDistance, playerData.whatIsGround);

        if (slopeHitOver)
        {
            isOnSlope = true;

            slopeSideAngle = Vector2.Angle(slopeHitOver.normal, Vector2.up);
        }
        else if (slopeHitUnder)
        {
            isOnSlope = true;

            slopeSideAngle = Vector2.Angle(slopeHitUnder.normal, Vector2.up);
        }
        else //if no slope could be found
        {
            slopeSideAngle = 0.0f;
            isOnSlope = false;
        }
    }

    private void SlopeCheckVertical(Vector2 position)
    {
        //stores the vertical hit
        verticalSlopeHit = Physics2D.Raycast(position, Vector2.right * player.FacingDirection, playerData.slopeCheckDistance, playerData.whatIsGround); //

        if (verticalSlopeHit) //if hit smth
        {
            //90� to the hit
            slopeNormalPerp = Vector2.Perpendicular(verticalSlopeHit.normal).normalized;

            //inbetween the normal and Y Axis or X Axis and slope
            slopeDownAngle = Vector2.Angle(verticalSlopeHit.normal, Vector2.left * player.FacingDirection);

            //is true when standing on slope
            if (slopeDownAngle != lastSlopeAngle)
            {
                isOnSlope = true;
            }

            lastSlopeAngle = slopeDownAngle;

            //Debugs rays
            Debug.DrawRay(verticalSlopeHit.point, -slopeNormalPerp, Color.blue);
            Debug.DrawRay(verticalSlopeHit.point, verticalSlopeHit.normal, Color.green);


            if (slopeDownAngle > playerData.maxSlopeAngle || slopeSideAngle > playerData.maxSlopeAngle) //to fall all the way to the ground
            {
                canClimbOnSlope = false;
            }
            else
            {
                canClimbOnSlope = true;
            }
        }
    }
}
