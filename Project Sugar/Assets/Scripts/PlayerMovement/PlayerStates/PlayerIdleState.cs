using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : PlayerGroundedState
{
    public PlayerIdleState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();     
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        //change state when player starts moving
        if(xInput == 0)
        {
            player.SetVelocityX(0f);
        }
        else if(xInput != 0f && !isExitingState)
        {
            stateMachine.ChangeState(player.MoveState);
        }
        else if(isTouchingMoss && !isGrounded)
        {
            stateMachine.ChangeState(player.WallGrabState);
        }
        else if (grapplingHookInput && hittingHookLayer && playerData.hookIsActivated) //grappling hook
        {
            player.GrapplingHookState.CheckIfHookLayer(hittingHookLayer);
            stateMachine.ChangeState(player.GrapplingHookState);
        }
        else if (!isGrounded && !isTouchingWall)
        {
            stateMachine.ChangeState(player.InAirState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
