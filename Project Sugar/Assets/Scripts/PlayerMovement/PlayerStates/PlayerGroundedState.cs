using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundedState : PlayerState
{
    public delegate void OnGroundedEnter();
    public OnGroundedEnter onGroundedEnter;
    public delegate void OnGroundedExit();
    public OnGroundedExit onGroundedExit;

    protected Vector2 slopeCheckPosition;

    protected Vector2 slopeNormalPerp; //perpendicular    

    protected int xInput;
    protected int yInput;

    protected bool slowWalkInput;
    protected bool isTouchingMoss;
    protected bool isTouchingTreesap;
    protected bool isJumpingShroomed;
    protected bool isTouchingWall;
    protected bool hittingHookLayer;
    protected bool grabInput;
    protected bool isOnSlope;
    protected bool canWalkOnSlope;
    protected bool shouldDie;
    
    protected bool JumpInput;
    private bool isTouchingLedge;
    protected bool grapplingHookInput;
    private bool shroomBounceHasBeenApplied = false;

    private float slopeDownAngle;
    private float slopeSideAngle;
    private float lastSlopeAngle;

    public bool isGrounded;

    protected RaycastHit2D currentSlopeHit;
    private RaycastHit2D slopeSideHit;
    private RaycastHit2D slopeOtherSideHit;

    public PlayerGroundedState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();

        isGrounded = player.CheckIfGrounded();
        isTouchingWall = player.CheckIfTouchingWall();
        isTouchingLedge = player.CheckIfTouchingLedge();
        isTouchingMoss = player.CheckIfTouchingMoss();
        isTouchingTreesap = player.CheckIfTouchingTreesap();
        shouldDie = player.CheckIfShouldDie();

        slopeCheckPosition = player.CheckIfOnSlope();
        SlopeCheckHorizontal(slopeCheckPosition);
        SlopeCheckVertical(slopeCheckPosition);

        player.targetPosition = player.hookCamera.ScreenToWorldPoint(Input.mousePosition);
        player.targetPosition.z = player.hookCamera.nearClipPlane;
        hittingHookLayer = player.CheckIfHittingHookLayer();
    }

    public override void Enter()
    {
        base.Enter();

        //----------------------------For Sound-------------------------------------------------
        onGroundedEnter?.Invoke();

        player.JumpState.ResetAmountOfJumpsLeft();

        player.InAirState.highPt = false;

        player.InAirState.glideTimer = 0f;
        player.InAirState.startGlideTimer = false;
        //player.InAirState.resetDistance = true;
    }

    public override void Exit()
    {
        base.Exit();

        //----------------------------For Sound-------------------------------------------------
        onGroundedExit?.Invoke();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        isJumpingShroomed = player.CheckIfTouchingJumpingShroom();
        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;
        JumpInput = player.InputHandler.JumpInput;
        grabInput = player.InputHandler.GrabInput;
        slowWalkInput = player.InputHandler.SlowWalkInput;
        grapplingHookInput = player.InputHandler.GrapplingHookInput;

        player.CheckIfShouldRotate(player.transform.rotation, playerData.rotateBackOnGroundTime);


        if (JumpInput && player.JumpState.CanJump()) //jumping
        {
            player.InAirState.startGlideTimer = true;
            stateMachine.ChangeState(player.JumpState);
        }
        else if (isJumpingShroomed) //jumping shroom -- ++++++++++++++++++++++++++++++++++++++++++++
        {
            player.InAirState.highPt = false;
            stateMachine.ChangeState(player.BounceState);
        }
        else if (!isJumpingShroomed && player.BounceState.firstJump)
        {
            player.BounceState.firstJump = false;
        }
        else if (!isGrounded) //If the player falls off a cliff
        {
            //if the player is not supposed to jump after falling off a cliff
            player.InAirState.StartAfterCliffTime();
            stateMachine.ChangeState(player.InAirState);
        }
        else if ((isTouchingMoss || isTouchingTreesap) && grabInput && isTouchingLedge && xInput == player.FacingDirection) //wallgrab
        {
            stateMachine.ChangeState(player.WallGrabState);
        }
        else if (grapplingHookInput && hittingHookLayer && playerData.hookIsActivated) //grappling hook
        {
            player.GrapplingHookState.CheckIfHookLayer(hittingHookLayer);
            stateMachine.ChangeState(player.GrapplingHookState);
        }

        if (shouldDie) //death trigger
        {
            stateMachine.ChangeState(player.DeathState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    private void SlopeCheckHorizontal(Vector2 position)
    {
        //check for slope before or behind player
        RaycastHit2D slopeHitFront = Physics2D.Raycast(position, player.transform.right, playerData.slopeCheckDistance, playerData.whatIsGround);
        RaycastHit2D slopeHitBack = Physics2D.Raycast(position, -player.transform.right, playerData.slopeCheckDistance, playerData.whatIsGround);

        if (slopeHitFront)
        {
            isOnSlope = true;

            slopeSideAngle = Vector2.Angle(slopeHitFront.normal, Vector2.up);

            //-------------------------------------------------------------------------------
            //currentSlopeHit = Physics2D.Raycast(position, player.transform.right, playerData.slopeCheckDistance, playerData.whatIsGround);
        }
        else if (slopeHitBack)
        {
            isOnSlope = true;

            slopeSideAngle = Vector2.Angle(slopeHitBack.normal, Vector2.up);
        }
        else //if no slope could be found
        {
            slopeSideAngle = 0.0f;
            isOnSlope = false;
        }
    }

    private void SlopeCheckVertical(Vector2 position)
    {
        //stores the vertical hit
        currentSlopeHit = Physics2D.Raycast(position, Vector2.down, playerData.slopeCheckDistance, playerData.whatIsGround);

        if (currentSlopeHit || slopeSideHit || slopeOtherSideHit) //if hit smth
        {
            //90� to the hit
            slopeNormalPerp = Vector2.Perpendicular(currentSlopeHit.normal).normalized;

            //inbetween the normal and Y Axis or X Axis and slope
            slopeDownAngle = Vector2.Angle(currentSlopeHit.normal, Vector2.up);

            //is true when standing on slope
            if (slopeDownAngle != lastSlopeAngle)
            {
                isOnSlope = true;
            }

            lastSlopeAngle = slopeDownAngle;
                        
            //Debugs rays
            Debug.DrawRay(currentSlopeHit.point, -slopeNormalPerp, Color.blue);
            Debug.DrawRay(currentSlopeHit.point, currentSlopeHit.normal, Color.green);


            if (slopeDownAngle > playerData.maxSlopeAngle || slopeSideAngle > playerData.maxSlopeAngle) //to fall all the way to the ground
            {
                canWalkOnSlope = false;
            }
            else
            {
                canWalkOnSlope = true;
            }

            if (isOnSlope && canWalkOnSlope && xInput == 0.0f) //wenn auf slope
            {
                player.PlayerRigidbody.sharedMaterial = player.noSlidingPhysicsMaterial;
            }
            else // wenn nicht auf slope
            {
                player.PlayerRigidbody.sharedMaterial = player.playerPhysicsMaterial;
            }
        }
    }
}
