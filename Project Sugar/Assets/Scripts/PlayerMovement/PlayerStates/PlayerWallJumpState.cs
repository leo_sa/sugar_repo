﻿using UnityEngine;
using System.Collections;

public class PlayerWallJumpState : PlayerAbilityState
{
    public delegate void OnWallJumpEnter();
    public OnWallJumpEnter onWallJumpEnter;

    private int wallJumpDirection;

    public PlayerWallJumpState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();

        player.InputHandler.UseJumpInput();
        player.JumpState.ResetAmountOfJumpsLeft();
        player.SetJumpVelocity(playerData.wallJumpVelocity, playerData.wallJumpAngle, wallJumpDirection);
        player.CheckIfShouldFlip(wallJumpDirection);
        player.JumpState.DecreaseAmountOfJumpsLeft();

        //----------------------------For Sound-------------------------------------------------
        onWallJumpEnter?.Invoke();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        player.PlayerAnimator.SetFloat("yVelocity", player.CurrentVelocity.y);
        player.PlayerAnimator.SetFloat("xVelocity", Mathf.Abs(player.CurrentVelocity.x));

        if(Time.time >= startTime + playerData.wallJumpTime)
        {
            isAblilityDone = true;
        }      
        
        if (isTouchingWall)
        {
            isAblilityDone = true;
            stateMachine.ChangeState(player.InAirState);
        }
        else if (isGrounded)
        {
            isAblilityDone = true;
            stateMachine.ChangeState(player.IdleState);
        }
        else if (Time.time >= startTime + (playerData.wallJumpTime / 2) && xInput != 0)
        {
            stateMachine.ChangeState(player.InAirState);
        }
    }

    public void DetermineWallJumpDirection(bool isTouchingWall)
    {
        if(isTouchingWall)
        {
            wallJumpDirection = -player.FacingDirection;
        }
        else
        {
            wallJumpDirection = player.FacingDirection;
        }
    }
}
