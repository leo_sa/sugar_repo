﻿using UnityEngine;
using System.Collections;

public class PlayerWallHopState : PlayerAbilityState
{
    private int wallHopDirection;

    public PlayerWallHopState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(!isExitingState)
        {
            startTime = 0f;

            player.SetJumpVelocity(playerData.jumpOffWallVelocity, playerData.jumpOffWallAngle, wallHopDirection);
            
            //player.CheckIfShouldFlip(xInput);

            if (Time.time >= startTime + playerData.jumpOffWallTime)
            {
                isAblilityDone = true;
            }
        }
    }

    public void DetermineWallHopDirection(bool isTouchingWall)
    {
        if (isTouchingWall)
        {
            wallHopDirection = -player.FacingDirection;
        }
        else
        {
            wallHopDirection = player.FacingDirection;
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
