using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallSlideState : PlayerTouchingWallState
{
    public delegate void OnWallSlideStart();
    public OnWallSlideStart onWallSlideStart;
    public delegate void OnWallSlideStop();
    public OnWallSlideStop onWallSlideStop;

    private bool canSlide;

    public PlayerWallSlideState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();
        //----------------------------For Sound-------------------------------------------------
        onWallSlideStart?.Invoke();
    }

    public override void Exit()
    {
        base.Exit();
        //----------------------------For Sound-------------------------------------------------
        onWallSlideStop?.Invoke();
    }

    public override void LogicUpdate()
    {        

        base.LogicUpdate();

        if (!isExitingState)
        {
            //if (isTouchingWall && !isTouchingMoss && !isGrounded && !isJumpingShroomed && xInput == player.FacingDirection)
            //{
            //    player.SetVelocityY(-playerData.wallSlideVelocity);
            //    Debug.Log("I is stuck here");
            //}
            //else

            
            
            if (isGrounded || !isTouchingWall)
            {
                canSlide = false;

                stateMachine.ChangeState(player.IdleState);
            }
            if (isTouchingLedge && (!isTouchingMoss || !isTouchingWall) && !isGrounded)
            {
                player.SetVelocityX(0);
                player.SetForce(Vector2.down * 1);
            }
            else if (!isTouchingWall && !isGrounded) // when letting go in air
            {
                canSlide = false;

                stateMachine.ChangeState(player.InAirState);
            }            
            else if (isJumpingShroomed) //no sliding on jumping shrooms
            {
                canSlide = false;

                stateMachine.ChangeState(player.InAirState);
            }
            else if (isTouchingWall && !isTouchingMoss && xInput != player.FacingDirection) //Holds wrong button
            {
                canSlide = false;

                player.WallHopState.DetermineWallHopDirection(isTouchingWall);
                stateMachine.ChangeState(player.WallHopState);
            }

            if (isTouchingMoss)
            {
                if ((yInput < 0 || !grabInput) && !isGrounded)
                {
                    player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.FromToRotation(player.transform.right, -verticalSlopeHit.normal) * player.transform.rotation, 8 * Time.deltaTime);

                    //player.SetVelocityY(-playerData.wallSlideVelocity);
                    canSlide = true;
                }
                else if (grabInput && xInput == player.FacingDirection) //yInput == 0 &&
                {
                    canSlide = false;
                    stateMachine.ChangeState(player.WallGrabState);
                }
                else if (grabInput && xInput != player.FacingDirection) //Holds wrong button
                {
                    canSlide = false;

                    player.WallHopState.DetermineWallHopDirection(isTouchingMoss);
                    stateMachine.ChangeState(player.WallHopState);
                }

                if (jumpInput) //wall jump
                {
                    canSlide = false;

                    player.WallJumpState.DetermineWallJumpDirection(isTouchingMoss);
                    stateMachine.ChangeState(player.WallJumpState);
                }
            }            
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        if(canSlide)
        {
            player.SetVelocity(player.FacingDirection * -playerData.wallSlideVelocity * -slopeNormalPerp.x, -playerData.wallSlideVelocity); //!!!! * -playerData.wallSlideVelocity !!! nur ge�ndert, um Bug zu finden
        }
    }
}
