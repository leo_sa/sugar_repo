﻿using UnityEngine;
using System.Collections;

public class PlayerDeathState : PlayerState
{
    private Vector3 position;
    private bool loadedCheckpoint;

    public PlayerDeathState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();

        Vector3 cached = Vector3.zero;
        position = SavingSystem<Vector3>.LoadData(cached, "saveCheckpointPosition");

        player.transform.position = position;
        loadedCheckpoint = true;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        
        if(loadedCheckpoint)
        {
            loadedCheckpoint = false;
            stateMachine.ChangeState(player.IdleState);
        }
    }
}
