﻿using UnityEngine;
using System.Collections;

public class PlayerBounceState : PlayerInAirState
{
    public bool firstJump;

    public float newHighestBounce;

    public PlayerBounceState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();
        
        
        if (newHighestBounce <= playerData.smallestShroomJump)
        {
            firstJump = true;
        }
        else
        {
            firstJump = false;
        }
        
        if(firstJump)
        {
            player.SetVelocityY(playerData.shroomJumpVelocity);
        }

        if (addVelocity && newHighestBounce < playerData.maxBounceHeigth)
        {
            float currentBounceHeigth = playerData.shroomJumpVelocity - playerData.smallestShroomJump + newHighestBounce;
            player.SetVelocityY(currentBounceHeigth + playerData.shroomHeigthAdd);
        }
        else if (reduceVelocity && !firstJump || newHighestBounce >= playerData.maxBounceHeigth)
        {
            float currentBounceHeigth = playerData.shroomJumpVelocity - playerData.smallestShroomJump + newHighestBounce + 3;
            player.SetVelocityY(currentBounceHeigth - playerData.shroomHeigthReduce);
        }
    }
    
    // man muss den ganzen Weg nach unten S drücken -> S loslassen verlangsamt ihn aber nicht

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    

    // mehrere Shrooms im Radius
    // -> Distance zu allen berechnen
    // -> Erkennen zu welchem Shroom welche Distance gehört
    // --> Mit Array durch alle shrooms loopen

    // delay nach dem Sprung, damit man nicht noch ausversehen S drückt
    // -> Delay selbe Reichweite wie Ausgangshöhe (etwas kleiner)
    // --> Nach aufkommen delay berechnen
    // --> ENTWEDER mit Time.deltaTime
    // --> ODER mit der direkten Höhe
}
