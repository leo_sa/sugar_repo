using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLandState : PlayerGroundedState
{

    public PlayerLandState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (!isExitingState)
        {
            if (isOnSlope)
            {
                
            }

            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.FromToRotation(-player.transform.up, -currentSlopeHit.normal) * player.transform.rotation, 8 * Time.deltaTime);

            if (xInput != 0)
            {
                player.CheckIfShouldFlip(xInput);
                player.SetVelocityX(playerData.momentumVelocity * xInput);
                player.MoveState.lastXinput = xInput;
                stateMachine.ChangeState(player.MoveState);
            }
            else if (isAnimationFinished)
            {
                stateMachine.ChangeState(player.IdleState);
            }
        }
    }
}
