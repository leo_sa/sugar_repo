﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerGrapplingHookState : PlayerAbilityState
{
    public delegate void OnHookLeave();
    public OnHookLeave onHookLeave;
    public delegate void OnHookHit();
    public OnHookLeave onHookHit;

    private int newXInput;
    private int lastXInput;

    private bool hittingHookLayer;
    private bool hookInput;
    private bool hitPointEmpty;
    private bool hookHasTwoPoints;
    private bool hookHasFinalLength;
    private bool addedVelocity;
    private bool reachedMaxSwinging;
    private bool hookIsTooClose;
    private bool addedOnce;
    private bool lowestPointReached;
    private bool addedLength;
    private bool hitIsHook;

    private bool firstLerp;
    private bool secondLerp;
    private bool addedVelToStartSwinging;

    private float currentHookLength;
    private float hookPullingTime = 10f;
    private float missingLength;
    private float lengthToAddTo;
    private float newLength;
    private float originalHitLength;

    private Vector2 lowestPointVelocity;
    private Vector2 lowestPointPosition;

    private RaycastHit2D hit;
    private RaycastHit2D hittingOtherLayer;

    public PlayerGrapplingHookState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
        ServiceLocator.grappleState = this;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public void CheckIfHookLayer(bool checkedLayer)
    {
        hittingHookLayer = checkedLayer;
    }

    public override void Enter()
    {
        base.Enter();
                
        hitPointEmpty = true;
        hookHasFinalLength = false;        
        player.DistanceJoint.autoConfigureDistance = true;

        Physics2D.queriesHitTriggers = false;
        hittingOtherLayer = Physics2D.Raycast(player.transform.position, player.targetPosition - player.transform.position, playerData.maxThrowLength, ~player.playerLayer); // ~ to ignore the layer
        
        if (hittingOtherLayer)
        {
            if (hittingOtherLayer.collider.gameObject.layer == 9) //layer 9 is grapplingHook Layer
            {
                hitIsHook = true;
            }
            else
            {
                hitIsHook = false;
            }
        }

        LogicUpdate();

        if(hitIsHook)
        {
            onHookHit?.Invoke();
        }
    }

    public override void Exit()
    {
        base.Exit();

        //leaving this in here in case I want to work with triggers
        Physics2D.queriesHitTriggers = true;

        hookPullingTime = 10f;
        hookHasTwoPoints = false;
        hookHasFinalLength = false;
        addedVelocity = false;
        addedLength = false;
        hitPointEmpty = true;
        addedOnce = false;
        hookIsTooClose = false;
        hitIsHook = false;
        addedVelToStartSwinging = false;

        player.DistanceJoint.autoConfigureDistance = true;
        player.DistanceJoint.enabled = false;
        player.grapplingHookLine.enabled = false;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        
        newXInput = player.InputHandler.NormalizedInputX;
        hookInput = player.InputHandler.GrapplingHookInput;
        

        if (hittingHookLayer && hitIsHook)
        {
            player.DistanceJoint.enabled = true;
            player.DistanceJoint.connectedBody = player.hookHit.collider.gameObject.GetComponent<Rigidbody2D>();
            player.DistanceJoint.connectedAnchor = player.hookHit.point - new Vector2(player.hookHit.collider.transform.position.x, player.hookHit.collider.transform.position.y);
            
            if (hitPointEmpty)
            {
                player.grapplingHookLine.SetPosition(0, player.gapplingHookPosition.transform.position);
                player.grapplingHookLine.SetPosition(1, player.hookHit.point);
                hookHasTwoPoints = true;
                player.grapplingHookLine.enabled = true;

                hitPointEmpty = false;
            }

            player.grapplingHookLine.SetPosition(0, player.gapplingHookPosition.transform.position);
        }

        if (hookHasTwoPoints && !isGrounded && hitIsHook)
        {
            if(!hookIsTooClose)
            {
                currentHookLength = player.DistanceJoint.distance;
            }
            
            HookDistance();
        }

        if (hookHasFinalLength || hookIsTooClose) //if reached final hook distance
        {
            if (player.CurrentVelocity.y >= -1 && player.CurrentVelocity.y <= 1 && !(player.CurrentVelocity.x >= -1 && player.CurrentVelocity.x <= 1))
            {
                lowestPointVelocity = player.CurrentVelocity;
                lowestPointPosition = player.transform.position;
                lowestPointReached = true;
                
                if (!addedVelocity) //Momentum swinging
                {
                    //+ rechts
                    //- links
                    //player.CurrentVelocity.x >= -0.5 && player.CurrentVelocity.x <= 0 -> going left
                    //player.CurrentVelocity.x <= 0.5 && player.CurrentVelocity.x >= 0 ->going right
                    
                    if (xInput > 0 && lowestPointVelocity.x > 0 && lowestPointVelocity.x < playerData.maxSwinging) //right while moving right - if +0 is smaller than 80
                    {
                        player.SetVelocityX(lowestPointVelocity.x + playerData.swingStrength);
                        Debug.Log("I should go faster right");
                        addedVelocity = true;
                    }
                    else if (xInput > 0 && lowestPointVelocity.x < 0 && lowestPointVelocity.x < -playerData.minSwinging) //right while moving left - if -0 is smaller than -20
                    {
                        player.SetVelocityX(lowestPointVelocity.x + playerData.swingStrength);
                        Debug.Log("I should go slower right");
                        addedVelocity = true;
                    }
                    else if (xInput < 0 && lowestPointVelocity.x < 0 && lowestPointVelocity.x > -playerData.maxSwinging) //left while moving left - if -0 is bigger than -80
                    {
                        player.SetVelocityX(lowestPointVelocity.x - playerData.swingStrength);
                        Debug.Log("I should go faster left");
                        addedVelocity = true;
                    }
                    else if (xInput < 0 && lowestPointVelocity.x > 0 && lowestPointVelocity.x > playerData.minSwinging) //left while moving right - if +0 is bigger than 20
                    {
                        player.SetVelocityX(lowestPointVelocity.x - playerData.swingStrength);
                        Debug.Log("I should go slower left");
                        addedVelocity = true;
                    }                    
                }
            }

            //selber schwingen, wenn der Player am tiefsten Punkt stehen bleibt
            if (player.CurrentVelocity.x < 0 && player.CurrentVelocity.x > -1f && !addedVelocity && !addedVelToStartSwinging)
            {
                //basically if -0 is bigger than -5
                //player.SetVelocityX(-2);
                player.SetVelocityX(xInput * playerData.swingStrength);

                if (xInput != 0)
                {
                    addedVelToStartSwinging = true;
                }
            }
            else if (player.CurrentVelocity.x > 0 && player.CurrentVelocity.x < 1f && !addedVelocity && !addedVelToStartSwinging)
            {
                //and + 0 smaller than +5
                player.SetVelocityX(xInput * playerData.swingStrength);

                if (xInput != 0)
                {
                    addedVelToStartSwinging = true;
                }
            }

            if (player.CurrentVelocity.x >= -1 && player.CurrentVelocity.x <= 1)
            {
                addedVelocity = false;
                lowestPointReached = false;
                addedLength = false;
            }
        }

        if(newXInput != 0)
        {
            lastXInput = newXInput;
        }

        if (!hookInput || !hittingHookLayer || !hitIsHook)
        {
            //----------------------------For Sound-------------------------------------------------
            onHookLeave?.Invoke();
            

            if(player.CurrentVelocity.x > 0)
            {
                float newXVelToAdd = player.CurrentVelocity.x; // + playerData.swingStrength;
                player.SetVelocity(newXVelToAdd, player.CurrentVelocity.y + playerData.swingStrength);
            }
            else if (player.CurrentVelocity.x < 0)
            {
                float newXVelToAdd = player.CurrentVelocity.x - playerData.swingStrength;
                player.SetVelocity(newXVelToAdd, player.CurrentVelocity.y + playerData.swingStrength);
            }

            player.InAirState.inAirFromGrapplingState = true;
            stateMachine.ChangeState(player.InAirState);
        }

        if (isGrounded && hitIsHook)
        {
            player.DistanceJoint.autoConfigureDistance = false;
            player.DistanceJoint.maxDistanceOnly = true;

            //Flipping player when needed
            player.CheckIfShouldFlip(xInput);

            //only walking slow allowed
            player.SetVelocityX(playerData.slowWalkVelocity * xInput);

            if (jumpInput)
            {
                player.SetVelocityY(playerData.jumpVelocity);
            }
        }
        else
        {
            player.DistanceJoint.maxDistanceOnly = false;
            //player.DistanceJoint.autoConfigureDistance = true;
        }
    }

    private void HookDistance()
    {        
        hookPullingTime -= Time.deltaTime;
        
        if (currentHookLength > (playerData.maxLineLength + 0.5f)) //line long
        {
            player.DistanceJoint.autoConfigureDistance = true;
            player.DistanceJoint.distance--;
            hookIsTooClose = false;
        }
        else if (currentHookLength < (playerData.maxLineLength - 0.5f) && currentHookLength >= playerData.minLineLength) //line short
        {
            player.DistanceJoint.autoConfigureDistance = false;
            hookIsTooClose = true;
            HookShortDistance();
        }
        else if (currentHookLength < playerData.minLineLength) //line too short
        {
            player.DistanceJoint.autoConfigureDistance = true;
            player.DistanceJoint.distance++;
        }
        else //given lenght
        {
            player.DistanceJoint.autoConfigureDistance = false;
            hookIsTooClose = false;
            hookHasFinalLength = true;
        }
    }

    private void HookShortDistance()
    {
        if (!addedOnce)
        {
            originalHitLength = Vector3.Distance(player.transform.position, lowestPointPosition);

            currentHookLength = player.DistanceJoint.distance;
            missingLength = playerData.maxLineLength - currentHookLength;

            missingLength = missingLength / 2;
            newLength = currentHookLength + missingLength;

            firstLerp = true;
            secondLerp = false;
            addedOnce = true;
        }

        if (addedOnce && !addedLength) //&& lowestPointReached
        {
            if (player.CurrentVelocity.y < 0) //only when moving down
            {
                if(player.DistanceJoint.distance < newLength && !secondLerp && firstLerp) //first stretch
                {
                    float t = 1 - Vector3.Distance(player.transform.position, lowestPointPosition) / originalHitLength; 
                    player.DistanceJoint.distance = Mathf.SmoothStep(player.DistanceJoint.distance, newLength, t ); 
                }
                else if (player.DistanceJoint.distance >= newLength - 0.5 && !firstLerp || player.DistanceJoint.distance <= newLength + 0.5 && !firstLerp) //second stretch
                {
                    float t = 1 - Vector3.Distance(player.transform.position, lowestPointPosition) / originalHitLength;  
                    player.DistanceJoint.distance = Mathf.SmoothStep(player.DistanceJoint.distance, playerData.maxLineLength, t);
                }
            }

            if(player.CurrentVelocity.y > 0)
            {
                if (player.DistanceJoint.distance >= newLength - 0.5)
                {
                    firstLerp = false;
                    secondLerp = true;
                }
            }

            if (player.DistanceJoint.distance >= playerData.maxLineLength - 0.5f)
            {
                player.DistanceJoint.distance = playerData.maxLineLength;
                addedLength = true;
            }
        }
    }
}
