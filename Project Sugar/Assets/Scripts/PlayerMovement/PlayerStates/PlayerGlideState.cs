﻿using UnityEngine;
using System.Collections;

public class PlayerGlideState : PlayerInAirState
{
    private bool newGlideInput; //EXPOSE AND USE AS BOOL
    public bool NewGlideInput { get => newGlideInput; private set => newGlideInput = value; }
    private bool grapplingHookInput;
    private bool canMomentumJump;
    private bool isMomentumFlying;

    private float momentumTimerWorkspace;

    private int xInput;
    //private int yInput;
    private float originalMass;
    private float originalGravity;
    private float originalDrag;
    private Vector2 movementDir;

    private float startMomentumTimer;


    public PlayerGlideState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }
    
    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();

        isMomentumFlying = false;

        originalMass = player.PlayerRigidbody.mass;
        originalGravity = player.PlayerRigidbody.gravityScale;
        originalDrag = player.PlayerRigidbody.drag;
        
        player.SetPlayerGlideSettings(playerData.glideWeight, playerData.glideTurnGravity, playerData.glideTurnDrag);
    }

    public override void Exit()
    {
        base.Exit();
        player.SetPlayerGlideSettings(originalMass, originalGravity, originalDrag);
        isGliding = false;
        glideTimer = 0;
        startGlideTimer = false;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;
        newGlideInput = player.InputHandler.GlideInput;
        grapplingHookInput = player.InputHandler.GrapplingHookInput;

        isTouchingWall = player.CheckIfTouchingWall();
        

        player.CheckIfShouldFlip(xInput);
        CheckForceDirection();

        //move player in direction they are facing
        player.SetForce(movementDir * (playerData.glideVelocity * Time.deltaTime));
        isGliding = true;

        if (!newGlideInput && !isGrounded && !isTouchingWall && player.allowStateToChange) //stop pressing button in air
        {
            player.SetPlayerGlideSettings(originalMass, originalGravity, originalDrag);
            glidingInAir = true;
            player.InAirState.inAirFromGrapplingState = true;
            stateMachine.ChangeState(player.InAirState);
        }

        //AddForce für Momentum nach unten 
        if (yInput < 0) //if pressing down, fly faster
        {
            Debug.Log("Pressing down");

            if (isMomentumFlying)
            {
                startMomentumTimer = 0;
                isMomentumFlying = false;
            }

            player.SetForce((movementDir + Vector2.down) * (playerData.momentumVelocity * Time.deltaTime));
            startMomentumTimer += Time.deltaTime;

            momentumTimerWorkspace = 0f;
            canMomentumJump = true;
        }
        else //if letting go after pressing down
        {
            if (canMomentumJump)
            {
                Debug.Log("Stopped Pressing down");

                momentumTimerWorkspace += Time.deltaTime;

                if (momentumTimerWorkspace <= playerData.momentumTimer)
                {
                    player.SetForce((movementDir + Vector2.up) * (playerData.momentumVelocity * startMomentumTimer * Time.deltaTime));
                    isMomentumFlying = true;

                    if (!newGlideInput || isGrounded) //if touching ground or letting go of button
                    {
                        isMomentumFlying = false;
                        momentumTimerWorkspace = playerData.momentumTimer;
                        startMomentumTimer = 0;
                    }
                }
                else
                {
                    isMomentumFlying = false;
                    Debug.Log("Momentum cooldown");
                    canMomentumJump = false;
                    startMomentumTimer = 0;
                }
            }
        }

        if (grapplingHookInput && hittingHookLayer && playerData.hookIsActivated) //grappling hook
        {
            stateMachine.ChangeState(player.GrapplingHookState);
            Debug.Log("Am hooking");
        }

        if (isGrounded || jumpAfterCliffTime || isTouchingWall) //leave as soon as touches the ground/wall -- !glideInput || 
        {
            Debug.Log("I am leaving the glide state");
            glidingInAir = false;
            Exit();
        }
    }

    private void CheckForceDirection()
    {
        if(player.FacingDirection == 1) //If facing right
        {
            movementDir = Vector2.right;
        }
        else //if facing left
        {
            movementDir = Vector2.left;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
