using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpState : PlayerAbilityState
{
    public delegate void OnJump();
    public OnJump onJump;

    private int amountOfJumpsLeft;
    private string layer;

    private bool jumpOnce;

    public PlayerJumpState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
        ServiceLocator.jumpState = this;
        amountOfJumpsLeft = playerData.amountOfJumps;
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();

        jumpOnce = true;

        PhysicsUpdate();
        
        isAblilityDone = true;
        amountOfJumpsLeft--;
        player.InputHandler.UseJumpInput();
        //player.InAirState.resetDistance = true;

        //----------------------------For Sound-------------------------------------------------
        onJump?.Invoke();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        if(jumpOnce)
        {
            player.SetVelocityY(playerData.jumpVelocity);
            jumpOnce = false;
        }
    }

    public bool CanJump()
    {
        if(amountOfJumpsLeft > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public void ResetAmountOfJumpsLeft() => amountOfJumpsLeft = playerData.amountOfJumps;

    public void DecreaseAmountOfJumpsLeft() => amountOfJumpsLeft--;

    
}
