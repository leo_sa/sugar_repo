using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilityState : PlayerState
{
    protected bool isAblilityDone;
    protected bool tagString;
    protected bool isJumpingShroomed;
    protected bool isTouchingMoss;
    protected bool isGrounded;
    protected bool isTouchingWall;
    protected bool inSlowDownBush;
    protected bool jumpInput;
    protected bool glideInput;
    protected bool isPressingGlideInput;

    protected int xInput;
    protected int yInput;

    protected Vector2 currentVelocity;


    public PlayerAbilityState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();

        
        isJumpingShroomed = player.CheckIfTouchingJumpingShroom();
        isTouchingMoss = player.CheckIfTouchingMoss();
        isTouchingWall = player.CheckIfTouchingWall();
    }

    public override void Enter()
    {
        base.Enter();

        isAblilityDone = false;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        isGrounded = player.CheckIfGrounded();
        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;
        inSlowDownBush = player.CheckIfInSlowDownBush();
        jumpInput = player.InputHandler.JumpInput;
        glideInput = player.InputHandler.GlideInput;

        if(glideInput)
        {
            isPressingGlideInput = true;
        }
        else
        {
            isPressingGlideInput = false;
        }
        
        if (isAblilityDone)
        {
            if (isGrounded && xInput == 0 && !isTouchingMoss) // && player.CurrentVelocity.y < 0.01f
            {
                stateMachine.ChangeState(player.IdleState);
            }
            else if (isGrounded && xInput != 0 && !isTouchingMoss)
            {
                stateMachine.ChangeState(player.MoveState);
            }
            else
            {
                stateMachine.ChangeState(player.InAirState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
