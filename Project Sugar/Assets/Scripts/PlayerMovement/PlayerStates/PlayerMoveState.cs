using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveState : PlayerGroundedState
{
    public delegate void OnRunEnter();
    public OnRunEnter onRunEnter;

    private float currentSpeed;

    private bool notSlowWalking;
    private bool isFlipping;

    private bool hasBeenRunnng = false;
    private float graduallyIncreasingSpeed;

    public int lastXinput;

    private bool codeFinishedLoading;

    public PlayerMoveState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
                
        xInput = lastXinput;
        player.CheckIfShouldFlip(xInput);

        if (xInput > 0)
        {
            graduallyIncreasingSpeed = player.CurrentVelocity.x;
        }
        else
        {
            graduallyIncreasingSpeed = -player.CurrentVelocity.x;
        }
    }

    public override void Exit()
    {
        base.Exit();
        hasBeenRunnng = false;


        if (xInput > 0)
        {
            graduallyIncreasingSpeed = player.CurrentVelocity.x;
        }
        else
        {
            graduallyIncreasingSpeed = -player.CurrentVelocity.x;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(notSlowWalking && !hasBeenRunnng)
        {
            //----------------------------For Sound-------------------------------------------------
            onRunEnter?.Invoke();
            hasBeenRunnng = true;
        }

        //Flipping player when needed
        player.CheckIfShouldFlip(xInput);

        //move player
        if (slowWalkInput && xInput != 0)
        {
            player.SetVelocityX(playerData.slowWalkVelocity * xInput);
            currentSpeed = playerData.slowWalkVelocity;

            notSlowWalking = false;
            hasBeenRunnng = false;
            graduallyIncreasingSpeed = playerData.slowWalkVelocity;

            lastXinput = xInput;
        }
        else if(xInput != 0)
        {          
            if(player.firstGameStart)
            {
                codeFinishedLoading = true;
            }

            //how long it needs until max speed is reached 
            if (graduallyIncreasingSpeed < (playerData.movementVelocity - 0.3f))
            {
                graduallyIncreasingSpeed = Mathf.Lerp(graduallyIncreasingSpeed, playerData.movementVelocity, playerData.speedUpTime * Time.deltaTime * 100);
                //Debug.Log("I am lerping from " + graduallyIncreasingSpeed + " to the max");
                player.SetVelocityX(graduallyIncreasingSpeed * xInput);
            }
            else if (graduallyIncreasingSpeed >= playerData.movementVelocity - 0.3f)
            {
                player.SetVelocityX(playerData.movementVelocity * xInput);
                //Debug.Log("I am now in the may and wuth that the value is " + graduallyIncreasingSpeed );
                currentSpeed = playerData.movementVelocity;
            }

            notSlowWalking = true;

            lastXinput = xInput;
        }
        
        if (isOnSlope && canWalkOnSlope && !isTouchingWall && !JumpInput) //If on slope
        {
            player.SetVelocity(currentSpeed * slopeNormalPerp.x * -xInput, currentSpeed * slopeNormalPerp.y * -xInput);
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.FromToRotation(-player.transform.up, -currentSlopeHit.normal) * player.transform.rotation, 8 * Time.deltaTime);
        }

        //change state when player stops moving
        if (xInput == 0 && !isExitingState)
        {
            graduallyIncreasingSpeed = Mathf.Lerp(graduallyIncreasingSpeed, 0, playerData.slowDownTime * Time.deltaTime * 100);

            if(lastXinput > 0)
            {
                player.SetVelocityX(graduallyIncreasingSpeed);
            }
            else
            {
                player.SetVelocityX(-graduallyIncreasingSpeed);
            }

            if (graduallyIncreasingSpeed <= 0.5f)
            {
                graduallyIncreasingSpeed = 0;
                stateMachine.ChangeState(player.IdleState);
            }
        }        

        //When running against Moss or Treesap it changes to WallGrabState
        if(isTouchingMoss || isTouchingTreesap)
        {
            stateMachine.ChangeState(player.WallGrabState);
        }

        if (grapplingHookInput && hittingHookLayer && playerData.hookIsActivated) //grappling hook
        {
            player.GrapplingHookState.CheckIfHookLayer(hittingHookLayer);
            stateMachine.ChangeState(player.GrapplingHookState);
        }

        if (!isGrounded && !isTouchingWall)
        {
            stateMachine.ChangeState(player.InAirState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        if(player.firstGameStart)
        {
            player.SetVelocityX(playerData.movementVelocity * xInput);
            graduallyIncreasingSpeed = playerData.movementVelocity;

            if(codeFinishedLoading)
            {
                player.firstGameStart = false;
            }
        }
    }
}
