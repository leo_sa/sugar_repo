using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallGrabState : PlayerTouchingWallState
{
    private Vector2 holdPosition;

    public PlayerWallGrabState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void AnimationFinishTrigger()
    {
        base.AnimationFinishTrigger();
    }

    public override void AnimationTrigger()
    {
        base.AnimationTrigger();
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();

        holdPosition = player.transform.position;

        HoldPosition();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (!isExitingState)
        {          
            if (isTouchingMoss)
            {
                HoldPosition();
                //if (wallCheckHitPoint != null)
                //{
                //    player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
                //}

                if (yInput > 0) //wall climbing by holding w
                {
                    if (wallCheckHitPoint != null)
                    {
                        if (player.FacingDirection > 0)
                        {
                            wallCheckHitPoint += new Vector2(-playerData.distanceToWall, playerData.distanceToWall);
                        }
                        else if (player.FacingDirection < 0)
                        {
                            wallCheckHitPoint += new Vector2(playerData.distanceToWall, playerData.distanceToWall);
                        }
                        player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
                    }
                    stateMachine.ChangeState(player.WallClimbState);
                }
                else if (xInput != player.FacingDirection) //Holds wrong button
                {
                    player.WallHopState.DetermineWallHopDirection(isTouchingMoss);
                    stateMachine.ChangeState(player.WallHopState);
                }
                else if ((yInput < 0 || !grabInput || !grabInput && yInput > 0) && !isGrounded) //Lets go of button or holds s
                {
                    if (wallCheckHitPoint != null)
                    {
                        if (player.FacingDirection > 0)
                        {
                            wallCheckHitPoint += new Vector2(-playerData.distanceToWall, playerData.distanceToWall);
                        }
                        else if (player.FacingDirection < 0)
                        {
                            wallCheckHitPoint += new Vector2(playerData.distanceToWall, playerData.distanceToWall);
                        }
                        player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
                    }

                    stateMachine.ChangeState(player.WallSlideState);
                }
                else if (isGrounded && isTouchingWall) //Touches ground while using grab input, which is the same as move input
                {
                    stateMachine.ChangeState(player.MoveState);
                }
                else if(jumpInput) //wall jump
                {
                    player.WallJumpState.DetermineWallJumpDirection(isTouchingWall);
                    stateMachine.ChangeState(player.WallJumpState);
                }
            }
            else if (isTouchingTreesap)
            {
                HoldPosition();
                //if (wallCheckHitPoint != null)
                //{
                //    player.transform.position = Vector3.Lerp(player.transform.position, wallCheckHitPoint, 0.1f);
                //}

                if (jumpInput)
                {
                    player.WallJumpState.DetermineWallJumpDirection(isTouchingTreesap);
                    stateMachine.ChangeState(player.WallJumpState);
                }
            }
            else if (!isTouchingWall && !isGrounded)
            {
                stateMachine.ChangeState(player.InAirState);
            }
            else if(!isTouchingWall && isGrounded)
            {
                stateMachine.ChangeState(player.IdleState);
            }
        }
    }

    private void HoldPosition()
    {
        player.transform.position = holdPosition;

        player.SetVelocityX(0f);
        player.SetVelocityY(0f);
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
