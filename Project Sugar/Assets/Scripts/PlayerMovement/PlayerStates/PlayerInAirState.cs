using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInAirState : PlayerState
{
    protected bool isGrounded;
    protected bool isTouchingMoss;
    protected bool isTouchingTreesap;
    protected bool inJumpingShroomRadius;
    protected bool shouldDie;
    protected bool isGliding;
    protected bool wasOnJumpingShroomGround;
    protected bool jumpAfterCliffTime;
    protected bool hittingHookLayer;
    protected bool inSlowDownBush;
    protected bool isTouchingWall;
    protected bool glidingInAir;
    protected bool isPressingGlideInput;

    protected bool isJumpingShroomed;

    protected Vector3 playerPosition;

    private bool jumpInput;
    private bool oldIsTouchingWall;
    private bool oldIsTouchingWallBehind;
    private bool grabInput;
    private bool afterWallJumpTime;
    private bool isTouchingWallBehind;
    private bool isTouchingLedge;
    private bool glideInput;
    private bool grapplingHookInput;
    private bool isQuickFalling;

    private Vector2 currentVelocity;

    protected float[] jumpHeigthDistance;
    protected float highestJumpDistance;
    protected float newBounceHeight;
    private float previousJumpingHeigthDistance;
    private bool setFallSpeed;
    protected Vector3 currentPosition;
    protected float currentYPos;
    private bool canQuickFall;
    protected bool addVelocity;
    protected bool reduceVelocity;
    public bool highPt = false;
    protected float floatTimer = 0;
    private bool timerIsRunning;
    private float keepCurrentVelocityX;
    //public Vector3[] shroomPositions;
    //---------------------------------

    private float startAfterWallJumpTime;
    private float internalTimer = 0.5f;
    private float afterGrappleAirTimer;

    private int xInput;
    public int yInput;
    
    public float glideTimer;

    public bool startGlideTimer;
    public bool resetDistance;
    public bool inAirFromGrapplingState;
    
    public PlayerInAirState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();

        oldIsTouchingWall = isTouchingWall;
        oldIsTouchingWallBehind = isTouchingWallBehind;

        isGrounded = player.CheckIfGrounded();
        isTouchingWall = player.CheckIfTouchingWall();
        isTouchingWallBehind = player.CheckIfTouchingWallBehind();
        isTouchingLedge = player.CheckIfTouchingLedge();
        isTouchingMoss = player.CheckIfTouchingMoss();
        isTouchingTreesap = player.CheckIfTouchingTreesap();
        shouldDie = player.CheckIfShouldDie();

        player.targetPosition = player.hookCamera.ScreenToWorldPoint(Input.mousePosition); 
        player.targetPosition.z = player.hookCamera.nearClipPlane;
        hittingHookLayer = player.CheckIfHittingHookLayer();

        //if (isTouchingWall && !isTouchingLedge)
        //{
        //    player.LedgeClimbState.SetDetectedPosition(player.transform.position);
        //}

        if (!afterWallJumpTime && !isTouchingWall && !isTouchingWallBehind && (oldIsTouchingWall || oldIsTouchingWallBehind))
        {
            StartAfterWallJumpTime();
        }
    }

    public override void Enter()
    {
        base.Enter();

        isQuickFalling = false;
    }

    public override void Exit()
    {
        base.Exit();

        previousJumpingHeigthDistance = 0;
        highestJumpDistance = 0;
        afterGrappleAirTimer = 0;

        player.BounceState.newHighestBounce = newBounceHeight;

        canQuickFall = false;
        oldIsTouchingWall = false;
        oldIsTouchingWallBehind = false;
        isTouchingWall = false;
        isTouchingWallBehind = false;
        inAirFromGrapplingState = false;

        highPt = false;
        if (!highPt)
        {
            player.PlayerRigidbody.gravityScale = player.originalGravity;
            floatTimer = 0;
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        CheckAfterCliffTime();
        CheckAfterWallJumpTime();
        inSlowDownBush = player.CheckIfInSlowDownBush();

        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;
        jumpInput = player.InputHandler.JumpInput;
        grabInput = player.InputHandler.GrabInput;
        glideInput = player.InputHandler.GlideInput;
        grapplingHookInput = player.InputHandler.GrapplingHookInput;
        inJumpingShroomRadius = player.CheckIfInJumpingShroomRadius();
        isJumpingShroomed = player.CheckIfTouchingJumpingShroom();
        player.CheckIfShouldRotate(player.transform.rotation, playerData.rotateBackInAirTime);
        playerPosition = player.transform.position;

        if(isJumpingShroomed && !isTouchingWall)
        {
            wasOnJumpingShroomGround = true;
        }
        else if (isTouchingWall || isGrounded && !isJumpingShroomed)
        {
            wasOnJumpingShroomGround = false;
        }

        TimeBeforeGliding();

        
        if (isGrounded && player.CurrentVelocity.y < 0.01f) //land
        {
            startGlideTimer = false;
            glideTimer = 0;
            isGliding = false;
            glidingInAir = false;

            //player.CheckIfShouldFlip(xInput);
            stateMachine.ChangeState(player.LandState);
        }
        else if (isGrounded && xInput != 0) // || isTouchingWall && glideInput-------------------eventuell auf nur Moss und Treesap �ndern
        {
            setFallSpeed = false;
            glideTimer = 0;
            isGliding = false;
            glidingInAir = false;

            player.CheckIfShouldFlip(xInput);
            player.SetVelocityX(playerData.momentumVelocity * xInput);
            player.MoveState.lastXinput = xInput;
            stateMachine.ChangeState(player.MoveState);
        }        
        else if (jumpInput && ((isTouchingMoss || isTouchingTreesap) || isTouchingWallBehind || afterWallJumpTime)) //wall jump
        {
            startGlideTimer = false;
            isGliding = false;

            StopAfterWallJumpTime();
            isTouchingWall = player.CheckIfTouchingWall();
            player.WallJumpState.DetermineWallJumpDirection(isTouchingWall);
            stateMachine.ChangeState(player.WallJumpState);
        }
        else if (jumpInput && player.JumpState.CanJump()) //jumping
        {
            startGlideTimer = true;

            jumpAfterCliffTime = false;
            stateMachine.ChangeState(player.JumpState);
        }
        else if (glideInput && !isGrounded && !isTouchingWall && glideTimer >= playerData.timerToStartGliding || glidingInAir && glideInput) //gliding
        {
            stateMachine.ChangeState(player.GlideState);
        }
        else if ((isTouchingMoss || isTouchingTreesap) && grabInput && xInput == player.FacingDirection) //wallgrab ---- && isTouchingLedge
        {
            startGlideTimer = false;
            isGliding = false;
            glideTimer = 0;

            stateMachine.ChangeState(player.WallGrabState);
        }
        else if (isTouchingTreesap) //treesap wallgrab
        {
            startGlideTimer = false;
            isGliding = false;
            glideTimer = 0;

            stateMachine.ChangeState(player.WallGrabState);
        }
        else if (isTouchingWall && grabInput && xInput == player.FacingDirection && !isTouchingMoss || isTouchingMoss && !grabInput) //wall slide --- isTouchingMoss && player.CurrentVelocity.y <= 0 || 
        {
            startGlideTimer = false;
            isGliding = false;
            glideTimer = 0;

            stateMachine.ChangeState(player.WallSlideState);
        }
        else if (grapplingHookInput && hittingHookLayer && playerData.hookIsActivated) //grapplinghook
        {
            startGlideTimer = false;
            isGliding = false;

            player.GrapplingHookState.CheckIfHookLayer(hittingHookLayer);
            stateMachine.ChangeState(player.GrapplingHookState);
        }
        else if (!isGliding) //falling
        {
            if (inAirFromGrapplingState)
            {
                if (xInput != 0)
                {
                    afterGrappleAirTimer += Time.deltaTime;
                }

                if(afterGrappleAirTimer >= playerData.afterHookTime)
                {
                    inAirFromGrapplingState = false;
                }
            }

            if (xInput != 0 && !inAirFromGrapplingState || !inAirFromGrapplingState)
            {
                player.CheckIfShouldFlip(xInput);
                player.SetVelocityX(playerData.movementVelocity * xInput);
                inAirFromGrapplingState = false;
                afterGrappleAirTimer = 0;
            }

            
            
            player.PlayerAnimator.SetFloat("yVelocity", player.CurrentVelocity.y);
            player.PlayerAnimator.SetFloat("xVelocity", Mathf.Abs(player.CurrentVelocity.x));
        }

        if (shouldDie)
        {
            stateMachine.ChangeState(player.DeathState);
        }
        
        if (inSlowDownBush)
        {
            internalTimer -= Time.deltaTime;

            if (internalTimer >= 0)
            {
                currentVelocity = player.CurrentVelocity;
                player.SetVelocity(currentVelocity.x / playerData.stopStength, currentVelocity.y / playerData.stopStength);
            }
            else
            {
                inSlowDownBush = false;
            }
        }
        else
        {
            internalTimer = 0.5f;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();


        if (player.jumpingShrooms != null)
        {
            if (inJumpingShroomRadius)
            {
                // jumpHeigthDistance = Vector3.Distance(playerPosition, player.shroom.transform.position);

                float minDistance = float.MaxValue;

                for (int i = 0; i < player.jumpingShrooms.Length; i++)
                {
                    Vector3[] shroomPositions = new Vector3[player.jumpingShrooms.Length];
                    float[] jumpHeigthDistance = new float[player.jumpingShrooms.Length];


                    shroomPositions[i] = player.jumpingShrooms[i].transform.position;
                    float thisDistance = Vector3.Distance(playerPosition, shroomPositions[i]);

                    jumpHeigthDistance[i] = thisDistance;

                    if (thisDistance < minDistance)
                    {
                        minDistance = thisDistance;
                    }
                }

                if (previousJumpingHeigthDistance <= highestJumpDistance)
                {
                    previousJumpingHeigthDistance = minDistance;
                }
                else if (previousJumpingHeigthDistance > highestJumpDistance)
                {
                    highestJumpDistance = previousJumpingHeigthDistance;
                    newBounceHeight = highestJumpDistance;
                }

                if (highestJumpDistance >= (playerData.smallestShroomJump - 5f)) //has to be over the min jump to fall quicker
                {
                    canQuickFall = true;
                }
                else
                {
                    canQuickFall = false;
                }
            }
            else if (!inJumpingShroomRadius)
            {
                previousJumpingHeigthDistance = 0;
                highestJumpDistance = 0;
            }

            if (inJumpingShroomRadius && yInput < 0 && canQuickFall) //if pressing down
            {
                player.SetVelocityY(-playerData.quickFallVelocity);
                addVelocity = true;
                reduceVelocity = false;
                isQuickFalling = true;
            }

            if (inJumpingShroomRadius && yInput >= 0 && canQuickFall) //if pressing nothing or up
            {
                if(!isQuickFalling)
                {
                    reduceVelocity = true;
                    addVelocity = false;
                }                
            }

            if (!isTouchingWall && isTouchingLedge && !isGrounded) //touching a ledge  --------------------> WIP WEGEN STUCK AN WAND COLLIDERN
            {
                //stateMachine.ChangeState(player.WallSlideState);
                player.SetVelocityX(0);
                player.SetForce(Vector2.down * 1);
            }

            if (wasOnJumpingShroomGround && !isTouchingWall && (player.CurrentVelocity.y <= 0 || timerIsRunning) && highPt == false && highestJumpDistance >= (playerData.smallestShroomJump - 10f)) //highestJumpDistance > previousJumpingHeigthDistance
            {
                ProcessHighPt();
            }
            else if (highPt == true) // && player.CurrentVelocity.y > 0
            {
                //all of the other shit bacc again
                player.PlayerRigidbody.gravityScale = player.originalGravity;
                floatTimer = 0;
            }
        }
    }

    private void ProcessHighPt()
    {
        //gravity 0;
        float zeroGravity = 0;
        //timer for how long I'll stay in air
        floatTimer += Time.deltaTime;

        //zero y velocity
        if (floatTimer <= playerData.timeInAir)
        {
            player.SetVelocityY(0.5f);
            player.PlayerRigidbody.gravityScale = zeroGravity;
            timerIsRunning = true;
        }
        else
        {
            highPt = true;
            timerIsRunning = false;
        }
    }

    private void CheckAfterCliffTime()
    {
        if (jumpAfterCliffTime && Time.time > startTime + playerData.jumpAfterCliffTime)
        {
            jumpAfterCliffTime = false;
            player.JumpState.CanJump();
            //player.JumpState.DecreaseAmountOfJumpsLeft();
        }
    }

    private void TimeBeforeGliding()
    {
        if (startGlideTimer)
        {
            glideTimer += Time.deltaTime;
        }
        else
        {
            startGlideTimer = false;
        }
    }

    private void CheckAfterWallJumpTime()
    {
        if (afterWallJumpTime = true && Time.time > startAfterWallJumpTime + playerData.jumpAfterCliffTime)
        {
            afterWallJumpTime = false;
        }
    }

    public void StartAfterWallJumpTime()
    {
        afterWallJumpTime = true;
        startAfterWallJumpTime = Time.time;
    }


    public void StopAfterWallJumpTime() => afterWallJumpTime = false;

    public void StartAfterCliffTime() => jumpAfterCliffTime = true;    
}
