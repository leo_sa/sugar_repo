using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallClimbState : PlayerTouchingWallState
{
    public delegate void OnClimbEnter();
    public OnClimbEnter onClimbEnter;
    public delegate void OnClimbStop();
    public OnClimbStop onClimbStop;

    private Vector2 distanceToPoint;

    public PlayerWallClimbState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName) : base(player, stateMachine, playerData, animationBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();

        //----------------------------For Sound-------------------------------------------------
        onClimbEnter?.Invoke();
    }

    public override void Exit()
    {
        base.Exit();

        //----------------------------For Sound-------------------------------------------------
        onClimbStop?.Invoke();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        

        if (!isExitingState)
        {
            if (isTouchingMoss && grabInput) //As long as there is a MossWall
            {
                player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.FromToRotation(player.transform.right, -verticalSlopeHit.normal) * player.transform.rotation, 8 * Time.deltaTime);
            }

            if (yInput != 1) //Player stops moving
            {
                stateMachine.ChangeState(player.WallGrabState);
            }
            else if (yInput != 1 && xInput != player.FacingDirection || xInput != player.FacingDirection) //Player stops moving but holds wrong button or just holds wrong button
            {
                player.WallHopState.DetermineWallHopDirection(isTouchingMoss);
                stateMachine.ChangeState(player.WallHopState);
            }
            else if (yInput < 0 || !grabInput && isTouchingMoss) //Player stops holding button
            {
                stateMachine.ChangeState(player.WallSlideState);                
            }         
            
            //if(!isTouchingMoss) -> Creates a bug, in which you sometimes wall jump instead of upjump & a bug in which you endless jump in roof corners
            //{
            //    stateMachine.ChangeState(player.InAirState);
            //}
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        if(!isExitingState)
        {
            if (isTouchingMoss && grabInput) //As long as there is a MossWall
            {
                player.SetVelocity(player.FacingDirection * playerData.wallClimbVelocity * -slopeNormalPerp.x, playerData.wallClimbVelocity); //  * Time.deltaTime * 100
            }
            
            if (yInput > 0 && jumpInput && isTouchingMoss) //Player jumps while climbing
            {
                player.SetVelocity(player.FacingDirection * playerData.wallClimbJump * -slopeNormalPerp.x, playerData.wallClimbJump);
            }
        }
    }
}
