using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UsePlayerState
{
    IDLE,
    WALK,
    JUMP,
}

public class PlayerState
{
    //classes that inherit from PlayerState can access protected variables
    protected Player player;
    protected PlayerStateMachine stateMachine;
    protected PlayerData playerData;

    protected bool isAnimationFinished;
    protected bool isExitingState;

    protected float startTime;

    private string animationBoolName;

    //constructor
    //Use state hinzuf�gen
    public PlayerState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, string animationBoolName)
    {
        this.player = player;
        this.stateMachine = stateMachine;
        this.playerData = playerData;
        this.animationBoolName = animationBoolName;
    }

    //virtual says that this function can be overwritten by classes that inherit from this class
    //use when entering a state
    public virtual void Enter()
    {
        DoChecks();
        player.PlayerAnimator.SetBool(animationBoolName, true);
        startTime = Time.time;
        //Debug.Log(animationBoolName);
        isAnimationFinished = false;
        isExitingState = false;
    }

    //use when leaving a state
    public virtual void Exit()
    {
        player.PlayerAnimator.SetBool(animationBoolName, false);
        isExitingState = true;
    }

    //gets called every frame
    public virtual void LogicUpdate()
    {

    }

    //gets called every fixed update
    public virtual void PhysicsUpdate()
    {
        DoChecks();
    }

    //looks for walls & grounds
    public virtual void DoChecks()
    {

    }

    public virtual void AnimationTrigger() { }

    public virtual void AnimationFinishTrigger() => isAnimationFinished = true;
}
