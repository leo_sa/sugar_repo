using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    #region State Variables
    //every script that has a refference to player can get the variable & read it, but it can only be set in this script 
    public PlayerStateMachine StateMachine { get; private set; }
    public PlayerIdleState IdleState { get; private set; }
    public PlayerMoveState MoveState { get; private set; }
    public PlayerJumpState JumpState { get; private set; }
    public PlayerInAirState InAirState { get; private set; }
    public PlayerLandState LandState { get; private set; }
    public PlayerWallClimbState WallClimbState { get; private set; }
    public PlayerWallGrabState WallGrabState { get; private set; }
    public PlayerWallSlideState WallSlideState { get; private set; }
    public PlayerWallJumpState WallJumpState { get; private set; }
    public PlayerWallHopState WallHopState { get; private set; }
    public PlayerLedgeClimbState LedgeClimbState { get; private set; }
    public PlayerGlideState GlideState { get; private set; }
    public PlayerGrapplingHookState GrapplingHookState { get; private set; }
    public PlayerDeathState DeathState { get; private set; }
    public PlayerBounceState BounceState { get; private set; }


    [Header("Double-click to find changeable data")]
    [SerializeField]
    private PlayerData playerData;
    public PlayerData PlayerData { get => playerData; set => playerData = value; }
    #endregion

    #region Components
    public PlayerInputHandler InputHandler { get; private set; }
    public Animator PlayerAnimator { get; private set; }    
    public Rigidbody2D PlayerRigidbody { get; private set; }
    public DistanceJoint2D DistanceJoint { get; private set; }
    public PlayerInput Input { get; private set; }
    public GameObject gapplingHookPosition;
    public LineRenderer grapplingHookLine;
    public PhysicsMaterial2D playerPhysicsMaterial;
    public PhysicsMaterial2D noSlidingPhysicsMaterial;
    public Camera hookCamera;
    #endregion

    #region Check Tranforms
    [Header("Checks - don't leave empty")]
    [SerializeField]
    private Transform groundCheck;

    [SerializeField]
    private Transform slopeCheck;

    [SerializeField]
    private Transform wallCheck;

    [SerializeField]
    private Transform ledgeCheck;
       
    [SerializeField]
    private LayerMask mossLayer;

    [SerializeField]
    private LayerMask treesapLayer;

    [SerializeField]
    public LayerMask jumpingMushroomLayer;

    [SerializeField]
    public LayerMask playerLayer;

    [Header("Grappling Hooks need a Ridgidbody 2D of body type static")]
    [SerializeField]
    public LayerMask hookLayer;
    #endregion

    #region tags
    [Header("Tags - don't leave empty")]
    public string deathTag;
    public string slowDownBushTag;
    public string stayInState;
    #endregion

    #region Enable/Disable bools
    [Header("Enable/Disable bools")]
    public bool playerMovementEnabled = true;
    public bool allowStateToChange = true;
    #endregion

    #region other Variables 
    public Vector2 CurrentVelocity { get; private set; }
    public int FacingDirection { get; private set; }
    
    private Quaternion originalRotation;
    [HideInInspector]
    public float originalGravity;

    private Vector2 workspace;
    
    [HideInInspector]
    public Vector3 targetPosition;

    public RaycastHit2D hookHit;
    public RaycastHit2D wallHit;

    [HideInInspector]
    public Collider2D shroomHit;

    [HideInInspector]
    public Vector2 wallCheckHit;

    private Collider2D collisionGroundLayer;
    private RaycastHit2D collisionWallLayer;

    private bool inDeathTrigger;
    private bool inSlowDownBush;

    [HideInInspector]
    public bool firstGameStart;

    public GameObject[] jumpingShrooms;
    #endregion

    #region Unity Callback Functions
    private void Awake()
    {
        StateMachine = new PlayerStateMachine();
        playerData.hookIsActivated = false;

        //References
        ServiceLocator.stateMachine = StateMachine;
        ServiceLocator.player = this;

        IdleState = new PlayerIdleState(this, StateMachine, playerData, "idle");
        MoveState = new PlayerMoveState(this, StateMachine, playerData, "move");
        JumpState = new PlayerJumpState(this, StateMachine, playerData, "inAir");
        InAirState = new PlayerInAirState(this, StateMachine, playerData, "inAir");
        LandState = new PlayerLandState(this, StateMachine, playerData, "land");
        WallClimbState = new PlayerWallClimbState(this, StateMachine, playerData, "wallClimb");
        WallSlideState = new PlayerWallSlideState(this, StateMachine, playerData, "wallSlide");
        WallGrabState = new PlayerWallGrabState(this, StateMachine, playerData, "wallGrab");
        WallJumpState = new PlayerWallJumpState(this, StateMachine, playerData, "inAir");
        WallHopState = new PlayerWallHopState(this, StateMachine, playerData, "inAir");
        LedgeClimbState = new PlayerLedgeClimbState(this, StateMachine, playerData, "ledgeClimbState");
        GlideState = new PlayerGlideState(this, StateMachine, playerData, "inAir");
        GrapplingHookState = new PlayerGrapplingHookState(this, StateMachine, playerData, "inAir");
        DeathState = new PlayerDeathState(this, StateMachine, playerData, "idle");
        BounceState = new PlayerBounceState(this, StateMachine, playerData, "inAir");
    }

    private void Start()
    {
        PlayerAnimator = GetComponent<Animator>();
        InputHandler = GetComponent<PlayerInputHandler>();
        PlayerRigidbody = GetComponent<Rigidbody2D>();
        DistanceJoint = GetComponent<DistanceJoint2D>();
        Input = GetComponent<PlayerInput>();

        firstGameStart = true;
        playerMovementEnabled = true;
        allowStateToChange = true;
        StateMachine.StayInState(allowStateToChange);

        //since we always start facing right (for now)
        FacingDirection = 1;

        //the rotation the player starts with
        originalRotation = transform.rotation;
        originalGravity = PlayerRigidbody.gravityScale;

        StateMachine.Initialize(IdleState);
    }

    private void Update()
    {
        CurrentVelocity = PlayerRigidbody.velocity;
        StateMachine.CurrentState.LogicUpdate();

        if(!playerMovementEnabled)
        {
            Input.actions.Disable();
        }
        else
        {
            Input.actions.Enable();
        }

        jumpingShrooms = GameObject.FindGameObjectsWithTag(playerData.jumpingShroomTag);

        //for (int i = 0; i < jumpingShrooms.Length; i++)
        //{
        //    jumpingShrooms[i] = shroomHit.GetComponent<Transform>().gameObject;
        //    Debug.Log("honestly idk what i am doing");
        //}

        Debug.Log("The current state is " + StateMachine.CurrentState);        
    }

    private void FixedUpdate()
    {
        StateMachine.CurrentState.PhysicsUpdate();

        StateMachine.StayInState(allowStateToChange);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag(deathTag))
        {
            inDeathTrigger = true;
        }

        if (collision.CompareTag(slowDownBushTag))
        {
            inSlowDownBush = true;            
        }

        if (collision.CompareTag(stayInState))
        {
            if (StateMachine.CurrentState == GlideState)
            {
                allowStateToChange = false;
            }
        }
    }    

    private void OnTriggerExit2D(Collider2D collision)
    {
        inDeathTrigger = false;
        inSlowDownBush = false;
    }
    #endregion

    #region Set Functions
    //only use to set velocity of Player gameobject
    public void SetVelocityX(float velocity)
    {
        workspace.Set(velocity, CurrentVelocity.y);
        PlayerRigidbody.velocity = workspace;
        CurrentVelocity = workspace;
    }

    public void SetVelocityY(float velocity)
    {
        workspace.Set(CurrentVelocity.x, velocity);
        PlayerRigidbody.velocity = workspace;
        CurrentVelocity = workspace;            
    }

    public void SetVelocity(float xVelocity, float yVelocity)
    {
        workspace.Set(xVelocity, yVelocity);
        PlayerRigidbody.velocity = workspace;
        CurrentVelocity = workspace;
    }

    public void SetAutomaticVelocityY(Vector2 force)
    {
        PlayerRigidbody.AddForce(force, ForceMode2D.Impulse);
    }

    public void SetJumpVelocity(float velocity, Vector2 angle, int direction)
    {
        angle.Normalize();
        workspace.Set(angle.x * velocity * direction, angle.y * velocity);
        PlayerRigidbody.velocity = workspace;
        CurrentVelocity = workspace;
    }

    public void SetForce(Vector2 force)
    {
        PlayerRigidbody.AddForce(force, ForceMode2D.Impulse);
    }

    public void SetVelocityZero()
    {
        PlayerRigidbody.velocity = Vector2.zero;
        CurrentVelocity = Vector2.zero;
    }

    public void SetPlayerGlideSettings(float mass, float gravity, float drag)
    {
        PlayerRigidbody.mass = mass;
        PlayerRigidbody.gravityScale = gravity;
        PlayerRigidbody.drag = drag;
    }
    #endregion

    #region Check Functions
    public bool CheckIfTouchingTreesap()
    {
        return Physics2D.Raycast(wallCheck.position, Vector2.right * FacingDirection, playerData.wallCheckDistance, playerData.treeSapLayer);
    }

    public bool CheckIfTouchingMoss()
    {
        return Physics2D.Raycast(wallCheck.position, Vector2.right * FacingDirection, playerData.wallCheckDistance, playerData.mossLayer);
    }

    public bool CheckIfTouchingJumpingShroom()
    {
        return Physics2D.OverlapCircle(groundCheck.position, playerData.groundCheckRadius, playerData.jumpingShroomLayer);
    }

    public bool CheckIfInJumpingShroomRadius()
    {
        shroomHit = Physics2D.OverlapCircle(transform.position, playerData.shroomCheckRadius, playerData.jumpingShroomLayer);
        return shroomHit;
    }

    public bool CheckIfGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, playerData.groundCheckRadius, playerData.whatIsGround);
    }

    public bool CheckIfTouchingWall()
    {
        wallHit = Physics2D.Raycast(wallCheck.position, Vector2.right * FacingDirection, playerData.wallCheckDistance, playerData.whatIsGround);
        wallCheckHit = wallHit.point;
        return wallHit;
    }

    public bool CheckIfTouchingLedge()
    {
        //return Physics2D.Raycast(ledgeCheck.position, Vector2.right * FacingDirection, playerData.wallCheckDistance, playerData.whatIsGround);
        return Physics2D.OverlapBox(ledgeCheck.position, playerData.wallStuckCheck, 0, playerData.whatIsGround);
    }

    public bool CheckIfTouchingWallBehind()
    {
        return Physics2D.Raycast(wallCheck.position, Vector2.right * -FacingDirection, playerData.wallCheckDistance, playerData.whatIsGround);
    }

    public bool CheckIfHittingHookLayer()
    {
        Physics2D.queriesHitTriggers = false;
        hookHit = Physics2D.Raycast(transform.position, targetPosition - transform.position, playerData.maxThrowLength, ~playerLayer ); //hookLayer
        Debug.DrawRay(transform.position, targetPosition - transform.position, Color.yellow);

        if (!hookHit)
        {
            return false;
        }

        if (hookHit.collider.gameObject.layer == 9) //layer 9 is grapplingHook Layer
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckIfShouldDie()
    {
        return inDeathTrigger;
    }

    public bool CheckIfInSlowDownBush()
    {
        return inSlowDownBush;
    }

    public Vector2 CheckIfOnSlope()
    {
        Vector2 checkPos = slopeCheck.position;
        return checkPos;
    }

    public Vector2 CheckIfOnCurvedWall()
    {
        Vector2 checkPos = wallCheck.position;
        return checkPos;
    }

    //checking the x input to see when we need to flip the Player gameobject
    public void CheckIfShouldFlip(int xInput)
    {
        if(xInput != 0 && xInput != FacingDirection)
        {
            Flip();
        }
    }

    public void CheckIfShouldRotate(Quaternion currentRotation, float rotationSpeed)
    {
        if (currentRotation != originalRotation)
        {
            if(FacingDirection == 1)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 0), rotationSpeed * Time.deltaTime);
            }
            else if (FacingDirection == -1)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 180f, 0), rotationSpeed * Time.deltaTime);
            }
        }        
    }
    #endregion

    #region Other Functions
    private void AnimationTrigger() => StateMachine.CurrentState.AnimationTrigger();

    private void AnimationFinishTrigger() => StateMachine.CurrentState.AnimationFinishTrigger();
    
    public Vector2 DetemineCornerPosition()
    {
        RaycastHit2D xHit = Physics2D.Raycast(wallCheck.position, Vector2.right * FacingDirection, playerData.wallCheckDistance, playerData.whatIsLedge);
        float xDistance = xHit.distance;
        workspace.Set((xDistance + 0.015f) * FacingDirection, 0f);
        RaycastHit2D yHit = Physics2D.Raycast(ledgeCheck.position + (Vector3)(workspace), Vector2.down, ledgeCheck.position.y - wallCheck.position.y + 0.015f, playerData.whatIsLedge);
        float yDistance = yHit.distance;

        workspace.Set(wallCheck.position.x + (xDistance * FacingDirection), ledgeCheck.position.y - yDistance);
        return workspace;
    }

    //To flip the Player gameobject when moving
    private void Flip()
    {
        FacingDirection *= -1;
        transform.Rotate(0.0f, 180.0f, 0.0f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;

        Gizmos.DrawWireSphere(groundCheck.position, playerData.groundCheckRadius);
        Gizmos.DrawWireSphere(transform.position, playerData.shroomCheckRadius);
        //Gizmos.DrawCube(ledgeCheck.position, playerData.wallStuckCheck);

        Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + playerData.wallCheckDistance, wallCheck.position.y, wallCheck.position.z));
        Gizmos.DrawLine(ledgeCheck.position, new Vector3(ledgeCheck.position.x + playerData.wallCheckDistance, ledgeCheck.position.y, ledgeCheck.position.z));
    }
    #endregion
}
