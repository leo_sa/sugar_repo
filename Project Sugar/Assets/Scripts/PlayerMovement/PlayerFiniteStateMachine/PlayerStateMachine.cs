using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine
{
    //every script that has a refference to playerstatemachine can get the variable & read it, but it can only be set in this script 
    public PlayerState CurrentState { get; private set; }

    private bool allowStateToChange;

    //called to initialize state
    public void Initialize(PlayerState startingState)
    {
        CurrentState = startingState;
        CurrentState.Enter();
    }

    //called when changing states
    public void ChangeState(PlayerState newState)
    {
        if(allowStateToChange)
        {
            CurrentState.Exit();
            CurrentState = newState;
            CurrentState.Enter();
        }        
    }

    public void StayInState(bool playerStaysInCurrentState)
    {
        allowStateToChange = playerStaysInCurrentState;
    }
}
