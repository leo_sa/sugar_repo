using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newPlayerData", menuName = "Data/Player Data/Base Data")]
public class PlayerData : ScriptableObject
{
    [Header("-----Tags-----")]
    [Header(" ")]
    public string mossTag;
    public string treeSapTag;
    public string jumpingShroomTag;
    public LayerMask mossLayer;
    public LayerMask treeSapLayer;
    public LayerMask jumpingShroomLayer;

    [Header("-----Move State-----")]
    [Header(" ")]
    [Range(0.0f, 0.1f)]
    public float speedUpTime;
    [Range(0.0f, 0.1f)]
    public float slowDownTime;
    public float movementVelocity = 20f;
    public float slowWalkVelocity = 10f;

    [Header("-----Walking On Slopes-----")]
    [Header(" ")]
    [Tooltip("When raising the number, make sure the slope won't collide with the Wallcheck")]
    public float maxSlopeAngle;
    public float slopeCheckDistance = 8f;
    public float sideCheckDistance = 3f;

    [Header("-----Climbing On Slopes-----")]
    [Header(" ")]
    [Tooltip("irrelevant for now")]
    public float maxWallSlopeAngle;
    public float wallSlopeCheckDistance = 10f;
    public float distanceToWall = 0.05f;

    [Header("-----Jump State-----")]
    [Header(" ")]
    public float jumpVelocity = 32f;
    public int amountOfJumps = 2;

    [Header("-----Shroom State-----")]
    [Header(" ")]
    public float smallestShroomJump = 23f;
    public float shroomJumpVelocity = 50f;
    public float shroomHeigthAdd = 5f;
    public float shroomHeigthReduce = 5f;
    public float maxBounceHeigth = 80f;
    public float quickFallVelocity = 50f;
    [Range(0.0f, 0.5f)]
    public float timeInAir;

    [Header("-----Glide State-----")]
    [Header(" ")]
    public float timerToStartGliding = 1.5f;
    public float glideTurnGravity = 1f;
    public float glideVelocity = 5f;
    public float glideWeight = 0.5f;
    public float glideTurnDrag = 2f;
    public float momentumVelocity = 2f;
    public float momentumTimer = 5f;

    [Header("-----Wall Jump State-----")]
    [Header(" ")]
    public float wallJumpVelocity = 20f;
    public float wallJumpTime = 0.4f;
    public Vector2 wallJumpAngle = new Vector2(1, 2);

    [Header("-----Jump off wall - in WallGrabState-----")]
    [Header(" ")]
    public float jumpOffWallVelocity = 20f;
    public float jumpOffWallTime = 0.4f;
    public Vector2 jumpOffWallAngle = new Vector2(1, 2);

    [Header("-----Wall Hop State-----")]
    [Header(" ")]
    public float wallHopVelocity = 5f;

    [Header("-----In Air State-----")]
    [Header(" ")]
    public float jumpAfterCliffTime = 0.2f;

    [Header("-----Slow Down Bushes-----")]
    [Header(" ")]
    public float stopStength = 1.3f;

    [Header("-----Wall Slide State-----")]
    [Header(" ")]
    public float wallSlideVelocity = 2f;

    [Header("-----Wall Climb State-----")]
    [Header(" ")]
    public float wallClimbVelocity = 3f;
    public float wallClimbJump = 6f;

    [Header("-----Ledge Climb State-----")]
    [Header(" ")]
    public Vector2 startOffset;
    public Vector2 stopOffset;

    [Header("-----Grappling Hook-----")]
    [Header(" ")]
    [Range(0.0f, 1f)]
    public float afterHookTime;
    public bool hookIsActivated = true;
    public float maxLineLength = 30f;
    public float maxThrowLength = 50f;
    [Tooltip("Player gets pushed to this length automatically to avoid getting stuck")]
    public float minLineLength = 10f;
    public float swingStrength = 15f;
    public float maxSwinging = 70f;
    [Tooltip("Don't put the value under 20 since it will create a bug")]
    public float minSwinging = 20f;
    
    [Header("-----Check Variables-----")]
    [Header(" ")]
    public float groundCheckRadius = 0.3f;
    public float wallCheckDistance = 0.5f;
    public float shroomCheckRadius = 5f;
    public LayerMask whatIsGround;
    public LayerMask whatIsLedge;
    public Vector2 wallStuckCheck;

    [Header("-----Other-----")]
    [Header(" ")]
    public float rotateBackInAirTime = 15f;
    public float rotateBackOnGroundTime = 3f;
}
