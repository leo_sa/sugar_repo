using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator anim;

    private float xInput;
    private float jumpTimer;
    private float turnTimer;

    private bool isFacingRight = true;
    private bool isWalking;
    private bool isGrounded;
    private bool canNormalJump;
    private bool canWallJump;
    private bool isTouchingWall;
    private bool isWallSliding;
    private bool isAttemptingToJump;
    private bool checkJumpMulitplier;
    private bool canMove;
    private bool canFlip;
    private bool isTouchingLedge;
    private bool canClimbLedge = false;
    private bool ledgeDetected;
    private bool isJumpingShroomed;

    private int amountOfJumpsLeft;
    private int facingDirection = 1;

    private Vector2 ledgePosBot;
    private Vector2 ledgePos1;
    private Vector2 ledgePos2;

    [Header("NEEDS TESTING & FEEDBACK")]
    public bool glideByPressingSpace;

    [Header("Jumps")]
    public float jumpForce = 16.0f;
    public int amountOfJumps = 1;
    public float variableJumpHeightMultiplier = 0.5f;

    [Header("Walk Speed")]
    public float movementSpeed = 10.0f;

    [Header("Wall Sliding")]
    public float isWallSlideSpeed;

    [Header("In Air movement")]
    //public float movementForceInAir;
    public float glideSpeed = 1f;
    public float airDragMultiplier = 0.95f;

    [Header("Wall Jumping")]
    //public float wallHopForce;
    public float wallJumpForce;

    [Header("Don't change")]
    public float groundCheckRadius;
    public float wallCheckDistance;
    public float jumpTimerSet = 0.15f;
    public float turnTimerSet = 0.1f;
    public float idleGravity = 4f;

    public float ledgeClimbXOffset1 = 0f;
    public float ledgeClimbYOffset1 = 0f;
    public float ledgeClimbXOffset2 = 0f;
    public float ledgeClimbYOffset2 = 0f;

    //public Vector2 wallHopDirection;
    public Vector2 wallJumpDirection;

    public Transform groundCheck;
    public Transform wallCheck;
    public Transform ledgeCheck;
    public LayerMask whatIsGround;
    public LayerMask jumpingShroomLayer;

    public PhysicsMaterial2D playerPhysicsMaterial;
    
    [Header("Slope")]
    [SerializeField]
    private float slopeCheckDistance;
    [SerializeField]
    private float maxSlopeAngle;
    [SerializeField]
    private PhysicsMaterial2D noFriction;
    [SerializeField]
    private PhysicsMaterial2D fullFriction;

    private float slopeSideAngle;
    private float slopeDownAngle;
    private float lastSlopeAngle;

    private bool isOnSlope;
    //------------------fehlend--------------------

    private bool isJumping;
    private bool canWalkOnSlope;
    private bool canJump;

    private Vector2 newVelocity;
    private Vector2 newForce;

    //--------------------------------------------------
    private Vector2 slopeNormalPerp;

    private Vector2 capsuleColliderSize;
    private CapsuleCollider2D cc;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        amountOfJumpsLeft = amountOfJumps;
        //wallHopDirection.Normalize();
        wallJumpDirection.Normalize();
        
        cc = GetComponent<CapsuleCollider2D>();

        //um den tiefsten Punkt des cc zu finden
        capsuleColliderSize = cc.size;
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        CheckMovementDirection();
        UpdateAnimations();
        CheckIfCanJump();
        CheckIfWallSliding();
        CheckJump();
        CheckIfCanGlide();
        CheckLedgeClimb();
    }

    private void FixedUpdate()
    {
        ApplyMovement();
        CheckSurroundings();
        CheckGround();
        SlopeCheck();
    }

    private void UpdateAnimations()
    {
        //later
    }    

    private void CheckSurroundings()
    {
        //Empty GroundCheck Object in scene
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        isJumpingShroomed = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, jumpingShroomLayer);
        
        isTouchingWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsGround);
    
        isTouchingLedge = Physics2D.Raycast(ledgeCheck.position, transform.right, wallCheckDistance, whatIsGround);

        if(isTouchingWall && !isTouchingLedge && !ledgeDetected)
        {
            ledgeDetected = true;
            ledgePosBot = wallCheck.position;
        }
    }

    private void CheckGround()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        //-----------------fehlend----------------
        if (rb.velocity.y <= 0.0f)
        {
            isJumping = false;
        }

        if (isGrounded && !isJumping && slopeDownAngle <= maxSlopeAngle) //damit man nicht auf Slopes springen kann
        {
            canJump = true;
        }
        //----------------------------------------------

    }

    private void SlopeCheck()
    {
        //um den tiefsten Punkt des cc zu finden
        Vector2 checkPos = transform.position - (Vector3)(new Vector2(0.0f, capsuleColliderSize.y / 2));

        SlopeCheckHorizontal(checkPos);
        SlopeCheckVertical(checkPos);
    }

    private void SlopeCheckHorizontal(Vector2 checkPos)
    {
        if(!isTouchingWall)
        {
            //um zu sehen, um vor oder hinter einen ein Slope ist
            RaycastHit2D slopeHitFront = Physics2D.Raycast(checkPos, transform.right, slopeCheckDistance, whatIsGround);
            RaycastHit2D slopeHitBack = Physics2D.Raycast(checkPos, -transform.right, slopeCheckDistance, whatIsGround);

            if (slopeHitFront) //zeigt in die FaceDirection
            {
                isOnSlope = true;

                slopeSideAngle = Vector2.Angle(slopeHitFront.normal, Vector2.up);
            }
            else if (slopeHitBack)
            {
                isOnSlope = true;

                slopeSideAngle = Vector2.Angle(slopeHitBack.normal, Vector2.up);
            }
            else //wenn kein Slope gefunden wird
            {
                slopeSideAngle = 0.0f;
                isOnSlope = false;
            }
        }       

    }

    private void SlopeCheckVertical(Vector2 checkPos)
    {
        //stores the vertical hit
        RaycastHit2D hit = Physics2D.Raycast(checkPos, Vector2.down, slopeCheckDistance, whatIsGround);

        if (hit) //if hit smth
        {
            //die, die im rechten Winkel zum hit steht aka nach links zeigt
            slopeNormalPerp = Vector2.Perpendicular(hit.normal).normalized;

            //die, die zwischen normal und Y Achse steht oder X Achse und Slope
            slopeDownAngle = Vector2.Angle(hit.normal, Vector2.up);

            //wenn auf slope, wird bool true
            if (slopeDownAngle != lastSlopeAngle)
            {
                isOnSlope = true;
            }

            lastSlopeAngle = slopeDownAngle;

            //Ray Linien einzeichnen
            Debug.DrawRay(hit.point, -slopeNormalPerp, Color.blue);
            Debug.DrawRay(hit.point, hit.normal, Color.green);
        }

        if (slopeDownAngle > maxSlopeAngle || slopeSideAngle > maxSlopeAngle) //damit man nicht vor dem Boden stoppt
        {
            canWalkOnSlope = false;
        }
        else
        {
            canWalkOnSlope = true;
        }

        if (isOnSlope && canWalkOnSlope && xInput == 0.0f) //wenn auf slope
        {
            rb.sharedMaterial = fullFriction;
        }
        else // wenn nicht auf slope
        {
            rb.sharedMaterial = noFriction;
        }
    }

    private void CheckLedgeClimb()
    {
        if(ledgeDetected && !canClimbLedge)
        {
            canClimbLedge = true;

            if(isFacingRight)
            {
                ledgePos1 = new Vector2(Mathf.Floor(ledgePosBot.x + wallCheckDistance) - ledgeClimbXOffset1,Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset1);
                ledgePos1 = new Vector2(Mathf.Floor(ledgePosBot.x + wallCheckDistance) + ledgeClimbXOffset2, Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2);
            }
            else
            {
                ledgePos1 = new Vector2(Mathf.Ceil(ledgePosBot.x - wallCheckDistance) + ledgeClimbXOffset1, Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset1);
                ledgePos1 = new Vector2(Mathf.Ceil(ledgePosBot.x - wallCheckDistance) - ledgeClimbXOffset2, Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2);
            }

            canMove = false;
            canFlip = false;
        }

        if (canClimbLedge)
        {
            transform.position = ledgePos1;
            if (transform.position.y == ledgePos1.y)
            {
                ledgePos2 = transform.position;
                FinishLedgeClimb();
            }
        }        
    }

    public void FinishLedgeClimb()
    {
        canClimbLedge = false;
        transform.position = ledgePos2;
        canMove = true;
        canFlip = true;
        ledgeDetected = false;
    }

    private void CheckIfWallSliding()
    {
        if(isTouchingWall && xInput == facingDirection && rb.velocity.y < 0 && !canClimbLedge)
        {
            isWallSliding = true;
        }
        else
        {
            isWallSliding = false;
        }
    }

    private void CheckMovementDirection()
    {
        if(isFacingRight && xInput < 0)
        {
            Flip();
        }
        else if(!isFacingRight && xInput > 0)
        {
            Flip();
        }

        //later
        if (rb.velocity.x != 0)
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }
    }

    private void CheckInput()
    {
        xInput = Input.GetAxisRaw("Horizontal");

        if(Input.GetKeyDown(KeyCode.W))
        {
            if(isGrounded || (amountOfJumpsLeft > 0 && !isTouchingWall))
            {
                NormalJump();
            }
            else
            {
                jumpTimer = jumpTimerSet;
                isAttemptingToJump = true;
            }
        }

        if(Input.GetButtonDown("Horizontal") && isTouchingWall)
        {
            if(!isGrounded && xInput != facingDirection)
            {
                canMove = false;
                canFlip = false;

                turnTimer = turnTimerSet;
            }
        }

        if(turnTimer >= 0)
        {
            turnTimer -= Time.deltaTime;
            if(turnTimer <= 0)
            {
                canMove = true;
                canFlip = true;
            }
        }

        if (checkJumpMulitplier && !Input.GetKey(KeyCode.W))
        {
            checkJumpMulitplier = false;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * variableJumpHeightMultiplier);
        }
    }

    private void CheckIfCanJump()
    {
        if(isGrounded && rb.velocity.y <= 0.01f)
        {
            amountOfJumpsLeft = amountOfJumps;
        }
        
        if(isTouchingWall)
        {
            canWallJump = true;
        }

        //amount of jumps can be set in inspector
        if(amountOfJumpsLeft <= 0)
        {
            canNormalJump = false;
        }
        else
        {
            canNormalJump = true;
        }        
    }

    private void CheckIfCanGlide()
    {
        //if(canNormalJump)
        //{
            if (glideByPressingSpace)
            {
                if (Input.GetKey(KeyCode.Space))
                {
                    if (amountOfJumpsLeft < amountOfJumps - 1)
                    {
                        rb.gravityScale = glideSpeed;
                    }
                }
                else
                {
                    rb.gravityScale = idleGravity;
                }
            }            
            else
            {
                if (amountOfJumpsLeft < amountOfJumps - 1)
                {
                    rb.gravityScale = glideSpeed;
                }
                else
                {
                    rb.gravityScale = idleGravity;
                }
            }
        //}
    }

    private void CheckJump()
    {
        if(jumpTimer > 0)
        {
            //wallJump
            if(isGrounded && isTouchingWall && xInput !=0 && xInput != facingDirection)
            {
                WallJump();
            }
            else if (isGrounded)
            {
                NormalJump();
            }
        }
        
        if(isAttemptingToJump)
        {
            jumpTimer -= Time.deltaTime;
        }

        //if(canJump && !isWallSliding)
        //{
        //    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        //    amountOfJumpsLeft--;
        //}
        //else if(isWallSliding && movementInputDirection == 0 && canJump)   // Wall hop
        //{
        //    isWallSliding = false;
        //    amountOfJumpsLeft--;
        //    Vector2 forceToAdd = new Vector2(wallHopForce * wallHopDirection.x * -facingDirection, wallHopForce * wallHopDirection.y);
        //    rb.AddForce(forceToAdd, ForceMode2D.Impulse);
        //}
        //else if((isWallSliding || isTouchingWall) && movementInputDirection != 0 && canJump)   //wall jump
        //{
        //    isWallSliding = false;
        //    amountOfJumpsLeft--;
        //    Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * movementInputDirection, wallJumpForce * wallJumpDirection.y);
        //    rb.AddForce(forceToAdd, ForceMode2D.Impulse);
        //}
    }

    private void NormalJump()
    {
        if (canNormalJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            amountOfJumpsLeft--;
            jumpTimer = 0;
            isAttemptingToJump = false;
            checkJumpMulitplier = true;
            isJumping = true;

            if (isJumpingShroomed) //If standing on a jumping shroom
            {
                playerPhysicsMaterial.bounciness = 0.5f;
                Debug.Log("Material to Shroom");
            }
            else //if standing on any other ground
            {
                playerPhysicsMaterial.bounciness = 0f;
                Debug.Log("Material to 0");
            }
        }
    }

    private void WallJump()
    {
        if (canWallJump)   //wall jump
        {
            rb.velocity = new Vector2(rb.velocity.x, 0.0f);
            isWallSliding = false;
            amountOfJumpsLeft = amountOfJumps;
            amountOfJumpsLeft--;
            Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * xInput, wallJumpForce * wallJumpDirection.y);
            rb.AddForce(forceToAdd, ForceMode2D.Impulse);
            jumpTimer = 0;
            isAttemptingToJump = false;
            checkJumpMulitplier = true;
            turnTimer = 0;
            canMove = true;
            canFlip = true;
        }
    }

    private void ApplyMovement()
    {
        if (!isGrounded && !isWallSliding && xInput == 0)
        {
            rb.velocity = new Vector2(rb.velocity.x * airDragMultiplier, rb.velocity.y);
        }
        else if(canMove)
        {
            rb.velocity = new Vector2(movementSpeed * xInput, rb.velocity.y);
        }
        //else if(!isGrounded && !isWallSliding && movementInputDirection != 0)
        //{
        //    Vector2 forceToAdd = new Vector2(movementForceInAir * movementInputDirection, 0);
        //    rb.AddForce(forceToAdd);

        //    if(Mathf.Abs(rb.velocity.x) > movementSpeed)
        //    {
        //        rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
        //    }
        //}

        //-----------------------------------SLOPE------------------------------------------------
        if (isGrounded && !isOnSlope && !isJumping) //if not on slope
        {
            Debug.Log("This one");
            newVelocity.Set(movementSpeed * xInput, 0.0f);
            rb.velocity = newVelocity;
        }
        else if (isGrounded && isOnSlope && canWalkOnSlope && !isJumping && !isTouchingWall) //If on slope
        {
            newVelocity.Set(movementSpeed * slopeNormalPerp.x * -xInput, movementSpeed * slopeNormalPerp.y * -xInput);
            rb.velocity = newVelocity;
        }
        else if (!isGrounded) //If in air -> can be taken out
        {
            newVelocity.Set(movementSpeed * xInput, rb.velocity.y);
            rb.velocity = newVelocity;
        }
        //-----------------------------------SLOPE------------------------------------------------

        if (isWallSliding)
        {
            if(rb.velocity.y < -isWallSlideSpeed)
            {
                rb.velocity = new Vector2(rb.velocity.x, -isWallSlideSpeed);
            }
        }
    }

    private void Flip()
    {
        if(!isWallSliding && canFlip)
        {
            facingDirection *= -1;
            isFacingRight = !isFacingRight;
            transform.Rotate(0.0f, 180.0f, 0.0f);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);

        Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z));

        Gizmos.DrawLine(ledgePos1, ledgePos2);

        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
