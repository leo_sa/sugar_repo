﻿using UnityEngine;
using System.Collections;

public class Checkpoints : MonoBehaviour
{
    public static Checkpoints Instance { get; private set; }

    public Player player;
    public Vector3 position;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            position = collision.transform.position;
            SavingSystem<Vector3>.SaveThisToJson(position, "saveCheckpointPosition");
            Debug.Log("--------------------is saving the players position---------------------------- " + position);
        }
    }
}
