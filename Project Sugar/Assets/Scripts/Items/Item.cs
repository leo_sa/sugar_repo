using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Item : SmoothTriggerStay   //This is the G E N E R I C (not really) parent class which is thought to be inherited by individual instances of Items.
{
    protected Action takeItem;  //Define this as the appopiat method
    private void Update(){
        if(!isTriggered) return;
        if(InputCustom.GetInteractDown()){
            takeItem?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
