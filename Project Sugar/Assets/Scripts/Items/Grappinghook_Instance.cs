using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappinghook_Instance : MonoBehaviour
{
    private static Grappinghook_Instance instance;

    public static Grappinghook_Instance Instance { get => instance; set => instance = value; }
    [SerializeField] private GameObject target;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip clip;
    [SerializeField] private ParticleSystem particleSystem;

    void Start()
    {
        instance = this;
        target.SetActive(false);    //Required for some animation stuff
        Debug.Log(target.activeInHierarchy + "KESO");
    }
    public void ChangeHookStatus(bool activate){
        //StartCoroutine(ServiceLocator.smoothCam.Shake(0.1f, 0.05f)); //!MAGIC NUMBERS
        target.SetActive(true);
        source.PlayOneShot(clip);
        particleSystem.Play();
        //You should also play sound n particles
    }
}
