using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grapplinghook : Item
{
    public delegate void OnHookTake();
    public OnHookTake onHookTake;
    [SerializeField] private bool disableAfterTaking;
    [SerializeField] private GameObject effects;
    void Awake(){
        ServiceLocator.hook = this;
        takeItem = EnableGrapplingHook;
    }
    private void EnableGrapplingHook(){
        //enable grappling hook here
        Grappinghook_Instance.Instance.ChangeHookStatus(true);
        ServiceLocator.player.PlayerData.hookIsActivated = true;
        onHookTake?.Invoke();
        if(disableAfterTaking) gameObject.SetActive(false);
        if(effects != null) Effects();
    }

    private void Effects(){
        effects.SetActive(true);
    }
}
