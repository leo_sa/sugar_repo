using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected GameObject enableTarget;
    private void OnTriggerEnter2D(Collider2D col){
        enableTarget.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D col){
        enableTarget.SetActive(false);
    }
}
