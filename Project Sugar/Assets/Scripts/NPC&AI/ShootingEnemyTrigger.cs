using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemyTrigger : SmoothTriggerStay
{
    [SerializeField] private bool setAnimBoolTrueOrFalse;
    [SerializeField] private Animator animator;
    [SerializeField] private string targetString = "Attack";
    private void Start(){
        onTriggered += SetAnimatorBool;
    }
    private void SetAnimatorBool(){
        animator.SetBool(targetString, setAnimBoolTrueOrFalse);
    }
}
