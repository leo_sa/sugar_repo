using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : Enemy
{
    [SerializeField] private Animator animator;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private AudioSource source;
    [SerializeField] private bool shootOnAwake;
    [SerializeField, Range(0, 1)] private float timeBetweenShots;
    [SerializeField] private Bulletpool pool;
    private float timer;
    private void Start(){
        timer = timeBetweenShots;
        ResetExecution.Instance.onReset += StopShooting; 
    }

    private void Update(){
        if(shootOnAwake) Utility.CountDownAction(ref timer, timeBetweenShots, Shoot);    //every timeBetweenShots a shot will be triggered
    }


    private void Shoot(){
        pool.ShootBullet(pool.transform.right * -1);
        //if(source != null) source.PlayFast(source.clip);
        if(particle != null){
            particle.Clear();
            particle.Play();
        }
    }

    public void ExecuteSingleShoot(){
        Shoot();
    }

    private void StopShooting(){
        animator.SetBool("Attack", false);  //!N Maybe make this somehow midifiable?
    }

    private void SetAnimationTrigger(string target){
        animator.SetTrigger(target);
    }

    private IEnumerator SetAnimationTriggetAfterDelay(string target){
        yield return new WaitForSecondsRealtime(Random.Range(.6f, 2.3f));
        SetAnimationTrigger(target);
    }

}
