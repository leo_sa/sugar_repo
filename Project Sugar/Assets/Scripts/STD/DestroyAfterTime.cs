﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class DestroyAfterTime : MonoBehaviour
    {
        [SerializeField] private bool justSetInactive;
        [SerializeField] private float delay;
        void Start()
        {
            if(!justSetInactive)Destroy(gameObject, delay);
            else Invoke("DisableGO", delay);
        }

        private void DisableGO(){
            gameObject.SetActive(false);
        }

    }


