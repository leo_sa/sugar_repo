using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DialogueTags
{
    public static void UpdateDialogue(string toUpdate, int charIndex, out string tagToAdd, out int newCharIndex, out float timerMul){
        //charindex + 1 because charIndex is <
        bool isCustom = false;
        if(toUpdate[charIndex + 1] == '!') isCustom = true;

        int closureIndex = 0;
        float typeTimerMultiplication = 1;

        for (int i = charIndex + 1; i < toUpdate.Length; i++)
        {
            if(toUpdate[i] == '>'){
                closureIndex = i;
                break;
            }
        }

        string tag = "";
        string rawTag ="";
        for (int i = charIndex; i < closureIndex + 1; i++)  //< and > need to be there
        {
            tag += toUpdate[i];
            if(i != charIndex && i != closureIndex) rawTag += toUpdate[i];
        }

        if(isCustom){
            tagToAdd = GiveUpdatedDialogue(rawTag, out typeTimerMultiplication);
        }
        else{
            tagToAdd = tag;
        }

        
        newCharIndex = closureIndex;
        timerMul = typeTimerMultiplication;
    }
    private static string GiveUpdatedDialogue(string input, out float newTime){
        string originalTag = input;
        float cachedTime = 1;

        
        switch(input[1]){
            case 'T':
            string toParse = "";
            for (int i = 2; i < input.Length; i++)
            {   
                toParse += input[i];
            }
            cachedTime = float.Parse(toParse, System.Globalization.CultureInfo.InvariantCulture);
            break;

            case 't':
            string toParse2 = "";
            for (int i = 2; i < input.Length; i++)
            {   
                toParse2 += input[i];
            }
            cachedTime = float.Parse(toParse2, System.Globalization.CultureInfo.InvariantCulture);
            break;
        }
        newTime = cachedTime;
        return "";
    }
}
