using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//!N Mutliply with deltaTime
public class DialogueManager : MonoBehaviour
{
#region  Variables

    //Management related
    private static DialogueObject[] currentDialogues;
    public static DialogueObject[] CurrentDialogues { get => currentDialogues; set => currentDialogues = value; }
    private static DialogueManager instance;
    private DialogueTrigger currentTrigger;


    //Dialogue Utilities and requirements
    [SerializeField, Range(0.01f, 0.1f), Tooltip("Dialogue speed")] private float timeBetweenChars;
    //[SerializeField, Range(0.5f, 2f)] private float timeBetweenDialogues;
    private static bool displayDialogue;

    //Typing requirements
    private string currentStringToType;
    private bool isFull;
    private int charIndex;
    private int sentenceIndex;      //index for current sentence in dialogue
    private int dialogueIndex;      //Index for current dialogue to display
    private float typeTimer;        //Time between chars typed
    private float typeTimerMultiplication = 1;
    private float dialogueTimer;    //Small offset between changing dialogues/sentences
    private bool firstFrameUpdateFlag;


    //UI
    [SerializeField] private TextMeshProUGUI sentenceText, charNameText;
    [SerializeField] private RectTransform dialogueDisplayCard, offPosition, onPosition;
    [SerializeField] private AudioSource voiceSource;   //Because our game is 2D, one single 2Dimensional Voice source should be enough
    [SerializeField] private AudioClip[] currentClip;  //may be changed to an array later
    //This should be fed-in based on the current NPC-Talking
    private RectTransform targetPlatform;

    [SerializeField] private bool useVoice;
    [SerializeField] private float dialogueDisplayYOffsetCoord = 300;   //The approach of using additional insted of setting-raw was kinda stupid...
    private Vector3 originalDisplayCardPos, targetDisplayPos;

#endregion


#region UnityMethods
    void Awake()
    {
        if(instance == null){
            instance = this;
        }
        else{
            Debug.Log("There already is another instance of this class! It is called:" + instance);
            Debug.Log("This was triggered from: " + gameObject);
            gameObject.SetActive(false);
        }
        originalDisplayCardPos = dialogueDisplayCard.position;
        targetPlatform = offPosition;
        targetDisplayPos = originalDisplayCardPos;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.timeScale == 0) return; //Dont do stuff on pause >:(
        if(displayDialogue){
            CheckForDialogue();
        }

        //Used continious LERP instead of while-coroutine because the latter one is kinda buggy
        if(dialogueDisplayCard.position != targetPlatform.position){
            dialogueDisplayCard.position = Vector3.Lerp(dialogueDisplayCard.position, targetPlatform.position, 0.1f * Time.deltaTime * 50);
        }
    }
#endregion

    public static void TriggerNewDialogue(DialogueObject[] dialogues, DialogueTrigger inputTrigger)
    {
        instance.currentTrigger = inputTrigger;
        currentDialogues = dialogues;
        instance.ResetDialogueTypingVariables();
        instance.NextDialogueOrSentence();  //instance will be this
        displayDialogue = true;
        instance.firstFrameUpdateFlag = true;

        instance.currentStringToType = currentDialogues[0].sentences[0];    //No matter the context or what happenes it should alway be zero. This line is kind of a safe-flag if something runs bad in the method
        instance.charNameText.text = currentDialogues[0].characterName;
        instance.targetDisplayPos = new Vector3(instance.originalDisplayCardPos.x, instance.originalDisplayCardPos.y + instance.dialogueDisplayYOffsetCoord, instance.originalDisplayCardPos.z);    //Basically originalPos but with positive Y offset
        instance.targetPlatform = instance.onPosition;
        //instance.StartCoroutine(instance.ChangeDisplayPos(new Vector3(instance.originalDisplayCardPos.x, instance.originalDisplayCardPos.y + instance.dialogueDisplayYOffsetCoord, instance.originalDisplayCardPos.z)));
    }

    private void ResetDialogueTypingVariables(){
        isFull = false;
        charIndex = 0;
        sentenceIndex = 0;      
        dialogueIndex = 0;      
        typeTimerMultiplication = 1;
    }

    private void CheckForDialogue(){

        if(!isFull) TypeString(currentStringToType);
        else
        {
            if(dialogueTimer < 0)
            {
                if (InputCustom.Control.GamePlay.Interact.triggered || Input.GetKeyDown(KeyCode.Mouse0)) NextDialogueOrSentence();
            }
            else
            {
                dialogueTimer -= Time.deltaTime;
            }
        }
    }

    private void TypeString(string input){

        if(!firstFrameUpdateFlag){
            if (Input.anyKeyDown)
            {
                sentenceText.text = input;
                isFull = true;
                return;
            }
        }
        else{
            firstFrameUpdateFlag = false;
        }


        if(typeTimer < timeBetweenChars)
        {
            typeTimer += Time.deltaTime * typeTimerMultiplication;
        }
        else
        {
            if(charIndex < input.Length)
            {             
                if(input[charIndex] != '<'){
                    sentenceText.text += input[charIndex];
                    if(useVoice) voiceSource.PlayFast(currentClip[Random.Range(0, currentClip.Length)]);
                } 
                else{
                    string cachedString;
                    DialogueTags.UpdateDialogue(input, charIndex, out cachedString, out charIndex, out typeTimerMultiplication);
                    sentenceText.text += cachedString;
                }
                
                //Debug.Log("TXT: " + sentenceText);
                //Debug.Log("STR: " + input);
                charIndex++;
            }
            else
            {
                isFull = true;
            }
            typeTimer = 0;
        }

    }

    //Decides if and what next dialogue/sentence do display
    private void NextDialogueOrSentence()
    {
        if(dialogueIndex < currentDialogues.Length){
            //Debug.Log("Strat C: " + sentenceIndex + "F: " + currentDialogues[dialogueIndex].sentences.Length);
            currentStringToType = currentDialogues[dialogueIndex].sentences[sentenceIndex];
            if (sentenceIndex == currentDialogues[dialogueIndex].sentences.Length - 1)
            {
                sentenceIndex = 0;
                charNameText.text = currentDialogues[dialogueIndex].characterName;
                dialogueIndex++;
                //dialogueTimer = timeBetweenDialogues;   //X sec cooldown  || Currently cut because... why should it be there?:eyes:
            }
            else
            {
                sentenceIndex++;
            }

            charIndex = 0;
            isFull = false;
            //the catch should only happen when the index exceeds the array. Bad way to do this and can be rewritten, but it works for now
            //Update 10.09.21 - Yeah sure it works buddy ._.
        }
        else{
            EndDialogue();
        }

        sentenceText.text = "";
    }

    public static void EndDialogeNow(){
        instance.EndDialogue();
    }
    public void EndDialogue(){
        //StartCoroutine(ChangeDisplayPos(originalDisplayCardPos));
        if(currentTrigger != null) currentTrigger.OnDialogueEnded();
        targetDisplayPos = originalDisplayCardPos;
        instance.targetPlatform = instance.offPosition;
        charNameText.text = "";
        displayDialogue = false;
    }


    //Currently unused!
    private IEnumerator ChangeDisplayPos(Vector3 target){
        while(Vector3.Distance(dialogueDisplayCard.position, target) > 1f){
            dialogueDisplayCard.position = Vector3.Lerp(dialogueDisplayCard.position, target, 0.6f * Time.deltaTime * 50);
            yield return new WaitForSeconds(0.01f);
        }
    }


    //for "no voicing"
    private AudioClip AlphanetClip(AudioClip[] source, char alphabet){
        AudioClip clip = null;
        int i = (int)char.ToUpper(alphabet) - 65;      

        try{
            clip = source[i];
        }
        catch{
                //i is here the index based on the alphabet. If this index exceeds the implemented array an error may occur           
        }
        return clip;
    }
}
