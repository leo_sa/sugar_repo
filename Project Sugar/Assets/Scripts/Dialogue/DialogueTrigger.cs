using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : Dialogue
{
   private bool triggeredDialogue;
   private void Start(){
      onTriggerExit += EndDialogueImmediately;
   }
   void Update(){
      CheckForDialogue();
   }


   private void CheckForDialogue()
   {
       if(isTriggered)
       {
            //if(Input.GetKeyDown(KeyCode.E) && !triggeredDialogue)
            if(InputCustom.Control.GamePlay.Interact.triggered && !triggeredDialogue)
            {
                DialogueManager.TriggerNewDialogue(dialogues, this);
                triggeredDialogue = true;
                Debug.Log("Triggered dialogue from" + gameObject.name + " and " + this.GetInstanceID() + " AAAAND " + triggeredDialogue);
            }
       }
       else{
           if(triggeredDialogue)
           {
               EndDialogueImmediately();
           } 
       }
   }

   public void OnDialogueEnded(){
       Invoke("DisableTrigger", Time.deltaTime*2);  //Just some smol delay
       Debug.Log("Triggered an ending dialogue :33333");
   }

   private void DisableTrigger(){
       triggeredDialogue = false;
   }

   private void EndDialogueImmediately(){
        DialogueManager.EndDialogeNow();
        triggeredDialogue = false; //reset
   }
}
