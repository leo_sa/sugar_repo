using UnityEngine;

[CreateAssetMenu(fileName = "NewDialogue", menuName = "ScriptableObjects/UI/Dialogue", order = 1)]
public class DialogueObject : ScriptableObject
{
    [TextArea(5, 200)]
    public string[] sentences;
    public string characterName;
}
