using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public static class DebugDefinitions
{
    //So....here you may define some nomenclature for your stuff..
    //So what will you need?
    //Spawn stuff: Spawn_ID
    //Execute custom event: Execute_ID
    //Set Values based on ID: SetVal_ID_val
    //Oh and you should note your commands somewhere :D
    //For now "SetTime" is important, do this through SetVal_time_1

    //! Warning first draft, this may change later


    public static void ExecuteCommand(string input){
        Debug.Log(input);
        string baseString = "";
        int discontinuation = 0;
        for (int i = 0; i < input.Length; i++)
        {
            if(input[i] == '_'){
                //current nomenclature uses "_" as discontinuation-source
                discontinuation = i+1;
                break;
            }
            baseString += input[i]; //This will only be reached if input[i] != _
        }

        switch (baseString)
        {
            case "SetVar":
            string id = "";
            string futureValue ="";
            for (int i = discontinuation; i < input.Length; i++)
            {
                if(input[i] == '_'){
                //current nomenclature uses "_" as discontinuation-source
                discontinuation = i+1;
                break;
                }

                id += input[i]; //This will only be reached if input[i] != _
            }

            for (int i = discontinuation; i < input.Length; i++)
            {
                if(input[i] == '_'){
                //current nomenclature uses "_" as discontinuation-source
                discontinuation = i+1;
                break;
                }
                
                futureValue += input[i]; //This will only be reached if input[i] != _
            }
            Debug.Log(futureValue + "w97zt");
            SetValues(id, float.Parse(futureValue, CultureInfo.InvariantCulture));
            break;

            case "TPA":
            string id2 = "";
            for (int i = discontinuation; i < input.Length; i++)
            {
                if(input[i] == '_'){
                //current nomenclature uses "_" as discontinuation-source
                discontinuation = i+1;
                break;
                }

                id2 += input[i]; //This will only be reached if input[i] != _
            }
            ServiceLocator.player.transform.position = GameObject.Find(id2).transform.position;
            break;


            case "SetEv":
            string id3 = "";
            for (int i = discontinuation; i < input.Length; i++)
            {
                if(input[i] == '_'){
                //current nomenclature uses "_" as discontinuation-source
                discontinuation = i+1;
                break;
                }

                id3 += input[i]; //This will only be reached if input[i] != _
            }
            ExecuteEvents(id3);
            break;
        }
    }

    private static void SetValues(string id, float value){
        switch (id)
        {
            case "time":
            //Does this work?
            TimeManager.SetTime(value);
            break;

            case "hook":
            ServiceLocator.player.PlayerData.hookIsActivated = true;
            break;

            case "fpsTar":
            Application.targetFrameRate = (int)value;
            break;
        }
    }

    private static void ExecuteEvents(string id){
        switch (id)
        {
            case "DemoCS":
            ServiceLocator.player.transform.position = GameObject.Find("TPAC").transform.position;
            ServiceLocator.player.PlayerData.hookIsActivated = true;
            break;
        }
    }
}
