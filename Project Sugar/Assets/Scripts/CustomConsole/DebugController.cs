using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DebugController : MonoBehaviour
{
    public void SetConsole(bool status){
        showConsole = status;
    }
    private bool showConsole = false;
    public string commandString = "Write here!";

    private void Update(){
        if(Input.GetKeyDown(KeyCode.F1)){
            showConsole = !showConsole;

            //Uncomment to pause the game while console is active
            if(showConsole){
                TimeManager.PauseGame(true);
            }
            else{
                TimeManager.PauseGame(false);
            }
        }
        if(showConsole){
                if(Input.GetKeyDown(KeyCode.Return)){
                ExecuteCommand(commandString);
                showConsole = false;
                commandString = "";
            }
        }
    }


    private void OnGUI(){
        if(!showConsole) return;

        float y = 0;

        GUI.Box(new Rect(0, y, Screen.width, 30), "");
        GUI.backgroundColor = new Color(0, 0, 0, 0);
        commandString = GUI.TextField(new Rect(10f, y + 5, Screen.width-20f, 20f), commandString);
    }

    private void ExecuteCommand(string str){
        DebugDefinitions.ExecuteCommand(str);
        TimeManager.PauseGame(false);
        showConsole = false;
    }
}
