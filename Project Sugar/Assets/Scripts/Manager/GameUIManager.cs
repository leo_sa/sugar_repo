using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameUIManager : MonoBehaviour
{
    [SerializeField] AudioMixerSnapshot paused, unPaused;
    [SerializeField] private AudioMixer mixer;
    private static GameUIManager instance;
    
    void Start()
    {
        //ensuring only a single instance
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        savedParrlelelStatus = new bool[parralelWindows.Length];
    }

    
    void Update()
    {
        if (InputCustom.Control.UI.ESC.triggered)
        {
            ChangePauseMenuStatus();
        }
    }

    [SerializeField] private GameObject pauseMenu, pauseMenuChild;
    [SerializeField] private GameObject[] additionalWindows, parralelWindows;
    private bool[] savedParrlelelStatus;

    public void ChangePauseMenuStatus()
    {
        //Disabling all additional windows when leaving pauseMenu
        //DISABLE PAUSE
        if (pauseMenu.activeInHierarchy)
        {
            //mixer.FindSnapshot("Default").TransitionTo(1f);
            unPaused.TransitionTo(.1f);
            for (int i = 0; i < additionalWindows.Length; i++)
            {
                additionalWindows[i].SetActive(false);
            }

            for (int o = 0; o < parralelWindows.Length; o++)
            {
                parralelWindows[o].SetActive(savedParrlelelStatus[o]);
            }
        }
        else //ENABLE PAUSE
        {
            //mixer.FindSnapshot("Paused").TransitionTo(1f);
            paused.TransitionTo(.1f);
            // and ensuring childs activation
            pauseMenuChild.SetActive(true);
            for (int o = 0; o < parralelWindows.Length; o++)
            {
                savedParrlelelStatus[o] = parralelWindows[o].activeInHierarchy;
                parralelWindows[o].SetActive(false);
            }
        }
        pauseMenu.SetActive(!pauseMenu.activeInHierarchy);        
        TimeManager.PauseSwitch();//By default the game isnt paused so this works
    }
}
