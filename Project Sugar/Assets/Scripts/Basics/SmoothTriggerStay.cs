using UnityEngine;

//Workaround for input in OnTriggerStay
public class SmoothTriggerStay : MonoBehaviour
{
    protected delegate void OnTriggered();
    protected OnTriggered onTriggered;
    protected delegate void OnTriggerExit();
    protected OnTriggerExit onTriggerExit;

    [SerializeField] private string collisionString = "Player";
    protected bool isTriggered;
    protected GameObject currentCollided;
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.CompareTag(collisionString))
        {
            Debug.Log("Triggered" + this.gameObject + "GOTR: " + collider.gameObject);
            onTriggered?.Invoke();
            currentCollided = collider.gameObject;
            isTriggered = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if(collider.CompareTag(collisionString))
        {
            Debug.Log("De-Triggered" + this.gameObject);
            onTriggerExit?.Invoke();
            currentCollided = null;
            isTriggered = false;
        }
    }
}
