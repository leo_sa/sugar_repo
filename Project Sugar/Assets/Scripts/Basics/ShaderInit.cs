using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShaderInit : MonoBehaviour
{
    [Space, SerializeField, Header("Shader Variables"), Tooltip("Texture to define cel shading samples in grayscale OR red. For more details open this script!:)")] 
   private Texture2D celShadingLUT;
  
   //Lut(Look Up Texture) Details:
   //Either in grayscale or red, the shader will later read the value of the R channel of the texture
   //Resolution: 1x100
   //Filter mode: Point
   //Wrapping mode: Repeat

    void Start()
    {
       InitGlobalTextures();
    }



    [ContextMenu("Re-Initialize global Texures. Use this if something isnt working but should")]
    private void InitGlobalTextures()
    {
     Shader.SetGlobalTexture("_CelLut", celShadingLUT);  //Here we set the scene-specific lut 
    }
}
