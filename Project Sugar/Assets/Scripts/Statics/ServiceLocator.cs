using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ServiceLocator
{
    public static Camera refCam;    //Non rendertexture camera for positional references
    public static PlayerGrapplingHookState grappleState;
    public static SmoothCameraFollow smoothCam;
    public static PlayerStateMachine stateMachine;
    public static Player player;
    public static Grapplinghook hook;
    public static PlayerJumpState jumpState;
    public static FadeOut fadeOut;
}
