using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GenericUtility<T>
{
    //Basically a shortcut to the return stuff here
    public static T RandomFromArray(T[] inputArr){
        return inputArr[Random.Range(0, inputArr.Length)];
    }  

    public static T RandomFromArrayNonRep(T[] inputArr, ref uint last){
        uint now =  (uint)Random.Range(0, inputArr.Length);
        if(now == last){
            now += 1;
            if(now >= inputArr.Length){
                now -= 2;   //2 because one was added before
            }
        }
        last = now;
        return inputArr[now];
    }
}
