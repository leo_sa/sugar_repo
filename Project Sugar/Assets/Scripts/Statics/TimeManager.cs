using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeManager
{
    private static float lastTimescale;
    private static bool isPaused = false;
    public static bool IsPaused { get => isPaused; set => isPaused = value; }

    private static void SetTimeCustom(float newTimeScale){  //Like SetTime but for this script 
        Time.timeScale = newTimeScale;
        Time.fixedDeltaTime = 0.02f * newTimeScale;
    }
    public static void SetTime(float newTimeScale){
        if(!isPaused){
            Time.timeScale = newTimeScale;
            Time.fixedDeltaTime = 0.02f * newTimeScale;
        }
    }

    public static void PauseGame(bool pauseOrContinue){ //This should currently be only triggered through GameUIManager
        if(pauseOrContinue) PauseGame();
        else ResumeGame();
    }

    public static void PauseSwitch(){
        if(isPaused){
            ResumeGame();
        }
        else{
            PauseGame();
        }
    }
    private static void PauseGame(){
        if(!isPaused){
            lastTimescale = Time.timeScale;
            SetTimeCustom(0);
            isPaused = true;
        }
        else{
            Debug.Log("tried to pause game while it was paused O_O");
        }
    }

    private static void ResumeGame(){
        if(isPaused){
            SetTimeCustom(lastTimescale); //Note! This value could better be saved in a variable so if slowmo is active its not "skipped"
            isPaused = false;
        }
        else{
            Debug.Log("tried to resume game while it was not paused");
        }
    }

}
