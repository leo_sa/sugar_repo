using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    //A Method to play an audiosource with more control and reduced repetition
    public static void PlayDetailed(this AudioSource audioSource, AudioClip clip, float originalPitch, float pitchModification, float delay, bool loop, bool timeModification)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;

        if (timeModification)
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification) * Time.timeScale;
        }
        else
        {
            audioSource.pitch = originalPitch + Random.Range(-pitchModification, pitchModification);
        }


        audioSource.PlayDelayed(delay);
    }


    //A Method to play an audiosource easier, when multiple audiosources are available
    public static void PlayFast(this AudioSource audioSource, AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.Play();
    }

        public static void PlayFastUnscaled(this AudioSource audioSource, AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f));
        audioSource.Play();
    }


    // A simple Chance for if statements
    public static bool Chance(int chance)
    {
        int n = Random.Range(0, 100);

        if (n <= chance) return true;
        else return false;
    }


    //Lerp but float-inaccuracy is taken in
    public static Vector3 SmoothLerp(Vector3 from, Vector3 target, float valueBuffer, float speed)
    {

        if (Vector3.Distance(from, target) > valueBuffer)
        {
            from = Vector3.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    //Lerp but float-inaccuracy is taken in
    public static float SmoothLerpValue(float from, float target, float valueBuffer, float speed)
    {

        if (Mathf.Abs(from - target) > valueBuffer)
        {
            from = Mathf.Lerp(from, target, speed * 50 * Time.deltaTime);
        }
        else
        {
            from = target;
        }

        return from;
    }

    //Inaccurate wobbly lerp
    public static float WobbleLerp(float from, float target, float speed)   //This probably does not work properly
    {
        float x = from + Mathf.Lerp(0, (target - from), speed * TimeMultiplication());
        return x;
    }
   
    //returns all children of the object
    public static GameObject[] GetAllChildren(this Transform transform)
    {
        List<GameObject> childList = new List<GameObject>();

        for (int i = 0; i < transform.childCount; i++)
        {
            childList.Add(transform.GetChild(i).gameObject);
        }

        return childList.ToArray();
    }


    //returns a random integer
    public static int RandomNumber(int length)
    {
        int num;
        num = Random.Range(0, length);
        return num;
    }


    //Changes the hue of an color
    public static Color ChangeHue(this Color OriginalColorRGB, float H)
    {
        float localH, localS, localV;
        Color.RGBToHSV(OriginalColorRGB, out localH, out localS, out localV);

        localH = H;

        return Color.HSVToRGB(localH, localS, localV);

    }

    //rotate towards an object
    public static void RotateTowards(this Transform input, Vector3 towards, float offset)
    {
        float rotZ;

        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        input.rotation = Quaternion.Euler(input.eulerAngles.x, input.eulerAngles.y, rotZ + offset);
    }
    public static void RotateTowardsLerp(this Transform input, Vector3 towards, float offset, float speed)
    {
        float rotZ;

        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        Vector3 target = SmoothStepV3(input.eulerAngles, new Vector3(0, 0, rotZ + offset), speed * TimeMultiplication());
        input.rotation = Quaternion.Euler(target);
    }

    public static Quaternion RotateTowards_Return(Transform input, Vector3 towards, float offset){
        float rotZ;

        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        return Quaternion.Euler(input.eulerAngles.x, input.eulerAngles.y, rotZ + offset);
    }

    //set framerate independent
    public static float TimeMultiplication()
    {
        float returnValue = Time.deltaTime * 50;
        return returnValue;
    }

    //change the timescale smoothly
    public static void SetTime(float mutliplicator)
    {
        Time.timeScale = mutliplicator;
        Time.fixedDeltaTime = 0.02f * mutliplicator;
    }


    //Sinus Time
    public static float SinTime(float multiplier, float offset)
    {
        return Mathf.Sin(Time.timeSinceLevelLoad * multiplier + offset);
    }

    public static float PositiveSinTime(float multiplier, float offset){
        return (Mathf.Sin(Time.timeSinceLevelLoad * multiplier + offset) + 1) * 0.5f; 
    }


    //Bool but with custom buffer for floating point inaccuracy
    public static bool EqualsAround(float inValue, float targetValue, float valueBuffer)
    {
        if(Mathf.Abs(inValue - targetValue) < valueBuffer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string RandomFromArray(this string[] input){
        return input[Random.Range(0, input.Length)];
    }


    public static void FlagCondition(bool condition, ref bool flag, System.Action onChangeTrue, System.Action onChangeFalse){
        if(condition){
            if(flag){
                onChangeTrue?.Invoke();
                flag = false;
            }
        }
        else{
            if(!flag){
                onChangeFalse?.Invoke();
                flag = true;
            }
        }
    }

    
    public static void CountDownAction(ref float input, float maxtTime, System.Action toExecute){
        if(input > 0){
            input -= Time.deltaTime;
        }
        else if (input < 0){
            toExecute?.Invoke();
            input = maxtTime;
        }
    }

    public static Vector3 WobbleLerpV3(Vector3 from, Vector3 target, ref Vector3 tracker, float speed, float intensity){
        Vector3 x = Vector3.Lerp(tracker, (target - from), speed * 1.5f);
        tracker = x;
        return from + x * intensity * Time.deltaTime * 60;
    }

    public static Vector3 SmoothStepV3(Vector3 from, Vector3 to, float t){
        float x, y, z;
        x = Mathf.SmoothStep(from.x, to.x, t);
        y = Mathf.SmoothStep(from.y, to.y, t);
        z = Mathf.SmoothStep(from.z, to.z, t);
        return new Vector3(x, y, z);
    }

    
    public static void RotateRowards2DYAxisFlip(this Transform input, Vector3 towards, float offset, bool isFlipped){
        float rotZ;
        int mul = isFlipped? -1 : 1;
        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y * mul, difference.x) * Mathf.Rad2Deg;

        input.rotation = Quaternion.Euler(input.eulerAngles.x, input.eulerAngles.y, rotZ + offset);
    }


}
