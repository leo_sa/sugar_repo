using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTrigger : SmoothTriggerStay
{
    public System.Action onReset;
    [SerializeField] private bool triggerOnce;
    private bool wasTriggered;
    [SerializeField] private string toTrigger = "death";
    protected virtual void Start(){
        onTriggered += Reset;
    }

    private void Reset(){
        if(triggerOnce){
            if(wasTriggered){
                return;
            }   
            else{
                wasTriggered = true;
            }
        }
        StartCoroutine(ServiceLocator.smoothCam.Shake(0.1f, 0.1f));
        if(!ResetExecution.IsResetting){
            RigAnimationController.Instance.Animator.SetTrigger(toTrigger);
            ResetExecution.IsResetting = true;
        }
        onReset?.Invoke();
    }
}
