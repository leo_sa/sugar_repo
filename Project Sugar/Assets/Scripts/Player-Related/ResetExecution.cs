using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class ResetExecution : MonoBehaviour
{
    [SerializeField] private AudioMixerSnapshot dying, defaultMixer;
    private static ResetExecution instance;
    public static ResetExecution Instance { get => instance; private set => instance = value; }

    public delegate void OnReset();
    public OnReset onReset;
    [SerializeField] private AudioSource deathJingleSource;
    [SerializeField] private Animator animator;
    [SerializeField] private Blinker blinker;
    [SerializeField] private float orthographicSizeTarget = 10, speedInSeconds = 1;
    [SerializeField] private float rotationSpeed = 0.0005f;
    private static bool isResetting;
    public static bool IsResetting { get => isResetting; set => isResetting = value; }
    private void Awake(){
        instance = this;
    }
    //For some reason the audio on the rip ball gets cut off ._.
    public void Focus(){
        onReset?.Invoke();
        CustomCameraBrain.Instance.SetSizeTarget(orthographicSizeTarget, speedInSeconds);
        CustomCameraBrain.Instance.SetRot(-30, rotationSpeed);    //!MAGIC NUMBER

        TimeManager.SetTime(0.01f);
        animator.updateMode = AnimatorUpdateMode.UnscaledTime;
        ServiceLocator.player.playerMovementEnabled = false;
        blinker.ForceBlink();
        isResetting = true; //This always plays in the beginning
        dying.TransitionTo(5f);
        //deathJingleSource.Play();
    }
    public void StartDefocus(){
        //CustomCameraBrain.Instance.SetSizeTarget(orthographicSizeTarget, speedInSeconds);
        CustomCameraBrain.Instance.ResetSizeTarget();
        CustomCameraBrain.Instance.SetSizeTargetRaw(10);
        CustomCameraBrain.Instance.SetRotRaw(-30);
        CustomCameraBrain.Instance.SetRot(0, 0.01f);    //!MAGIC NUMBER
    }
    public void ResetPlayer(){
        ServiceLocator.stateMachine.ChangeState(ServiceLocator.player.DeathState);
    }

    public void ExecuteReset(){
        StartCoroutine(ResetPlayerAnimation());
        defaultMixer.TransitionTo(3f);
    }
    private IEnumerator ResetPlayerAnimation(){
        yield return new WaitForSecondsRealtime(.5f);
        ChangePlayerPos(Vector3.zero);
    }

    public void ChangePlayerPos(Vector3 worlPosition){
        ServiceLocator.player.transform.position = worlPosition;
        Vector3 camTarget = new Vector3(worlPosition.x, worlPosition.y, ServiceLocator.smoothCam.gameObject.transform.position.z);  //Because 2D, Z-Position should stay the same
        ServiceLocator.smoothCam.ChangePosition(camTarget);
        CustomCameraBrain.Instance.ResetSizeTarget();
        CustomCameraBrain.Instance.SetRotRaw(0);
    }

    public void ReEnableMovement(){
        //Enable player movement in here
        isResetting = false;    //And this in the end
        ServiceLocator.player.playerMovementEnabled = true;
        animator.updateMode = AnimatorUpdateMode.Normal;
    }

    public void ReOpenEyes(){
        blinker.OpenEyes();
    }
}
