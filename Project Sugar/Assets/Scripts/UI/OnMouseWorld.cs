using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseWorld : MonoBehaviour
{
    protected delegate void OnMouseOverDel();
    protected delegate void OnMouseExitDel();
    protected OnMouseOverDel onMouseOverDel;
    protected OnMouseExitDel onMouseExitDel;
    public void OnMouseOverEv(){
        onMouseOverDel?.Invoke();
        //Debug.Log("enterOnMouseWorld");
    }

    public void OnMouseLeave(){
        onMouseExitDel?.Invoke();
    }
}
