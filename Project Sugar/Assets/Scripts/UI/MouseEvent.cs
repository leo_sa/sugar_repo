using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseEvent : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Camera refCam;
    [SerializeField] private float rayLength = 100;
    private OnMouseWorld currentTarget;
   private void Update(){
       RaycastHit2D hit2D;
       hit2D = Physics2D.Raycast(refCam.ScreenToWorldPoint(Input.mousePosition), Vector3.forward, rayLength, layerMask);  //!N Magic Number
       if(hit2D.collider != null){
           try{
               OnMouseWorld cached = hit2D.collider.gameObject.GetComponent<OnMouseWorld>();
               cached.OnMouseOverEv();
               ChangeCurrentMouse(cached);
               //Debug.Log("Could retrieve OnMouseWorld" + cached.gameObject.name);
           }
           catch{
               //Debug.Log("Could not retrieve OnMouseWorld");
               //Nah
           }
       }
       else{
           ChangeCurrentMouse(null);
       }
   }

   private void OnDrawGizmosSelected(){
       Gizmos.color = Color.red;
       Gizmos.DrawRay(refCam.gameObject.transform.position, Vector3.forward * rayLength);
   }

   private void ChangeCurrentMouse(OnMouseWorld input){
       if(currentTarget != input){
           if(currentTarget != null)currentTarget.OnMouseLeave();
       }
       currentTarget = input;
   }
}
