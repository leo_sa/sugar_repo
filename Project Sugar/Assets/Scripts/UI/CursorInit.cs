using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorInit : MonoBehaviour
{
    [SerializeField] private Texture2D defaultCursor;
    private void Start(){
        CursorController.SetCursorSprite(defaultCursor);
    }
}
