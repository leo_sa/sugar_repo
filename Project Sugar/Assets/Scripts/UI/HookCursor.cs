using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookCursor : SpriteOnCursor
{
    protected override void Start()
    {
        base.Start();
        onEnterCondition = false;
        ServiceLocator.hook.onHookTake += EnableCursor;
    }

    private void EnableCursor(){
        onEnterCondition = true;
    }
}
