using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CursorController
{
    private static Texture2D defaultCursor;
    public static void SetCursorSprite(Texture2D cursorTex){
        if(defaultCursor == null) defaultCursor = cursorTex;
        Cursor.SetCursor(cursorTex, Vector2.zero, CursorMode.Auto);
    }

    public static void ResetCursor(){
        Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
    }
}
