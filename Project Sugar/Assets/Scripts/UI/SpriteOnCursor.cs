using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteOnCursor : OnMouseWorld
{
   [SerializeField] private Texture2D cursorSprite;
   protected bool onEnterCondition = true;
   protected virtual void Start(){
       onMouseOverDel += OnMouseOverSub;
       onMouseExitDel += OnMouseExitSub;
   }
   void OnMouseOverSub(){
       if(onEnterCondition) CursorController.SetCursorSprite(cursorSprite);
       //Debug.Log(onEnterCondition + "enterOnMouseWorld");
   }

   void OnMouseExitSub(){
       CursorController.ResetCursor();
   }
}
