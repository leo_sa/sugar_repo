using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    [SerializeField] private float speed = 1;
    [SerializeField] private RectTransform toLerpTp, target;
    private float distanceThreshhold = 2f;
    private float fastSpeed = 3, actualSpeed = 1;

    private void Start(){
        actualSpeed = speed;
        fastSpeed = speed * fastSpeed;
    }

    private void Update(){
        //I think linear is appropriate in this context as like one of the ONLY times xD
        if(Mathf.Abs(toLerpTp.position.y - target.position.y) > distanceThreshhold){

            if(Input.GetMouseButtonDown(0)){
                actualSpeed = fastSpeed;
            }
            else if (Input.GetMouseButtonUp(0)){
                actualSpeed = speed;
            }

            target.position = new Vector3(target.position.x, target.position.y  + actualSpeed * Time.unscaledDeltaTime, target.position.z);
        }
        else{
            OnCreditsUp();
        }
    }


    private void OnCreditsUp(){
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
