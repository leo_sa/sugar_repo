using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Options/GraphicOptions", order = 2)]
public class GraphicalOptions : ScriptableObject
{
    public Resolution currentResolution;
    public int currentDropDownIndex, vsyncamount, targetFramerate;
    public bool fullScreen = true, vsync = false;
}
