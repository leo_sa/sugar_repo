using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Input", menuName = "Options/InputOptions", order = 2)]
public class InputOptions : ScriptableObject
{
    public string[] ids;
}
