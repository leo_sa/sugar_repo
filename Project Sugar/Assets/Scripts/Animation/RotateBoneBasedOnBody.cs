using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBoneBasedOnBody : MonoBehaviour
{
    [SerializeField] private Transform scaleRef;
    [SerializeField] private Rigidbody2D baseBody;
    [SerializeField] private Transform target;
    [SerializeField] private float degrees = 10;
    private Vector3 originalRotation;
    private void Start(){
        originalRotation = target.localEulerAngles;
        if(scaleRef == null){
            scaleRef = transform.parent;
        }
    }

    private void Update(){
        RotateBone();
    }

    private void RotateBone(){
        float targetRotZ = baseBody.velocity.x * degrees * Mathf.Sign(scaleRef.localRotation.y);
        Vector3 targetRot = new Vector3(0, 0, targetRotZ);
        
        target.localRotation = Quaternion.Lerp(target.localRotation, Quaternion.Euler(originalRotation + targetRot), 0.1f * Time.deltaTime * 100);
    }
}
