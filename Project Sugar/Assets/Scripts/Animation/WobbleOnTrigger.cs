using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleOnTrigger : SmoothTriggerStay
{
    private Vector3 originalScale;
    [SerializeField, Range(0, 2)] private float xMul, yMul, onLeveMulx, onLeaveMuly;    //Bugged :/
    [SerializeField] private float speed;
    private Vector3 velocity = new Vector3(1, 1, 1);
    private bool canSet = true;
    private void Start() {
        originalScale = transform.localScale;
        onTriggered += SetScale;
        onTriggerExit += SetScaleSmall;
    }
    private void Update(){
        if(isTriggered){
            Vector3 target = new Vector3(Utility.WobbleLerp(transform.localScale.x, originalScale.x, speed), Utility.WobbleLerp(transform.localScale.y, originalScale.y, speed), 1);
            transform.localScale = target;
        }
        else{
            transform.localScale = Vector3.SmoothDamp(transform.localScale, originalScale, ref velocity, 0.7f);
        }
    }


    private void SetScale(){
        transform.localScale = new Vector3(originalScale.x * xMul, originalScale.y * yMul, 1);
    }

    private void SetScaleSmall(){
        transform.localScale = new Vector3(originalScale.x * onLeveMulx, originalScale.y * onLeaveMuly, 1);
    }
}
