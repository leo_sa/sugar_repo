using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigAnimationController : MonoBehaviour
{
    public delegate void OnGroundedExit();
    public delegate void OnGroundedEnter();
    public OnGroundedEnter onGroundedEnter;
    public OnGroundedExit onGroundedExit;

    public delegate void OnFlyEnterDel();
    public delegate void OnFlyExitDel();
    public OnFlyEnterDel onFlyEnter;
    public OnFlyExitDel onFlyExit;

    [SerializeField] private Player controller;
    [SerializeField] private Rigidbody2D body;  //Currently setting this to avoid modifying player script for now.
   [SerializeField] private Animator animator;
    public Animator Animator { get => animator; set => animator = value; }
   private Vector2 inputVelocity;
    public Vector2 InputVelocity { get => inputVelocity; set => inputVelocity = value; }    //Maybe setting this to raw-player input at some point (?) lets see!
    public Rigidbody2D Body { get => body;}

    private float stopMovingOffset = 0.2f, stopMovingTimer;
    private bool lastGrounded;
    private static RigAnimationController instance;    //pseudo singleton here
    public static RigAnimationController Instance { get => instance;}
    private bool resetFlag;

    void Awake(){
        if(controller == null) controller = FindObjectOfType<Player>();   //There should be always only one instance, so having this "safe thingy" may be a good Idea.
        instance = this;    //It may occur that there is a player-instance per scene so...yeah :D
    }

    private void Update(){
        //Debug.Log(AudioListener.volume + " vol");
        if(resetFlag){
            if(controller.CheckIfGrounded()){
                animator.SetTrigger(deathTrigger);
                resetFlag = false;
            }
            else{
                return;
            }
        }
        SetContiniousValues();
        DecideGrounded();
    }
    private bool IsMoving(float maxOffsetTime){
        bool mainCondition = Mathf.Abs(inputVelocity.x) > 1f && Mathf.Abs(Input.GetAxisRaw("Horizontal")) >= 1;
        return mainCondition;
    }
    private void SetContiniousValues(){
        bool isWalking = Input.GetKey(KeyCode.LeftShift);
        float wallInput = Input.GetAxisRaw("Vertical");
        inputVelocity = body.velocity;
        Debug.Log(InputVelocity.y + "GIZMO");


        PlayerGlideState currentInAirState = null;
        bool isGliding = false;
        try{
            currentInAirState = controller.StateMachine.CurrentState as PlayerGlideState;
            isGliding = currentInAirState.NewGlideInput;
        }
        catch{
            isGliding = false;
        }
        animator.SetBool("isWalledRaw", controller.CheckIfTouchingWall());
        animator.SetBool("isSapped", controller.CheckIfTouchingTreesap());
        animator.SetFloat("speedX", inputVelocity.x);   //Kinda misleading, velocityX and speedX should be switched. Mental to-do list ahoy!
        animator.SetFloat("velocityX", Mathf.Abs(inputVelocity.x));
        animator.SetFloat("speedY", Mathf.Sign(inputVelocity.y));
        animator.SetBool("isMoving", IsMoving(stopMovingOffset));  //this is not good because the player may transition in the middle of it
        animator.SetBool("isFalling", Mathf.Abs(inputVelocity.y) > 2f);
        animator.SetBool("isGrounded", controller.CheckIfGrounded());
        animator.SetBool("isWalled", controller.StateMachine.CurrentState is PlayerWallGrabState || controller.StateMachine.CurrentState is PlayerWallSlideState || controller.StateMachine.CurrentState is PlayerWallClimbState);    //The question here arises is, is this state also the same when moving?:O
        animator.SetBool("isGrappled", controller.StateMachine.CurrentState is PlayerGrapplingHookState);    //The question here arises is, is this state also the same when moving?:O
        animator.SetBool("isGliding", isGliding);
        animator.SetBool("isWalking", isWalking);
        animator.SetFloat("walledVel", wallInput);
        animator.SetInteger("walledDirRaw", (int)Mathf.Abs(wallInput));   //misleading name again :|
        animator.SetInteger("speedXAbs", (int)Mathf.Abs(inputVelocity.x));
        //Debug.Log(controller.StateMachine.CurrentState + " KEY INVL");
        //This aintnt working. Why :/
        //Debug.Log(wallInput + "KIRO");
        //Debug.Log(controller.StateMachine.CurrentState + "ddawddd");
        //animator.SetBool("landing", controller.StateMachine.CurrentState is PlayerLandState);
        //Debug.Log(IsMoving(stopMovingOffset));s
        //Here a grounded_bool is required to set the walking animation properly.
        //Allright what to do now: Look at further transitions and pseudo Planar-ref shader!:D

        //Sooo... the player will also have animation states when walking up n' down in the wallclimb.
        //If wallclimbing should be readable by isWalled, rb.vel.y && isMovingVertically(or something)
        //Depending on how fast this transitions either:
        //Set it all as a Blend-State if transition is very short
        //Set Walk as a blend-state/or seperate and have them switch to iswalled based on condition :)
    }


    private bool groundedFlag;
    private bool flyingFlag;
    private void DecideGrounded(){
        if(controller.CheckIfGrounded()){
            if(groundedFlag){
                OnGroundedEnterVoid();
                groundedFlag = false;
            }
        }
        else{
            if(!groundedFlag){
                OnGroundedExitVoid();
                groundedFlag = true;
            }
        }
        Utility.FlagCondition(controller.StateMachine.CurrentState is PlayerGlideState, ref flyingFlag, OnFlyEnter, OnFlyExit); //Like the code above but shortetend into 'genericeness'
    }

    private void OnGroundedExitVoid(){
        onGroundedExit?.Invoke();
    }

    private void OnGroundedEnterVoid(){
        onGroundedEnter?.Invoke();
        animator.SetTrigger("landTrigger");
    }


    private void OnFlyEnter(){
        onFlyEnter?.Invoke();
    }

    private void OnFlyExit(){
        onFlyExit?.Invoke();
    }


    private string deathTrigger;
    public void TriggerReset(string deathTrigger){  //This is pretty lazy ngl. If the time arises, you may change this :D
        //Disable player movement here
        resetFlag = true;
    }

}
