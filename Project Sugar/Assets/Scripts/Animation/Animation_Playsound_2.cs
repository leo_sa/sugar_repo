using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_Playsound_2 : MonoBehaviour
{
   [SerializeField] private AudioSource[] sources;
   [SerializeField] private AudioClip[] clips;

    //Thats horrendously complicated for such a simple task :I
   public void Play(string str){
       string firstNumber ="";
       string secondNumber ="";
       int split = 0;
       for (int i = 0; i < str.Length; i++)
       {
           if(str[i]=='_'){
               split = i;
               break;
           }
           firstNumber += str[i];
       }

       for (int i = split + 1; i < str.Length; i++)
       {
           secondNumber += str[i];
       }

       int finalA = int.Parse(firstNumber, System.Globalization.CultureInfo.InvariantCulture);
       int finalB = int.Parse(secondNumber, System.Globalization.CultureInfo.InvariantCulture);
       //Debug.Log("KEY225" + "  FN: " + firstNumber + "")
       sources[finalA].PlayFast(clips[finalB]);
   }
}
