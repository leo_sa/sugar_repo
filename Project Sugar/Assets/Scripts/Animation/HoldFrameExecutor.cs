using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldFrameExecutor : MonoBehaviour
{
    [SerializeField] private float holdLength = 0.2f;
    [SerializeField, Range(0, 1)] private float timeModification = 0;
    [SerializeField, Range(0, 1)] private float lerpSpeedA = 0.2f, lerpSpeedB = 0.2f;
    [SerializeField, Range(0, 0.1f)] private float timeModFrequency = 0.01f;

    public void ExecuteHoldframe(){
        StartCoroutine(HoldFrame());
    }

    private IEnumerator HoldFrame(){
        while(Time.timeScale - timeModification > 0.05f){
            TimeManager.SetTime(Mathf.Lerp(Time.timeScale, timeModification, lerpSpeedA));
            yield return new WaitForSecondsRealtime(timeModFrequency);
        }
        TimeManager.SetTime(timeModification);
        yield return new WaitForSecondsRealtime(holdLength);

        while(1 - Time.timeScale > 0.05f){
            TimeManager.SetTime(Mathf.Lerp(Time.timeScale, 1, lerpSpeedB));
            yield return new WaitForSecondsRealtime(timeModFrequency);
        }
        TimeManager.SetTime(1); //Reset would prolly be more appropriate or something :I
    }
}
