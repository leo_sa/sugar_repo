using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectSinAnim : MonoBehaviour
{
    [SerializeField] private RectTransform thisTransform, refUpper, refLower;
    [SerializeField] private float intensity, speed = 1;
    [SerializeField] private bool alsoLerpRotation;
    [SerializeField] private float rotationA, rotationB, rotationOffset, rotationSpeed;
    Vector3 ogPos, ogogPos;
    private float ogRotZ;
    void Start()
    {
        ogPos = thisTransform.position;
        ogogPos = thisTransform.position;
        ogRotZ = thisTransform.localEulerAngles.z;
    }

   
    void Update()
    {
        //ogPos.y = ogogPos.y + Mathf.Sin(Time.time * speed) * intensity;
        Vector3 newPosition = Vector3.Lerp(refLower.position, refUpper.position, Utility.PositiveSinTime(speed, 0));
        thisTransform.position = newPosition;

        if(alsoLerpRotation){
            Vector3 tA = new Vector3(0, 0, ogRotZ - rotationA);
            Vector3 tB = new Vector3(0, 0, ogRotZ + rotationB);
            transform.localRotation = Quaternion.Lerp(Quaternion.Euler(tA), Quaternion.Euler(tB), Utility.PositiveSinTime(rotationSpeed, 1 + rotationOffset));
        }
    }
}