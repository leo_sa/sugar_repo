using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeExecutor : MonoBehaviour
{
    [SerializeField] private float intensity = 0.1f, length = 0.1f;

    public void ExecuteScreenShake(){
        StartCoroutine(ServiceLocator.smoothCam.Shake(length, intensity));
    }
}
