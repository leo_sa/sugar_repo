using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Verlet2D
{
//https://www.youtube.com/watch?v=FcnvwtyxLds&t=404s
public struct SingleSegment{
    public Vector2 currentPos, oldPos;
    public SingleSegment(Vector2 position){
        this.currentPos = position;
        oldPos = position;
    }
}

//ToDo: Rewrite as ComputeShader with "toggable" 2D/3D
//!N So, while this algorythm is really neat ngl, its pretty bad having this run on the CPU, especially if there are multiple long chains.
//So yeah, Shader-Time
public class Verlet : MonoBehaviour
{
    [SerializeField] private ComputeShader computeShaderExecution;
    [SerializeField, Header("Verlet Variables")] private Vector2 gravityForces = new Vector2(0, -1);
    [SerializeField] protected uint iterations = 50;
    [SerializeField, Range(0.1f, 0.5f)] private float stiffness = 0.5f;
    [SerializeField, Range(0, 1)] private float speed = 1;
    private Transform pivot;
    protected List <SingleSegment> segments = new List<SingleSegment>();
    protected float ropeSegmentLength = 1;
    protected bool useLengthArray = false;  //For spring bones, original lengths have to be kept
    protected float[] lengthArray;
    protected bool applyFirstPivotPosition = true;
    //____Shader Related Variables______
    private ComputeBuffer segmentBuffer;

    protected void InitSegments(uint length, Vector3 startPointInput, Transform pivotInput){
        pivot = pivotInput;
        for (int i = 0; i < length; i++)
        {
            
            segments.Add(new SingleSegment(startPointInput));
        }
    }
    protected void InitSegmentsRaw(uint length, Vector3[] startPointInput, Transform pivotInput){
        pivot = pivotInput;
        for (int i = 0; i < length; i++)
        {
            segments.Add(new SingleSegment(startPointInput[i]));
        }
    }
    private float RopeLength(int i){
        float cachedLength = 0;
            if(useLengthArray){
                if(i >= lengthArray.Length){
                    cachedLength = ropeSegmentLength;
                }
                else{
                    cachedLength = lengthArray[i];
                }
            }
            else{
                cachedLength = ropeSegmentLength;
            }
        return cachedLength;
    }
    protected virtual void FixedUpdate(){
        if(computeShaderExecution != null){

        }
        else{
            Simulate();
        }
    }

    private void Simulate(){
        //SIM
        Vector2 gravity = gravityForces;
        for (int i = 0; i < this.segments.Count; i++)
        {
            SingleSegment startSegment = segments[i];
            Vector2 velocity = startSegment.currentPos - startSegment.oldPos;
            startSegment.oldPos = startSegment.currentPos;
            startSegment.currentPos += velocity * speed;
            startSegment.currentPos += gravity * Time.fixedDeltaTime * 100;
            segments[i] = startSegment;

        }
        //CONSTRAINTS
        for (int i = 0; i < iterations; i++)    //!N Iterations
        {
            Constrain();
        }
    }

    private void Constrain(){
        if(applyFirstPivotPosition){
            SingleSegment firstSegment = segments[0];
            firstSegment.currentPos = pivot.position;
            segments[0] = firstSegment;
        }

        for (int i = 0; i < segments.Count; i++)
        {
            if(i >= segments.Count - 1) break;
            float length = RopeLength(i);

            SingleSegment startSegment = segments[i];
            SingleSegment secondSegment = segments[i + 1];
            float distance = (startSegment.currentPos - secondSegment.currentPos).magnitude;

            float newDist = Mathf.Abs(distance - length);    //!N This should be based on original-Length

            Vector2 changeDirection = Vector2.zero;

            if(distance > length){
                changeDirection = (startSegment.currentPos - secondSegment.currentPos).normalized;
            } else if (distance < length){
                changeDirection = (secondSegment.currentPos - startSegment.currentPos).normalized;
            }

            Vector2 changeAmount = changeDirection * newDist;
            if(i != 0){
                startSegment.currentPos -= changeAmount * stiffness;
                secondSegment.currentPos += changeAmount * stiffness;
                segments[i] = startSegment;
                segments[i + 1] = secondSegment;
            }
            else{
                if(applyFirstPivotPosition){
                    secondSegment.currentPos += changeAmount;
                    segments[i + 1] = secondSegment;
                }
                else{
                    startSegment.currentPos -= changeAmount * stiffness;
                    secondSegment.currentPos += changeAmount * stiffness;
                    segments[i] = startSegment;
                    segments[i + 1] = secondSegment;
                }
            }
        }

    }
    //Computebuffer execution
    private void InitializeBuffer(int kernel){
        segmentBuffer = SegmentBuffer(segments.Count);
        segmentBuffer.SetData(segments.ToArray());
        computeShaderExecution.SetBuffer(kernel, Shader.PropertyToID("_segmentBuffer"), segmentBuffer);
    }

    private ComputeBuffer SegmentBuffer(int length){
        int Vector2Size = sizeof(float) * 2;   //vector2 has 2 floats
        return new ComputeBuffer(length, Vector2Size);
    }
}

}
