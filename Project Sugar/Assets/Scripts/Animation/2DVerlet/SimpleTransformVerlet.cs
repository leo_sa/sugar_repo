using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Verlet2D
{
    public class SimpleTransformVerlet : Verlet
    {
        [SerializeField] private Transform[] transformSegments;
        private void Start(){
            InitSegments((uint)transformSegments.Length, transform.position, transform);
        }

        protected override void FixedUpdate(){
            base.FixedUpdate();
            Apply();
        }
        
        private void Apply(){
            for (int i = 0; i < transformSegments.Length; i++)
            {
                transformSegments[i].position = segments[i].currentPos;
            }
        }

    } 
}

