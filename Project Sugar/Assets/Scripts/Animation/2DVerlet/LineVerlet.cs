using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Verlet2D{

public class LineVerlet : Verlet
{
    [SerializeField] private uint linePointAmount;
    [SerializeField] private LineRenderer renderer;
    [SerializeField] private float width;

    private void Start(){
        InitSegments(linePointAmount, transform.position, transform);
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        ApplyToLine();
    }

    private void ApplyToLine(){
        float widthCached = width;
        renderer.startWidth = widthCached;
        renderer.endWidth = widthCached;

        Vector3[] ropePositions = new Vector3[segments.Count];
        for (int i = 0; i < ropePositions.Length; i++)
        {
            ropePositions[i] = segments[i].currentPos;
        }

        renderer.positionCount = ropePositions.Length;
        renderer.SetPositions(ropePositions);
    }
}

}
