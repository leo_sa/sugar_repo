using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Verlet2D{
    public class SpringBone2D : Verlet
    {
        [SerializeField, Range(0.001f, 1f), Header("Spring Bone Variables")] private float rotationSpeed;
        [SerializeField, Range(0.1f, 1)] private float positionalLerpSpeed = 1;
        [SerializeField, Range(0, 360)] private float boneOffsetRotation;
        [SerializeField, Header("Spring bone Sin Variables")] private bool useSinOffsetRotation;
        [SerializeField, Range(0, 360)] private float sinRotationOffset;
        [SerializeField] private float boneSinOffset, sinSpeed;
        [SerializeField, Tooltip("Directly sets rotation instead of interpolating. May remove some artifacts")] private bool useRawRotation = true;
        [SerializeField] private Transform[] bones;
        [SerializeField, Tooltip("If in a 2D-Game with -flipping sprites- and this is done via -180Y rotation but a reference in here to compensate for this")] private Transform _YAxisReference;
        [SerializeField, Range(-360, 360)] private float clampFirstRotationLower = -360, clampFirstRotationUpper = 360;
        private Vector3[] originalEulerAnges;
        private float[] originaloffsets, originalZPositions;
        private void OnEnable(){
            for (int i = 0; i < segments.Count; i++)
            {
                SingleSegment cached = segments[i];
                cached.currentPos = bones[i].position;
                segments[i] = cached;
            }
        }
        private void Awake(){
            float[] cachedDistances = new float[bones.Length];
            originaloffsets = new float[bones.Length];
            originalZPositions = new float[bones.Length];

            Vector3[] positions = new Vector3[bones.Length];
            originalEulerAnges = new Vector3[bones.Length];

            for (int i = 0; i < bones.Length; i++)
            {
                positions[i] = bones[i].position;
                originaloffsets[i] = bones[i].localEulerAngles.z;
                originalZPositions[i] = bones[i].position.z;
                originalEulerAnges[i] = bones[i].localEulerAngles;
                try{
                    float cachedDistance = Vector3.Distance(bones[i].position, bones[i + 1].position);
                    cachedDistances[i] = cachedDistance;
                }
                catch{
                    //bad way to do but yeah works
                }

            }
            useLengthArray = true;
            lengthArray = cachedDistances;
            
            InitSegmentsRaw((uint)bones.Length, positions, transform);
        }

        protected override void FixedUpdate(){
            base.FixedUpdate();
            Apply();
        }
            
        private void Apply(){
            for (int i = 0; i < bones.Length; i++)
            {

                //bones[i].position = new Vector3(segments[i].currentPos.x, segments[i].currentPos.y, originalZPositions[i]);
                Vector3 targetPosition = new Vector3(segments[i].currentPos.x, segments[i].currentPos.y, originalZPositions[i]);
                bones[i].position = Utility.SmoothStepV3(bones[i].position, targetPosition, positionalLerpSpeed * Time.fixedDeltaTime * 50);

                if(i < bones.Length - 1){
                    if(!useRawRotation){
                        //!N Y-Axis compensation not supported yet
                        Quaternion cachedTarget = Utility.RotateTowards_Return(bones[i], new Vector3(segments[i + 1].currentPos.x, segments[i + 1].currentPos.y, originalZPositions[i + 1]), 0);
                        Vector3 target = Vector3.Lerp(new Vector3(originalEulerAnges[i].x, originalEulerAnges[i].y, bones[i].eulerAngles.z), cachedTarget.eulerAngles, rotationSpeed);
                        bones[i].rotation = Quaternion.Euler(target);
                    }
                    else{
                        bool condition = Mathf.Abs(_YAxisReference.eulerAngles.y) >= 150;
                        float offset = useSinOffsetRotation? Utility.SinTime(sinSpeed, i * boneSinOffset) * sinRotationOffset : boneOffsetRotation;
                        bones[i].RotateRowards2DYAxisFlip(new Vector3(segments[i + 1].currentPos.x, segments[i + 1].currentPos.y, originalZPositions[i]), offset, condition);
                        if(i == 0){
                            float clampedAngle = Mathf.Clamp(bones[i].eulerAngles.z, clampFirstRotationLower, clampFirstRotationUpper);
                            bones[i].eulerAngles = new Vector3(bones[i].eulerAngles.x, bones[i].eulerAngles.y, clampedAngle);
                        }
                        //Debug.Log(condition + " condition" + _YAxisReference.eulerAngles.y);
                    }
                }
                else{
                    bones[i].rotation = bones[i -1].rotation;
                    bones[i].eulerAngles = new Vector3(bones[i].eulerAngles.x, bones[i].eulerAngles.y, bones[i].eulerAngles.z * Mathf.Sign((_YAxisReference.eulerAngles.y * -1) + 1));
                } 

            }
        }

    }
}

