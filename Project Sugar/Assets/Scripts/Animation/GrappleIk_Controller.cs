using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleIk_Controller : MonoBehaviour
{
    [SerializeField, Range(0,360)] private float hookOffset, bodOffset;
    [SerializeField] private Transform spriteTarget, rotationReference, bodTarget;
    [SerializeField] private LineRenderer target;
    [SerializeField] private Transform[] iks;
    private Vector3 currentPosition, originalSpriteRotation, originalBodRotation;
    private bool updatePosition;
    [SerializeField] private ParticleSystem particleSystem;
    private Player player;
    void Start(){
        ServiceLocator.player.GrapplingHookState.onHookHit += SetCurrentPosition;
        ServiceLocator.player.GrapplingHookState.onHookLeave += SetCurrentPositionInactive;
        originalSpriteRotation = spriteTarget.localEulerAngles;
        originalBodRotation = bodTarget.localEulerAngles;
        player = ServiceLocator.player;
    }
    private void SetCurrentPosition(){
        //transform.rotation = rotationReference.rotation;
        //Make this rotate properly qwq
        SetCurrentPositionIV();
        Invoke("SetCurrentPositionIV", Time.deltaTime * 2); //Literally just needs to trigger next frame
        StartCoroutine(PlayParticle());
    }

    private void SetCurrentPositionInactive(){
        updatePosition = false;      
    }

    private void SetCurrentPositionIV(){
        updatePosition = true;
        currentPosition = target.GetPosition(1);
        //RotateTowards(particleSystem.transform, currentPosition, hookOffset);
        //Debug.Log(currentPosition + "PAWJFOUAWH");
    }


    private void Update(){
        if(updatePosition){
            foreach(Transform IK in iks){
                IK.transform.position = currentPosition;    
            }
            RotateTowards(spriteTarget, currentPosition, hookOffset);
            if(!player.CheckIfGrounded())RotateTowards(bodTarget, currentPosition, bodOffset);
        }
        else{
            spriteTarget.localRotation = Quaternion.Lerp(spriteTarget.transform.localRotation, Quaternion.Euler(originalSpriteRotation), 0.2f * Time.deltaTime * 60); //!MAGIC NUMBER
            bodTarget.localRotation = Quaternion.Lerp(bodTarget.transform.localRotation, Quaternion.Euler(originalBodRotation), 0.2f * Time.deltaTime * 60); //!MAGIC NUMBER
        }
    }

    private float refFloatVelocity;
    private void RotateTowards(Transform input, Vector3 towards, float offset)
    {
        float rotZ;

        Vector3 difference = towards - input.position;
        rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        float mul = Mathf.Sign(rotationReference.eulerAngles.y * -1 + 1);
        //Debug.Log(Mathf.Sign(rotationReference.eulerAngles.y * -1 + 1) + " AWUOHBFFUII" + "    " + rotationReference.eulerAngles.y);
        Quaternion target = Quaternion.Euler(input.eulerAngles.x, input.eulerAngles.y,  mul * rotZ + mul * offset);
        input.rotation = Quaternion.Lerp(input.rotation, target, 0.04f * Time.deltaTime * 100); //!N Magic Number
    }

    private IEnumerator PlayParticle(){
        particleSystem.Clear();
        particleSystem.Play();
        //Debug.Log("PLPA");
        yield return new WaitForSeconds(1);
        particleSystem.Stop();
    }

}
