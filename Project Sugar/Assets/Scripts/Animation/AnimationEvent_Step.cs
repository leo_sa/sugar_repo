using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent_Step : AnimationEvent_Playsound
{
    [SerializeField] private LayerMask groundTypeLayer;
    [SerializeField] private AudioSource layerSource;
   [SerializeField] private AudioClip[] grassLayer;
   private AudioClip[] currentClipArray;
   private string currentGroundtype;    //This is basically the "key" to what additional layer is ought to play on the footstep sound

   private void Start(){
       onSoundPlayed = PlayNewLayer;
       currentClipArray = grassLayer;   //just as default
   }

   private void PlayNewLayer(){
       Utility.PlayFast(layerSource, GenericUtility<AudioClip>.RandomFromArray(currentClipArray));    //Plays a random grasslayer
   }


   private AudioClip[] SwitchCurrentArr(string type){
       return currentClipArray; //WIP
   }
}
