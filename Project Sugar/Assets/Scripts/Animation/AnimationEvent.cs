using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    [SerializeField] protected bool allowEvent = true;

    public bool AllowEvent { get => allowEvent; set => allowEvent = value; }
}
