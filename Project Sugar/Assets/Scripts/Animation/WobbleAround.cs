using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleAround : MonoBehaviour
{
    [SerializeField, Range(0, 180)] private float rotationIntensity;
    [SerializeField, Range(0, 1)] private float scaleMultiplier = 0.1f;
    [SerializeField] private float intensity = 1;
    private Vector3 originalScale;
    [SerializeField] private Vector2 bothSinOffsets = new Vector2(5, 5);
    void Start()
    {
        originalScale = transform.localScale;
    }

    void Update()
    {
        float targetZ = Mathf.Lerp(-rotationIntensity, rotationIntensity, Utility.PositiveSinTime(Utility.PositiveSinTime(intensity, bothSinOffsets.x), bothSinOffsets.x));
        float targetScale = Mathf.Lerp(1 - scaleMultiplier, scaleMultiplier + 1, Utility.PositiveSinTime(Utility.PositiveSinTime(intensity, bothSinOffsets.y), bothSinOffsets.y));
       transform.localEulerAngles = new Vector3(0, 0, targetZ);
       transform.localScale = originalScale * targetScale;
    }
}
