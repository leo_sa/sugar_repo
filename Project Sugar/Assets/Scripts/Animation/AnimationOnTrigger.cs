using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationOnTrigger : SmoothTriggerStay
{
    [SerializeField] protected string animationTriggerString;
    [SerializeField] protected Animator target;
    [SerializeField] protected bool useTrigger, alsoActivateOnLeave;

    private void Start(){
        onTriggered += TriggerAnim;
        if(alsoActivateOnLeave) onTriggerExit += TriggerAnim;
    }



    private void TriggerAnim(){
        if(!useTrigger){
            target.SetBool(animationTriggerString, !target.GetBool(animationTriggerString));
        }
        else{
            target.SetTrigger(animationTriggerString);
        }
    }
}
