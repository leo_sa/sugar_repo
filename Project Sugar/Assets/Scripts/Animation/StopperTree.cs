using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopperTree : SmoothTriggerStay
{
    [SerializeField] private AudioSource source;
    [SerializeField] private float rotationIntensity;
    [SerializeField] private Vector2 scaleIntensity;
    [SerializeField] private Transform targetRot, targetScale;
    [SerializeField] private ParticleSystem particle;
    [SerializeField, Range(0, 1)] private float rotationSpeed = 0.3f, scaleSpeed = 0.3f;
    [SerializeField] private float wobbleIntensity;
    private bool lerpTransforms;
    private Vector3 originalScale, refWobbleTracker;
    private float originalRotation, originalYRot;

    private void Start(){
        onTriggered += PlayEffect;
        originalRotation = targetRot.localEulerAngles.z;
        originalYRot = targetRot.localEulerAngles.y;    //For "Flipping" the GO
        originalScale = targetScale.localScale;
    }

    private void Update(){
        if(lerpTransforms){
            if(targetRot.localEulerAngles.z != originalRotation){
                float z = Mathf.SmoothStep(targetRot.localEulerAngles.z, originalRotation, rotationSpeed * Time.deltaTime * 200);
                Vector3 target = new Vector3(0, originalYRot, z);
                targetRot.localEulerAngles = target;
            }   

            if(targetScale.localScale != originalScale){
                Vector3 localTarget = Utility.WobbleLerpV3(targetScale.localScale, originalScale,ref refWobbleTracker, scaleSpeed, wobbleIntensity);
                targetScale.localScale = localTarget;
            }

            if(targetRot.localEulerAngles.z == originalRotation && targetScale.localScale == originalScale){
                lerpTransforms = false;
            }
        }
    }

    private void PlayEffect(){
        particle.Stop();
        particle.Clear();
        particle.Play();
        source.Play();
        lerpTransforms = true;

        targetRot.localRotation = Quaternion.Euler(0, 0, rotationIntensity);
        targetScale.localScale = new Vector3(originalScale.x * scaleIntensity.x, originalScale.y * scaleIntensity.y, 1);
    }
}
