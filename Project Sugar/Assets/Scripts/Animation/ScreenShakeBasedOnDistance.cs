using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShakeBasedOnDistance : MonoBehaviour
{
   [SerializeField] private float maxDistanceThreshhold = 5;
   [SerializeField] private float baseIntensity = 0.1f, baseDuration = 0.05f;

   public void ExecuteDistanceScreenShake(){
       float distance = Vector2.Distance(transform.position, ServiceLocator.player.transform.position);
       if(distance > maxDistanceThreshhold) return;
       float baseValue = 1 - (distance / maxDistanceThreshhold);
       float targetIntensity = baseIntensity * baseValue;

       StartCoroutine(ServiceLocator.smoothCam.Shake(baseDuration, targetIntensity));
   }
}
