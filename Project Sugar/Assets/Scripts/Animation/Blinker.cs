using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{
    [SerializeField] private SpriteRenderer target;
    [SerializeField] private Sprite eyeOpne, eyeClosed;
    [SerializeField, Header("x is eye open and y is eye closed"), Tooltip("Random influence gets added to 1 (Random(1, 1+this))")] private Vector2 lengths, randomInfluence;

    private float timerOpen, timerClosed;
    [SerializeField] private bool stayBlinked = false;

    private void Start(){
        RecalcTimers();
    }

    private void Update(){
        if(timerOpen > 0){
            timerOpen -= Time.deltaTime;
        }
        else{
            StartCoroutine(Blink());
        }
    }



    private void RecalcTimers(){
        timerOpen = lengths.x * Random.Range(1f, 1 + randomInfluence.x);
        timerClosed = lengths.y * Random.Range(1f, 1 + randomInfluence.y);;
    }


    IEnumerator Blink(){
        target.sprite = eyeClosed;
        yield return new WaitForSeconds(timerClosed);
        if(!stayBlinked) target.sprite = eyeOpne;

        RecalcTimers();
    }

    public void ForceBlink(){
        bool blink = true;
        Sprite sprite = blink? eyeClosed : eyeOpne;
        target.sprite = sprite;
        stayBlinked = blink;
    }

    public void OpenEyes(){
        bool blink = false;
        Sprite sprite = blink? eyeClosed : eyeOpne;
        target.sprite = sprite;
        stayBlinked = blink;
    }

}
