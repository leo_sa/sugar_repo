using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimation : MonoBehaviour
{
    [SerializeField] private Vector2 randomRange = new Vector2(4, 8);
    [SerializeField] private Animator target;
    [SerializeField] private string[] triggers;

    private void Start(){
        Invoke("ToTrigger", Random.Range(randomRange.x, randomRange.y));
    }
    private void ToTrigger(){
        target.SetTrigger(triggers.RandomFromArray());
        Invoke("ToTrigger", Random.Range(randomRange.x, randomRange.y));
    }
}
