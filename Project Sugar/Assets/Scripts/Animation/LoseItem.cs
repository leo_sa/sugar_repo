using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseItem : MonoBehaviour
{
    [SerializeField] private Transform pivot;
    [SerializeField] private float radius = 1;
    [SerializeField, Range(0, 1)] private float lerpSpeed = 0.1f;
    void Update(){
        transform.position = Utility.SmoothStepV3(transform.position, pivot.position, lerpSpeed);    //!N Magic number
        Vector3 distance = transform.position - pivot.position;
        distance = Vector3.ClampMagnitude(distance, radius);
        transform.position = pivot.position + distance;

        transform.rotation = Quaternion.Lerp(transform.rotation, pivot.rotation, lerpSpeed); //!N Magic nmber
    }
}
