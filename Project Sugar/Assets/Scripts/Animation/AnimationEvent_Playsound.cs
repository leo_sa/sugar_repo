using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationEvent_Playsound : AnimationEvent
{
    protected Action onSoundPlayed;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] stepsSounds;   //may be later changed to multiple arrays based on ground-type
    public void PlaySound(AudioClip clip){
        if(allowEvent){
            Utility.PlayFast(source, clip);
            onSoundPlayed?.Invoke();
        }
    }

    public void PlaySoundUnscaled(AudioClip clip){
        if(allowEvent){
            Utility.PlayFastUnscaled(source, clip);
            onSoundPlayed?.Invoke();
        }
    }

    public void PlaySoundArray(){
        if(allowEvent){
            Utility.PlayFast(source, stepsSounds[UnityEngine.Random.Range(0, stepsSounds.Length)]); //Plays a random sound from the array
            onSoundPlayed?.Invoke();
        }
    }
}
