using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnMouseOver : MonoBehaviour, IPointerEnterHandler
{
    protected delegate void OnMouseEnter();
    protected OnMouseEnter onMouseEnter;
    public void OnPointerEnter(PointerEventData eventData){
        onMouseEnter?.Invoke();
    }
}
