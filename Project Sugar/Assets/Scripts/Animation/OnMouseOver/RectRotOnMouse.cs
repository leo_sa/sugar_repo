using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectRotOnMouse : OnMouseOver
{
    [SerializeField] private float wobbleSpeed = 0.1f, wobbleIntensity = 1, scaleIntensity = 1;
    [SerializeField] private Vector3 offsetVector;
    [SerializeField] private Vector2 randomInfluence;
    [SerializeField] private RectTransform target;
    private Vector3 originalAngles, targetAngles, tracker, originalScale;
    private bool applyEffect;
    void Awake(){
        originalAngles = target.localEulerAngles;
        originalScale = target.localScale;
        onMouseEnter += OffsetRotation;
    }

    void OnEnable(){
        target.localRotation = Quaternion.Euler(originalAngles);    //Disable wobble after new menu
        transform.localScale = originalScale;
        applyEffect = false;
    }
    
    void Update(){
        if(applyEffect){
            target.transform.localRotation = Quaternion.Lerp(target.localRotation, Quaternion.Euler(targetAngles), 0.1f * Time.deltaTime * 70);
            //transform.localScale = originalScale * ((Vector3.Distance(targetAngles, originalAngles) + 1) * scaleIntensity);
            if(targetAngles != originalAngles){
                targetAngles = Utility.WobbleLerpV3(targetAngles, originalAngles, ref tracker, wobbleSpeed, wobbleIntensity);
            }
            else{
                applyEffect = false;
            }
        }
    }

    protected void OffsetRotation(){
        if(randomInfluence.magnitude != 0)targetAngles = offsetVector * Random.Range(randomInfluence.x, randomInfluence.y);
        else targetAngles = offsetVector;
        target.transform.localRotation = Quaternion.Lerp(target.localRotation, Quaternion.Euler(targetAngles), 0.1f * Time.deltaTime * 70);
       applyEffect = true; 
    }
}
