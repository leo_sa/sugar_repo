using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionShader : SmoothTriggerStay
{
    [SerializeField] private SpriteRenderer targetSprite;
    [SerializeField] private Material target;
    [SerializeField] private string targetShaderVarID;
    [SerializeField] private bool smoothed = true;
    [SerializeField, Range(0, 1)] private float smoothedSpeed = 0.3f;

    private void Update(){
        if(isTriggered){
            Bounds bounds = targetSprite.bounds;
            float targetFloat = 0;
            if(smoothed){
                targetFloat = Mathf.SmoothStep(target.GetFloat(targetShaderVarID), -transform.InverseTransformPoint(currentCollided.transform.position).x / bounds.extents.x, smoothedSpeed * Time.deltaTime * 100);
            }
            else{
                targetFloat = -transform.InverseTransformPoint(currentCollided.transform.position).x / bounds.extents.x;
            }
            target.SetFloat(targetShaderVarID, targetFloat);
            //Debug.Log(target + "´´");
        }
    }
}
