using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleAnimationController : MonoBehaviour
{
    [SerializeField] private Sprite defaultSprite, activated;
    [SerializeField] private SpriteRenderer renderer;
    [SerializeField] private Material grapplingHookMaterial;
    private float originalIntensity;
    private bool lerpBack;
    private string toChange = "_sinIntensity";
    private void Start(){
        //Debug.Log(gameObject + "KKKDD");
        originalIntensity = grapplingHookMaterial.GetFloat(toChange);

        ServiceLocator.grappleState.onHookHit += SetMaterialWobbly;
        ServiceLocator.grappleState.onHookLeave += SetMaterialStraight;
    }
    private void SetMaterialWobbly(){
        renderer.sprite = activated;
        grapplingHookMaterial.SetFloat(toChange, 3);
        lerpBack = true;
    }

    private bool flag;
    private float refVel = 1;
    private void Update(){
        if(lerpBack){
            float grappleHookIntensity = grapplingHookMaterial.GetFloat(toChange);
            if(grappleHookIntensity > 0.1f){
                grappleHookIntensity = Mathf.SmoothDamp(grappleHookIntensity, 0, ref refVel, .3f);
                grapplingHookMaterial.SetFloat(toChange, grappleHookIntensity);
            }
            else if (grappleHookIntensity != 0){
                grapplingHookMaterial.SetFloat(toChange, 0);
                lerpBack = false;
            }
        }
    }

    private void SetMaterialStraight(){
        renderer.sprite = defaultSprite;
        grapplingHookMaterial.SetFloat(toChange, 0);
    }
}
