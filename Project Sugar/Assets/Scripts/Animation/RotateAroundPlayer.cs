using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundPlayer : SmoothTriggerStay
{
    [SerializeField] private GameObject effects;
    [SerializeField] private Animator animator;
    [SerializeField] private string animationString;
    [SerializeField] private Transform target;
    [SerializeField, Range(-360, 360)] private float onOrbitRotationOffset = 90;
    [SerializeField] private float lerpTo = 0.1f, lerpBack = 0.1f;
    private Transform currentTarget;
    private bool applyEffect;

    private Vector3 originalPos, originalEuler;

    private void Start(){
        currentTarget = ServiceLocator.player.transform;
        onTriggered += EnableEffect;
        onTriggerExit += DisableEffect;

        originalPos = target.position;
        originalEuler = target.transform.localEulerAngles;
    }

    private void Update(){
        if(applyEffect){
            //Target.RotateAround(currentTarget.position, Vector3.forward, speed);

           // Vector3 distance = Target.position - currentTarget.position;
            //distance = Vector3.ClampMagnitude(distance, intensity);
            //Target.position = currentTarget.position + distance;
            target.transform.position = Utility.SmoothStepV3(target.position, currentTarget.position, lerpTo * Time.deltaTime * 100);   //!N Magic Number
            //Target.transform.rotation = Quaternion.Lerp(Target.transform.rotation, Target.rotation, 0.1f);
            //Quaternion cached = Utility.RotateTowards_Return(currentTarget, target.position, onOrbitRotationOffset);
            //target.transform.rotation = Quaternion.Lerp(target.transform.rotation, cached, 0.1f * Time.deltaTime * 100);
            target.transform.RotateTowards(currentTarget.position, onOrbitRotationOffset);
        }
        else{
            target.transform.position = Utility.SmoothStepV3(target.position, originalPos, lerpBack * Time.deltaTime * 100);   //!N Magic Number
            Vector3 targetRot = Utility.SmoothStepV3(target.localEulerAngles, originalEuler, lerpBack * Time.deltaTime * 100);
            target.transform.rotation = Quaternion.Euler(targetRot); 
        }
    }


    private void EnableEffect(){
        effects.SetActive(true);
        animator.SetBool(animationString, true);
        currentTarget = PlayerRotator.Instance.Apply();
        applyEffect = true;
    }

    private void DisableEffect(){
        effects.SetActive(false);
        animator.SetBool(animationString, false);
        PlayerRotator.Instance.UnSub(); 
        applyEffect = false;
    }
}
