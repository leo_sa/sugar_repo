using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericFadeInFullscreen : MonoBehaviour
{
    [SerializeField] private float targetAlpha;
    [SerializeField] private Image target;
    [SerializeField] private float timeToFade = 0.4f;
    private float refVel = 1;
    void Start(){
        if(Time.timeScale != 1) TimeManager.SetTime(1);
    }

    private void Update(){
        if(target.color.a != targetAlpha){
            float cachedA = Mathf.SmoothDamp(target.color.a, targetAlpha, ref refVel, timeToFade);
            Color newColor = new Color(target.color.r, target.color.g, target.color.b, cachedA);
            target.color = newColor;
        }
    }
}
