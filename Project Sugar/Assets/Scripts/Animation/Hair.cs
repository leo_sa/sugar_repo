﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hair : MonoBehaviour
{
    [SerializeField]
    Transform point;
    [SerializeField]
    float radius, offset;

    [SerializeField] bool useRotation = true, useSinFunction = false;

    [SerializeField]
    float gravityScale = -0.5f;

    Vector3 newLocation;

    Rigidbody2D rigidBody2D;

    [SerializeField] private bool clampX, clampY;
    private Vector3 originalPosition, originalPositionLocal;
    [SerializeField, Header("Sin Variables")]
    private Rigidbody2D rbSpeedRef;
    [SerializeField] private float sinIntensity = 0.01f, sinSpeed = 1;
    [SerializeField] private bool lerpBackToOrigin;
    private Quaternion originalRotation;

    private void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        originalPosition = transform.position;
        originalPositionLocal = transform.localPosition;
        originalRotation = transform.localRotation;
    }

    void Update()               //gameObject cant leave "radius"/Circle
    {
        //rigidBody2D.velocity = Vector2.up * gravityScale;
        if(lerpBackToOrigin){
            transform.localPosition = Utility.SmoothStepV3(transform.localPosition, originalPositionLocal, 0.1f);   //!N MAgic Number
            transform.localRotation = Quaternion.Lerp(transform.localRotation, originalRotation, 0.1f);
        }
        Vector3 distance = transform.position - point.position;
        distance = Vector3.ClampMagnitude(distance, radius);
        newLocation = point.position + distance;

        if(clampX){
            transform.position = new Vector3(originalPosition.x, newLocation.y, newLocation.z);
        }
        else if (clampY){
            transform.position = new Vector3(newLocation.x, originalPosition.y, newLocation.z);
        }
        else{            
            transform.position = newLocation;
        }

        if (useRotation) RotateTowardsPoint();
        if(useSinFunction) transform.position += transform.right * (Mathf.Sin(Time.time * sinSpeed * sinIntensity));
    }

    void RotateTowardsPoint()
    {
        float rotation;

        Vector3 difference = point.position - transform.position;                  //Dieser Code wurde prinzipiell nicht von mir geschrieben. Ich habe ihn aus dem Internet übernommen und nutze ihn inwzischen seit über einem Jahr, da er perfekt funktioniert und ich delay prinzipiell selber durch z.B. eine Lerp Funktion hinzufügen kann.
        rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0f, 0f, rotation + offset);
    }

    private void OnDrawGizmos(){
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(point.position, radius);
    }
}
