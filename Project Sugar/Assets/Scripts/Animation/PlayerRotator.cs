using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotator : MonoBehaviour
{
    private static PlayerRotator rotator;

    public static PlayerRotator Instance { get => rotator; set => rotator = value; }
    private int subscribers;

    void Start(){
        rotator = this;
    }

    void Update()
    {
        float noiseSpeed = Utility.PositiveSinTime(Utility.PositiveSinTime(1, 1), 2) + 1;
        noiseSpeed *= 0.5f;
        //Now between .5 & 1
        if(subscribers > 0)transform.RotateAround(ServiceLocator.player.transform.position, Vector3.forward, -1 * noiseSpeed);   //!N MAgic Number
    }

    public Transform Apply(){
        subscribers++;
        return transform;
    }

    public void UnSub(){
        subscribers--;
    }
}
