using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinAnim : MonoBehaviour
{
    [SerializeField] private float speed = 1, intensity = 0.1f;
    private Vector3 ogPos;
    private void Start(){
        ogPos = transform.localPosition;
    }
    private void Update(){
        float mul = Mathf.Sin(Time.time * speed) * intensity;
        transform.localPosition = new Vector3(ogPos.x, ogPos.y + mul, ogPos.z);
    }
}
