using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : SmoothTriggerStay //This should have been a fade-out controller but anywas
{
    public delegate void OnFadeOut();
    public OnFadeOut onFadeOut;
    [SerializeField] private string fadeOutString, offsetString;
    [SerializeField] private Material targetMaterial;
    [SerializeField, Tooltip("Leave empty to not load anything")] private string onFadeOutLoadScene ="";
    [SerializeField, Range(0, 4)] private float fadeOutSpeed = 1, fadeBeginningSpeed = 1;
    [SerializeField] private float fadeOffset = 1;
    private float max = 3, min = 0, threshhold = 0;
    [SerializeField] private bool fadeScreenInBeginning, alsoFadeTime, alsoFadeSound;
    private bool alsoFadeSoundIntern;
    [SerializeField] private float delay;
    [SerializeField] private float timeFadeMin = 0.45f, timeFadeSpeed = 1, audioFadeSpeed = 1;
    private float originalAudioVolume;
    
    private void Start(){
        onTriggered += FadeGameOutDelay;
        targetMaterial.SetFloat(fadeOutString, max);
        if(fadeScreenInBeginning){
           targetMaterial.SetFloat(fadeOutString, min);
           StartCoroutine(FadeScreenIn());
        } 
        ServiceLocator.fadeOut = this;
    }
    private void FadeGameOutDelay(){
        alsoFadeSoundIntern = alsoFadeSound;
        //fadeOutSpeed = 0.3f;    //!MAGIC NUMBER
        Invoke("FadeGameOut", delay);
    }
    public void FadeGameOut(){
        originalAudioVolume = AudioListener.volume;
        StartCoroutine(FadeScreenOut());
    }

    public void FadeGameOut(float speed, bool alsoFadeSoundInput){
        alsoFadeSoundIntern = alsoFadeSoundInput;
        fadeOutSpeed = speed;
        originalAudioVolume = AudioListener.volume;
        StartCoroutine(FadeScreenOutSolo());
    }

    private void ExecuteOnFadeOut(){
        if(onFadeOutLoadScene != ""){
            UnityEngine.SceneManagement.SceneManager.LoadScene(onFadeOutLoadScene);
        }
        onFadeOut?.Invoke();
    }
    float opennessTracker;
    private IEnumerator FadeScreenOut(){
        Vector2 offset;
        while(targetMaterial.GetFloat(fadeOutString) > min + threshhold){
            if(alsoFadeTime){
                if(Time.timeScale > timeFadeMin) TimeManager.SetTime(Time.timeScale - Time.unscaledDeltaTime * (timeFadeSpeed * fadeOutSpeed));
            }
            //if(alsoFadeSound) if(AudioListener.volume > 0) AudioListener.volume = Mathf.Lerp(AudioListener.volume, 0, audioFadeSpeed * fadeOutSpeed);
            if(alsoFadeSoundIntern) FadeSound(-80);
            offset.x = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).x / Screen.width - 0.5f);
            offset.y = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).y / Screen.height - 0.5f);
            targetMaterial.SetFloat(fadeOutString, targetMaterial.GetFloat(fadeOutString) - Time.unscaledDeltaTime * fadeOutSpeed);
            targetMaterial.SetVector(offsetString, new Vector4(offset.x, offset.y, 0, 0));
            yield return new WaitForEndOfFrame();
        }
        targetMaterial.SetFloat(fadeOutString, min);

        ExecuteOnFadeOut();
        yield return new WaitForSecondsRealtime(fadeOffset); //!MAGIC NUMBER

        while(targetMaterial.GetFloat(fadeOutString) < max - threshhold){
            if(Time.timeScale < 1) if(Time.timeScale > timeFadeMin) TimeManager.SetTime(Time.timeScale + Time.unscaledDeltaTime * (timeFadeSpeed * fadeOutSpeed));
            //if(alsoFadeSound) if(AudioListener.volume < 1) AudioListener.volume += Time.deltaTime * (audioFadeSpeed * fadeOutSpeed);
            if(alsoFadeSoundIntern) FadeSound(0);


            offset.x = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).x / Screen.width - 0.5f);
            offset.y = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).y / Screen.height - 0.5f);
            targetMaterial.SetVector(offsetString, new Vector4(offset.x, offset.y, 0, 0));
            targetMaterial.SetFloat(fadeOutString, targetMaterial.GetFloat(fadeOutString) + Time.unscaledDeltaTime * fadeOutSpeed);
            yield return new WaitForEndOfFrame();
        }
        targetMaterial.SetFloat(fadeOutString, max);
    }

    private IEnumerator FadeScreenIn(){
        Vector2 offset = Vector2.zero;;
        targetMaterial.SetVector(offsetString, new Vector4(0, 0, 0, 0));
        TimeManager.SetTime(1); //Just to make sure :shook:
        while(targetMaterial.GetFloat(fadeOutString) < max - threshhold){
            //Debug.Log("KEO" + targetMaterial.GetFloat(fadeOutString));
            //if(Time.timeScale < 1) if(Time.timeScale > timeFadeMin) TimeManager.SetTime(Time.timeScale + Time.unscaledDeltaTime * (timeFadeSpeed * fadeBeginningSpeed));
            //if(alsoFadeSound) if(AudioListener.volume < 1) AudioListener.volume = Mathf.Lerp(AudioListener.volume, 1, audioFadeSpeed * fadeBeginningSpeed);
            if(alsoFadeSoundIntern) FadeSound(0);

            offset.x = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).x / Screen.width - 0.5f);
            offset.y = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).y / Screen.height - 0.5f);
            targetMaterial.SetVector(offsetString, new Vector4(offset.x, offset.y, 0, 0));
            targetMaterial.SetFloat(fadeOutString, targetMaterial.GetFloat(fadeOutString) + Time.unscaledDeltaTime * fadeBeginningSpeed);
            yield return new WaitForEndOfFrame();
        }
        targetMaterial.SetVector(offsetString, new Vector4(0, 0, 0, 0));
        targetMaterial.SetFloat(fadeOutString, max);
    }

    private IEnumerator FadeScreenOutSolo(){
        Vector2 offset;
        targetMaterial.SetFloat(fadeOutString, 3);  //???
        while(targetMaterial.GetFloat(fadeOutString) > min + threshhold){
            if(alsoFadeTime){
                //if(Time.timeScale > timeFadeMin) TimeManager.SetTime(Time.timeScale - Time.unscaledDeltaTime * (timeFadeSpeed * fadeOutSpeed));
            }
            //if(alsoFadeSound) if(AudioListener.volume > 0) AudioListener.volume = Mathf.Lerp(AudioListener.volume, 0, audioFadeSpeed * fadeOutSpeed);
            if(alsoFadeSoundIntern) FadeSound(-80); //!N Shouldn't be zero but more "original"
            offset.x = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).x / Screen.width - 0.5f);
            offset.y = -(CustomCameraBrain.Instance.camRef.WorldToScreenPoint(ServiceLocator.player.transform.position).y / Screen.height - 0.5f);
            targetMaterial.SetFloat(fadeOutString, targetMaterial.GetFloat(fadeOutString) - Time.unscaledDeltaTime * fadeOutSpeed);
            targetMaterial.SetVector(offsetString, new Vector4(offset.x, offset.y, 0, 0));
            Debug.Log(targetMaterial.GetFloat(fadeOutString) + "KOR" + fadeOutSpeed);
            yield return new WaitForEndOfFrame();
        }
        targetMaterial.SetFloat(fadeOutString, min);
    }

    public void FadeGameIn(){
        StartCoroutine(FadeScreenIn());
    }

    private string key = "PreMaster_Vol";
    private void FadeSound(float target){
        float cachedOriginal = AudioMixing.Instance.GetMixerFloat(key);
        if(Mathf.Abs(cachedOriginal - target) > 0.5f) AudioMixing.Instance.SetMixerFloat(key, Mathf.Lerp(cachedOriginal, target, audioFadeSpeed * fadeOutSpeed));
    }
}
