using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailLimit : MonoBehaviour
{
        //Idk im not satisied yet with this
    [SerializeField] private float xInfluence = 1, yInfluence = 1, yOffset;
    [SerializeField] private Rigidbody2D body;
    private Vector3 originalPosition;

    private void Start(){
        originalPosition = transform.localPosition;
    }

    private void Update(){
        transform.localPosition = Vector3.Lerp(transform.localPosition, ModifiedTransform(), 0.04f * Time.deltaTime * 100);
    }

    private Vector3 ModifiedTransform(){
        float flip = Input.GetAxisRaw("Horizontal");    //This probably needs to be changed in the future or something°~°
        float xMod = -body.velocity.x * xInfluence * flip;
        float yMod = Mathf.Sin(Time.time * body.velocity.x * 0.4f + 0.1f) * 9 - body.velocity.y * yInfluence - Mathf.Abs(flip) * yOffset;
        Vector3 targetVec = new Vector3(xMod, yMod, 0);
        return originalPosition + targetVec;

    }
}
