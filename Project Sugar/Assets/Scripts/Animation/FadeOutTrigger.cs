using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutTrigger : MonoBehaviour
{
    [SerializeField] private float fadeOutSpeed = 10;
    [SerializeField] private bool alsoFadeSound;
    private void SetFadeOut(){
        ServiceLocator.fadeOut.FadeGameOut(fadeOutSpeed, alsoFadeSound);
    }

    private void SetFadeIn(){
        ServiceLocator.fadeOut.FadeGameIn();
    }
}
