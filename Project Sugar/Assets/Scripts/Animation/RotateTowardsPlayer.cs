using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTowardsPlayer : SmoothTriggerStay
{
    [SerializeField] private float clampDegrees = 15;
    [SerializeField] private Transform target;
    [SerializeField, Range(-360, 360)] private float rotationOffset;
    private Transform player;
    private bool enableEffect;
    Quaternion originalRotaton;
    private void Start(){
        player = ServiceLocator.player.transform;
        onTriggered += Enabling;
        onTriggerExit += Disabling;
        originalRotaton = transform.rotation;
    }

    void LateUpdate(){
        if(enableEffect){
            target.RotateTowards(player.position, rotationOffset);
            //float clamped = Mathf.Clamp(target.localEulerAngles.z, -Mathf.Abs(clampDegrees), clampDegrees);
            Debug.Log(target.localEulerAngles.z + "KEK");
            //target.localRotation = Quaternion.Euler(target.localEulerAngles.x, target.localEulerAngles.y, clamped);
        }
        else{
            transform.rotation = Quaternion.Lerp(transform.rotation, originalRotaton, 0.1f * Time.deltaTime * 100);
        }
    }


    private void Enabling(){
        enableEffect = true;
    }

    private void Disabling(){
        enableEffect = false;
    }
}
