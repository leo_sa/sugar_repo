using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpable : SmoothTriggerStay
{
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] clips;
   [SerializeField] private Transform IK;
   [SerializeField] private float onEnterIntensity = 3, onExitIntensity = -3, smoothSpeedA = 0.2f, smoothSpeedB = 0.4f;
   private Vector3 originalPosition, currentTarget;
   private Vector3 refVelA = new Vector3(1, 1, 1), refVelB = new Vector3(1, 1, 1);
   private void Start(){
       originalPosition = IK.position;
       onTriggered += AddIKForceEx;
       onTriggerExit += AddForceGo;
   }

   private void Update(){
       currentTarget = Vector3.SmoothDamp(currentTarget, originalPosition, ref refVelA, smoothSpeedA);       //!MAGIC NUMBER
       IK.transform.position = Vector3.SmoothDamp(IK.transform.position, currentTarget, ref refVelB, smoothSpeedB); //!MAGIC NUMBER
   }


   private void AddIKForce(float intensity){
       currentTarget = new Vector3(currentTarget.x, currentTarget.y - intensity, currentTarget.z);
   }

   private void AddIKForceEx(){
       AddIKForce(onEnterIntensity);
   }

   private void AddForceGo(){
       AddIKForce(onExitIntensity);
       source.PlayFast(GenericUtility<AudioClip>.RandomFromArray(clips));
   }
}
