using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleOnEnter : SmoothTriggerStay
{
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private bool playOnMovement = true;   //this should only be true when there is a reference to the player
    private bool wasPlayingFlag, wasEmittingFlag;
    private Rigidbody2D playerBody; //This aintnt a good way to do this but...yeah :I

    private void Start(){
        playerBody = RigAnimationController.Instance.Body;
        particle.Play();
        var emmission = particle.emission;
        emmission.enabled = false;
    }

    //!like 2x unecessary local variable
    private void Update(){
        if(isTriggered){
            if(playOnMovement){
                //Currently only the player can cast this
                if(playerBody.velocity.magnitude >= 0.1f){
                    if(!wasEmittingFlag){
                        //particle.loop = true;
                        var emmission = particle.emission;
                        emmission.enabled = true;
                        wasEmittingFlag = true;
                    }
                    particle.transform.position = playerBody.transform.position;    //not perfect but welp
                }
                else{
                    if(wasEmittingFlag){
                        var emmission = particle.emission;
                        emmission.enabled = false;
                        wasEmittingFlag = false;
                    }
                }
            }
            Debug.Log(wasEmittingFlag + "   awdaawd " +  playerBody.velocity.magnitude);
            if(!wasPlayingFlag){
                wasPlayingFlag = true;
            }
        }
        else{
            if(wasPlayingFlag){
                var emmission = particle.emission;
                emmission.enabled = false;
                wasEmittingFlag = false;
                wasPlayingFlag = false;
            }
        }
    }
}
