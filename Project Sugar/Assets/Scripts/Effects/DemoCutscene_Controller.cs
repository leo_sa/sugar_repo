using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoCutscene_Controller : MonoBehaviour
{
    [SerializeField] private SpriteRenderer leafs;
    [SerializeField] private GameObject leafSystem;


    public void ExecuteLeafRemoval(){   //Do it.
        leafs.enabled = false;
        leafSystem.SetActive(true); //using this for some ease of usage.
    }
}
